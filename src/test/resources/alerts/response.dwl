%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
    {
        "title": "Rip Current Statement issued February 16 at 2:24PM EST until February 17 at 7:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Wind Advisory issued February 16 at 12:10PM CST until February 17 at 6:00PM CST by NWS Mobile AL",
        "description": "* WHAT...South winds 15 to 20 mph with gusts up to 40 mph\nexpected.\n\n* WHERE...Portions of south central and southwest Alabama,\nnorthwest Florida and southeast Mississippi.\n\n* WHEN...From 8 AM to 6 PM CST Thursday.\n\n* IMPACTS...Gusty winds could blow around unsecured objects.\nTree limbs could be blown down and a few power outages may\nresult.",
        "instructions": "Use extra caution when driving, especially if operating a high\nprofile vehicle. Secure outdoor objects."
    },
    {
        "title": "High Surf Advisory issued February 16 at 12:09PM CST until February 18 at 9:00AM CST by NWS Mobile AL",
        "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet in the surf zone. For the High Rip Current Risk,\ndangerous rip currents.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...For the High Surf Advisory, until 9 AM CST Friday. For\nthe High Rip Current Risk, through Friday afternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
        "instructions": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 16 at 12:09PM CST until February 18 at 6:00PM CST by NWS Mobile AL",
        "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet in the surf zone. For the High Rip Current Risk,\ndangerous rip currents.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...For the High Surf Advisory, until 9 AM CST Friday. For\nthe High Rip Current Risk, through Friday afternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
        "instructions": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 16 at 7:02AM EST until February 17 at 7:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "High Surf Advisory issued February 16 at 5:18AM CST until February 18 at 9:00AM CST by NWS Mobile AL",
        "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet expected in the surf zone. For the High Rip Current\nRisk, dangerous rip currents.\n\n* WHERE...In Alabama, Mobile Coastal and Baldwin Coastal\nCounties. In Florida, Escambia Coastal, Santa Rosa Coastal and\nOkaloosa Coastal Counties.\n\n* WHEN...For the High Surf Advisory, from noon today to 9 AM CST\nFriday. For the High Rip Current Risk, through Friday\nafternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
        "instructions": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 16 at 5:18AM CST until February 18 at 6:00PM CST by NWS Mobile AL",
        "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet expected in the surf zone. For the High Rip Current\nRisk, dangerous rip currents.\n\n* WHERE...In Alabama, Mobile Coastal and Baldwin Coastal\nCounties. In Florida, Escambia Coastal, Santa Rosa Coastal and\nOkaloosa Coastal Counties.\n\n* WHEN...For the High Surf Advisory, from noon today to 9 AM CST\nFriday. For the High Rip Current Risk, through Friday\nafternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
        "instructions": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Beach Hazards Statement issued February 16 at 2:46AM EST until February 16 at 3:00AM EST by NWS Melbourne FL",
        "description": "* WHAT...Numerous strong, potentially life threatening rip\ncurrents, and gusty easterly winds producing rough surf and\nbreaking waves of 5 to 6 feet.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through late tonight.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water. Entering the surf is strongly\ndiscouraged due to the dangerous surf and rip current\nconditions.",
        "instructions": "Rip currents are powerful channels of water flowing quickly away\nfrom shore, which occur most often at low spots or breaks in the\nsandbar and in the vicinity of structures such as jetties and\npiers. Heed the advice of lifeguards, beach patrol flags and\nsigns.\n\nIf caught in a rip current, relax and float. Don't swim against\nthe current. If able, swim in a direction following the shoreline.\nIf unable to escape, face the shore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 16 at 2:46AM EST until February 17 at 4:00AM EST by NWS Melbourne FL",
        "description": "* WHAT...Numerous strong, potentially life threatening rip\ncurrents, and gusty easterly winds producing rough surf and\nbreaking waves of 5 to 6 feet.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through late tonight.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water. Entering the surf is strongly\ndiscouraged due to the dangerous surf and rip current\nconditions.",
        "instructions": "Rip currents are powerful channels of water flowing quickly away\nfrom shore, which occur most often at low spots or breaks in the\nsandbar and in the vicinity of structures such as jetties and\npiers. Heed the advice of lifeguards, beach patrol flags and\nsigns.\n\nIf caught in a rip current, relax and float. Don't swim against\nthe current. If able, swim in a direction following the shoreline.\nIf unable to escape, face the shore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 16 at 2:09AM EST until February 17 at 6:00PM EST by NWS Jacksonville FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Northeast Florida Beaches.\n\n* WHEN...From 6 AM EST this morning through Thursday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 16 at 1:51AM EST until February 18 at 7:00PM EST by NWS Tallahassee FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Walton, Bay, Gulf, and Franklin County Beaches.\n\n* WHEN...Through Friday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 15 at 8:53PM CST until February 18 at 6:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...From 6 PM CST this evening through Friday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 15 at 2:36PM CST until February 18 at 6:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...From 6 PM CST this evening through Friday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 15 at 3:22PM EST until February 17 at 7:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "TEST Tsunami Warning issued February 15 at 12:16PM EST until February 15 at 1:16PM EST by NWS National Tsunami Warning Center",
        "description": "GMZ130-150-155-230-235-250-255-330-335-350-355-450-452-455-\n532-534-536-538-550-552-555-630>635-650-655-730-750-755-765-\n830-836-850-853-856-656-657-031-032-034-035-042>044-052>055-\nAMZ630-650-651-550-552-555-450-452-454-330-350-352-354-250-\n252-254-256-130-135-150-152-154-156-158-ANZ631>638-656-658-\n650-652-654-430-431-450>455-330-335-338-340-345-350-353-355-\n230>237-250-254-255-256-150-050>052-TXZ251-256-257-242>247-\n213-214-236>238-215-216-LAZ052>054-073-074-040-062-064-066>070-\nMSZ080>082-ALZ263>266-FLZ202-204-206-008-012-014-015-018-027-\n028-034-139-142-148-149-050-151-155-160-162-165-069-070-075-\n076>078-174-168-172-173-047-054-059-064-141-147-124-125-033-\n038-GAZ154-166-117-119-139-141-SCZ048>052-054-056-NCZ106-108-\n110-045>047-080-081-094-095-098-103-104-015>017-030>032-102-\nVAZ084-086-091-094-095-098-099-100-MDZ025-DEZ002>004-NJZ006-\n012>014-021-023>026-106-108-NYZ071>075-078>081-176>179-CTZ009>012-\nRIZ002-004>008-MAZ007-016-019>024-NHZ014-MEZ022>028-029-030-\nNBZ570-550-660-641-NSZ210-230-260-250-110-120-130-170-160-\n150-140-270-280-320-410-450-440-430-QCZ670-680-NLZ340-220-\n230-210-120-132-140-241-242-110-131-540-530-570-520-510-560-\n610-720-710-730-740-750-760-770-151816-\n/T.NEW.PAAQ.TS.W.9004.220215T1716Z-220215T1816Z/\nThe U.S. east coast, Gulf of Mexico coasts, and Eastern\nCanadian coastal areas\n\n...THIS_MESSAGE_IS_FOR_TEST_PURPOSES_ONLY...\n\n...THIS IS A TEST TO DETERMINE TRANSMISSION TIMES INVOLVED IN THE",
        "instructions": null
    },
    {
        "title": "TEST Tsunami Warning issued February 15 at 12:16PM EST until February 15 at 1:16PM EST by NWS National Tsunami Warning Center",
        "description": "GMZ130-150-155-230-235-250-255-330-335-350-355-450-452-455-\n532-534-536-538-550-552-555-630>635-650-655-730-750-755-765-\n830-836-850-853-856-656-657-031-032-034-035-042>044-052>055-\nAMZ630-650-651-550-552-555-450-452-454-330-350-352-354-250-\n252-254-256-130-135-150-152-154-156-158-ANZ631>638-656-658-\n650-652-654-430-431-450>455-330-335-338-340-345-350-353-355-\n230>237-250-254-255-256-150-050>052-TXZ251-256-257-242>247-\n213-214-236>238-215-216-LAZ052>054-073-074-040-062-064-066>070-\nMSZ080>082-ALZ263>266-FLZ202-204-206-008-012-014-015-018-027-\n028-034-139-142-148-149-050-151-155-160-162-165-069-070-075-\n076>078-174-168-172-173-047-054-059-064-141-147-124-125-033-\n038-GAZ154-166-117-119-139-141-SCZ048>052-054-056-NCZ106-108-\n110-045>047-080-081-094-095-098-103-104-015>017-030>032-102-\nVAZ084-086-091-094-095-098-099-100-MDZ025-DEZ002>004-NJZ006-\n012>014-021-023>026-106-108-NYZ071>075-078>081-176>179-CTZ009>012-\nRIZ002-004>008-MAZ007-016-019>024-NHZ014-MEZ022>028-029-030-\nNBZ570-550-660-641-NSZ210-230-260-250-110-120-130-170-160-\n150-140-270-280-320-410-450-440-430-QCZ670-680-NLZ340-220-\n230-210-120-132-140-241-242-110-131-540-530-570-520-510-560-\n610-720-710-730-740-750-760-770-151816-\n/T.NEW.PAAQ.TS.W.9004.220215T1716Z-220215T1816Z/\nThe U.S. east coast, Gulf of Mexico coasts, and Eastern\nCanadian coastal areas\n\n...THIS_MESSAGE_IS_FOR_TEST_PURPOSES_ONLY...\n\n...THIS IS A TEST TO DETERMINE TRANSMISSION TIMES INVOLVED IN THE\nDISSEMINATION OF TSUNAMI INFORMATION...\n\n...ESTO ES UNA PRUEBA PARA DETERMINAR LOS TIEMPOS DE TRANSMISION\nENVUELTOS EN LA DISEMINACION DE INFORMACION SOBRE TSUNAMIS...\n\n\nRESPONSES ARE REQUIRED FROM\n---------------------------\n* All Coastal Weather Forecast Offices in the Eastern and\nSouthern Regions - respond using tsunami message\nacknowledgment (TMA) procedures. Emergency alert systems and\nNOAA Weather Radio are NOT to be activated.\n\n* State and Territorial Warning Points in ME, NH, MA, CT -\nRI, NY, NJ, DE, MD, PA, VA, NC, SC, GA, FL, AL -\nMS, LA, and TX.\n\n* Joint Typhoon Warning Center in Hawaii\n\n* Atlantic Storm Prediction Center NS, Government of Canada\nOperations Center, and Saint-Pierre et Miquelon.\n\n\nRESPONSES SHOULD INCLUDE\n------------------------\n* Time-of-receipt\n* Agency name\n* Email address\n* Phone number\n\n\nWeather Service Offices should respond in accordance with local\ndirectives. All others should reply by one of the available methods\nbelow.\n\n\nSEND RESPONSE BY\n----------------\n* Web - ntwc.arh.noaa.gov/commtest/index.html\n* Email address - ntwc@noaa.gov\n* AFTN address  - PAAQYQYX\n* AWIPS         - TMA\n* Fax           - 907-745-6071\n\n...THIS_MESSAGE_IS_FOR_TEST_PURPOSES_ONLY...\n\n...THIS IS A TEST TO DETERMINE TRANSMISSION TIMES INVOLVED IN THE\nDISSEMINATION OF TSUNAMI INFORMATION...",
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 15 at 11:48AM EST until February 15 at 7:00PM EST by NWS Tallahassee FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Franklin County Beaches.\n\n* WHEN...From noon EST today through this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "The Frost Advisory has been cancelled.",
        "description": "The Frost Advisory has been cancelled and is no longer in effect.",
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 15 at 7:19AM EST until February 17 at 7:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 15 at 6:16AM CST until February 18 at 6:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...In Alabama, Mobile Coastal and Baldwin Coastal\nCounties. In Florida, Escambia Coastal, Santa Rosa Coastal and\nOkaloosa Coastal Counties.\n\n* WHEN...From 6 PM CST this evening through Friday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 15 at 5:36AM EST until February 15 at 7:00PM EST by NWS Tallahassee FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Franklin County Beaches.\n\n* WHEN...From noon EST today through this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Beach Hazards Statement issued February 15 at 2:55AM EST until February 16 at 3:00AM EST by NWS Melbourne FL",
        "description": "* WHAT...Gusty easterly winds will produce rough surf and breaking\nwaves of 3 to 5 feet. There will also be a Moderate risk of\ndangerous rip currents.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through late tonight.\n\n* IMPACTS...Large breaking waves can knock you off your feet and\nmake you even more susceptible to being caught in the seaward\npull of a rip current.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 14 at 3:40PM EST until February 14 at 4:00PM EST by NWS Miami FL",
        "description": "A moderate risk of rip currents will remain through the rest of\nthe day. Exercise caution across the area beaches.",
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 14 at 3:40PM EST until February 17 at 7:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Palm Beach, Coastal Broward and Coastal Miami-\nDade Counties.\n\n* WHEN...From 7 AM EST this morning through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Frost Advisory issued February 14 at 2:11PM EST until February 15 at 9:00AM EST by NWS Jacksonville FL",
        "description": "* WHAT...Temperatures as low as 33 will result in frost\nformation.\n\n* WHERE...Portions of southeast Georgia and northeast and\nnorthern Florida.\n\n* WHEN...From 3 AM to 9 AM EST Tuesday.\n\n* IMPACTS...Frost could kill sensitive outdoor vegetation if\nleft uncovered.",
        "instructions": "Take steps now to protect tender plants from the cold."
    },
    {
        "title": "Rip Current Statement issued February 14 at 2:10PM EST until February 14 at 7:00PM EST by NWS Tampa Bay Ruskin FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Manatee, Pinellas, Coastal Charlotte, Coastal\nLee and Coastal Sarasota Counties.\n\n* WHEN...Until 7 PM EST this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 14 at 11:46AM EST until February 14 at 10:00PM EST by NWS Tallahassee FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Gulf County Beaches.\n\n* WHEN...Until 10 PM EST /9 PM CST/ this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.\n\n* ADDITIONAL DETAILS...St Joseph State Park is flying red flags\nthis morning.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Red Flag Warning issued February 14 at 10:28AM EST until February 14 at 5:00PM EST by NWS Tampa Bay Ruskin FL",
        "description": "The National Weather Service in Tampa Bay Area - Ruskin FL has\nissued a Red Flag Warning, which is in effect until 5 PM EST this\nafternoon.\n\n* AFFECTED AREA...Coastal Lee...Inland Lee.\n\n* WIND...Northerly winds 15 to 20 mph and gusty.\n\n* HUMIDITY...around 25 percent.\n\n* ERC...36.\n\n* IMPACTS...any fires that develop will likely spread rapidly.\nOutdoor burning is not recommended.",
        "instructions": "A Red Flag Warning means that critical fire weather conditions\nare either occurring now....or will shortly. A combination of\nstrong winds...low relative humidity...and warm temperatures can\ncontribute to extreme fire behavior."
    },
    {
        "title": "Freeze Warning issued February 14 at 8:40AM EST until February 14 at 9:00AM EST by NWS Jacksonville FL",
        "description": null,
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 14 at 4:28AM EST until February 14 at 3:00PM EST by NWS Tampa Bay Ruskin FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Manatee, Pinellas, Coastal Charlotte, Coastal\nLee and Coastal Sarasota Counties.\n\n* WHEN...Until 3 PM EST this afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Beach Hazards Statement issued February 14 at 3:32AM EST until February 14 at 7:00PM EST by NWS Melbourne FL",
        "description": "* WHAT...A strong current flowing southward, parallel to the\ncoast, within the surf zone and a moderate risk of life-\nthreatening rip currents.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through this evening.\n\n* IMPACTS...Strong currents can knock you off your feet and make\nyou even more susceptible to becoming caught in the seaward pull\nof a rip current.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 14 at 2:30AM EST until February 14 at 4:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Collier County.\n\n* WHEN...From 1 AM EST Monday through Monday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Rip Current Statement issued February 14 at 2:30AM EST until February 17 at 7:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Palm Beach, Coastal Broward and Coastal Miami-\nDade Counties.\n\n* WHEN...From 7 AM EST this morning through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "The Flood Warning has been cancelled.",
        "description": "The Flood Warning has been cancelled and is no longer in effect.",
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 13 at 3:13PM EST until February 14 at 4:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Collier County.\n\n* WHEN...From 1 AM EST Monday through Monday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Freeze Warning issued February 13 at 2:07PM EST until February 14 at 9:00AM EST by NWS Jacksonville FL",
        "description": "* WHAT...Sub-freezing temperatures as low as 31 expected.\n\n* WHERE...Portions of southeast Georgia and northeast and\nnorthern Florida.\n\n* WHEN...From 3 AM to 9 AM EST Monday.\n\n* IMPACTS...Frost and freeze conditions could kill crops and\nother sensitive vegetation.",
        "instructions": "Appropriate action should be taken to ensure tender vegetation\nand outdoor pets have adequate protection from the cold\ntemperatures. Young children, the elderly and the homeless are\nespecially vulnerable to the cold. Take measures to protect them."
    },
    {
        "title": "Rip Current Statement issued February 13 at 1:46PM EST until February 14 at 3:00PM EST by NWS Tampa Bay Ruskin FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Pinellas, Coastal Manatee, Coastal Sarasota, Coastal\nCharlotte and Coastal Lee Counties.\n\n* WHEN...Through Monday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Flood Warning issued February 13 at 10:15AM CST until February 13 at 8:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until this evening.\n\n* IMPACTS...At 17 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 10:05 AM CST Sunday the stage was 17.1 feet.\n- Forecast...The river is expected to fall below flood stage\nthis afternoon and continue falling to 12.6 feet Friday\nmorning.\n- Flood stage is 17 feet.\n- www.weather.gov/safety/flood",
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 13 at 6:32AM EST until February 14 at 7:00AM EST by NWS Tallahassee FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...West-Facing Gulf County Beaches.\n\n* WHEN...Through Monday morning.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Special Weather Statement issued February 13 at 5:09AM CST by NWS Mobile AL",
        "description": "...MUCH DRIER AIR AND INCREASED NORTHERLY WINDS WILL BRING AN\nELEVATED FIRE DANGER TO THE AREA THIS AFTERNOON...\n\nA much colder airmass is moving into southeast Mississippi and\ninterior southwest Alabama early this morning, with temperatures\nfalling into the mid to upper 30s. An approaching upper level\nsystem will bring an area of light precipitation to much of\nsoutheast Mississippi and southwest Alabama through around 9 AM\nCST this morning. Temperatures may be cold enough for light rain\nto briefly mix with or change to light snow flurries across\nlocations generally along and northwest of the Interstate 65\ncorridor early this morning. The wintry mix potential will be very\nbrief and no accumulations or impacts are anticipated.\n\nA much drier airmass will spread across the region in the wake of\nthe passing system this morning. Very low afternoon relative\nhumidity values in combination with increased northerly winds\nwill lead to elevated fire danger across the area this afternoon.\nExercise extreme caution if using flammable materials outdoors\ntoday.",
        "instructions": null
    },
    {
        "title": "Flood Warning issued February 12 at 7:06PM CST until February 13 at 3:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring and minor flooding is forecast.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until tomorrow afternoon.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 6:05 PM CST Saturday the stage was 17.8 feet.\n- Recent Activity...The maximum river stage in the 24 hours\nending at 6:05 PM CST Saturday was 18.1 feet.\n- Forecast...The river is expected to fall below flood stage\nlate tomorrow morning and continue falling to 12.5 feet\nThursday evening.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 12 at 11:39AM EST until February 14 at 7:00AM EST by NWS Tallahassee FL",
        "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...West-Facing Gulf County Beaches.\n\n* WHEN...From Sunday morning through Monday morning.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "Flood Warning issued February 12 at 10:06AM CST until February 13 at 4:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until tomorrow afternoon.\n\n* IMPACTS...At 17 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 9:05 AM CST Saturday the stage was 18 feet.\n- Forecast...The river is expected to crest at 18 feet this\nafternoon. It will then fall below flood stage late tomorrow\nmorning.\n- Flood stage is 17 feet.\n- www.weather.gov/safety/flood",
        "instructions": null
    },
    {
        "title": "Dense Fog Advisory issued February 12 at 10:02AM CST until February 12 at 10:00AM CST by NWS Mobile AL",
        "description": "Surface observations across the entire area are reporting that\nvisibilities have improved above dense fog advisory criteria.\nVisible satellite imagery is showing some residual valley fog\nremaining, and should dissipate through noon. Therefore, the\nDense Fog Advisory has been allowed to expire.",
        "instructions": null
    },
    {
        "title": "Dense Fog Advisory issued February 12 at 7:46AM EST until February 12 at 8:00AM EST by NWS Miami FL",
        "description": "Fog has begun to dissipate and will continue to become less dense\nas the morning progresses and thus the advisory will be allowed to\nexpire. However, patchy fog and localized visibility restrictions\nwill remain possible for the next hour or so in the area.",
        "instructions": null
    },
    {
        "title": "Dense Fog Advisory issued February 12 at 6:34AM CST until February 12 at 10:00AM CST by NWS Mobile AL",
        "description": "* WHAT...Visibility reduced to one quarter mile or less in dense\nfog.\n\n* WHERE...Portions of southwest Alabama and southeast\nMississippi.\n\n* WHEN...Until 10 AM CST this morning.\n\n* IMPACTS...Hazardous driving conditions due to low visibility.",
        "instructions": "If driving, slow down, use your headlights, and leave plenty of\ndistance ahead of you."
    },
    {
        "title": "Special Weather Statement issued February 12 at 6:43AM EST by NWS Melbourne FL",
        "description": "Surface observations, traffic cameras and satellite imagery\nindicate that patchy dense fog, producing visibilities of a\nquarter of a mile or less, has spread northward across portions\nof Osceola and southern Brevard counties early this morning,\nespecially near to west of Interstate 95 and across Florida's\nTurnpike. This fog is expected to persist across this area\nthrough sunrise, then dissipate shortly after 8 AM.\n\nMotorists on area roadways in these counties should prepare to\nencounter reduced visibilities due to the fog. When driving in\nfog, reduce your speed, use only your low beam headlights, and\nleave extra distance between your vehicle and the one in front of\nyou.",
        "instructions": null
    },
    {
        "title": "Dense Fog Advisory issued February 12 at 4:11AM CST until February 12 at 9:00AM CST by NWS Mobile AL",
        "description": "* WHAT...Visibility reduced to one quarter mile or less in dense\nfog.\n\n* WHERE...In Alabama, Monroe, Conecuh and Escambia Counties. In\nFlorida, Escambia Inland County.\n\n* WHEN...Until 9 AM CST Saturday.\n\n* IMPACTS...Hazardous driving conditions due to low visibility.",
        "instructions": "If driving, slow down, use your headlights, and leave plenty of\ndistance ahead of you."
    },
    {
        "title": "Special Weather Statement issued February 12 at 5:10AM EST by NWS Melbourne FL",
        "description": "Surface observations and satellite imagery indicate that patchy\ndense fog, producing visibilities of a quarter of a mile or less,\nhas begun to form across portions of Okeechobee, Indian River,\nSaint Lucie and Martin counties early this morning. This fog is\nexpected to persist across this area through sunrise, then\ndissipate shortly after 8 AM.\n\nMotorists on area roadways in these counties should prepare to\nencounter reduced visibilities due to the fog. When driving in\nfog, reduce your speed, use only your low beam headlights, and\nleave extra distance between your vehicle and the one in front of\nyou.",
        "instructions": null
    },
    {
        "title": "Dense Fog Advisory issued February 12 at 4:13AM EST until February 12 at 8:00AM EST by NWS Miami FL",
        "description": "* WHAT...Visibility a quarter mile or less.\n\n* WHERE...Glades and Hendry Counties.\n\n* WHEN...Until 8 AM EST this morning.\n\n* IMPACTS...Hazardous driving conditions due to low visibility.",
        "instructions": "If driving, slow down, use your headlights, and leave plenty of\ndistance ahead of you."
    },
    {
        "title": "Flood Warning issued February 11 at 7:17PM CST until February 13 at 9:00AM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring and minor flooding is forecast.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until late Sunday morning.\n\n* IMPACTS...At 19.0 feet, Low lying pastures will flood. Cattle\nshould be moved to higher ground.\n\n* ADDITIONAL DETAILS...\n- At 7:05 PM CST Friday the stage was 18.1 feet.\n- Recent Activity...The maximum river stage in the 24 hours\nending at 7:05 PM CST Friday was 18.1 feet.\n- Forecast...The river is expected to fall below flood stage\nearly Sunday morning and continue falling to 13.7 feet\nWednesday evening.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
        "instructions": null
    },
    {
        "title": "Flood Warning issued February 11 at 9:46AM CST until February 13 at 12:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until early Sunday afternoon.\n\n* IMPACTS...At 19 feet, Low lying pastures will flood. Cattle should\nbe moved to higher ground.\n\n* ADDITIONAL DETAILS...\n- At 9:05 AM CST Friday the stage was 18 feet.\n- Forecast...The river is expected to fall below flood stage\nSunday morning and continue falling to 14.2 feet Wednesday\nmorning.\n- Flood stage is 17 feet.\n- www.weather.gov/safety/flood",
        "instructions": null
    },
    {
        "title": "Flood Warning issued February 10 at 8:04PM CST until February 13 at 9:00AM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until late Sunday morning.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 7:05 PM CST Thursday the stage was 17.9 feet.\n- Forecast...The river is expected to rise to a crest of 18.1\nfeet early Friday afternoon. It will then begin to fall,\nfalling below flood stage early Sunday morning.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
        "instructions": null
    },
    {
        "title": "Flood Warning issued February 10 at 10:01AM CST until February 13 at 6:00PM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until Sunday afternoon.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands. At 19.0\nfeet, Low lying pastures will flood. Cattle should be moved to\nhigher ground.\n\n* ADDITIONAL DETAILS...\n- At 9:05 AM CST Thursday the stage was 17.7 feet.\n- Forecast...The river is expected to continue to rise,\ncresting at a stage of 18.3 feet early Friday afternoon. It\nwill then begin to fall, falling below flood stage Sunday\nafternoon.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
        "instructions": null
    },
    {
        "title": "The Rip Current Statement has been cancelled.",
        "description": "The Rip Current Statement has been cancelled and is no longer in effect.",
        "instructions": null
    },
    {
        "title": "Rip Current Statement issued February 10 at 2:26AM EST until February 10 at 7:00PM EST by NWS Miami FL",
        "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Palm Beach County.\n\n* WHEN...Through this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
        "instructions": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help."
    },
    {
        "title": "The Flood Warning has been cancelled.",
        "description": "The Flood Warning has been cancelled and is no longer in effect.",
        "instructions": null
    },
    {
        "title": "Flood Warning issued February 9 at 7:33PM CST until February 13 at 9:00AM CST by NWS Mobile AL",
        "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until late Sunday morning.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands. At 19.0\nfeet, Low lying pastures will flood. Cattle should be moved to\nhigher ground.\n\n* ADDITIONAL DETAILS...\n- At 7:05 PM CST Wednesday the stage was 17.5 feet.\n- Forecast...The river is expected to continue to rise,\ncresting at a stage of 18.2 feet early Friday morning. It\nwill then begin to fall, falling below flood stage early\nSunday morning.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
        "instructions": null
    }
])