{
    "@context": [
        "https://geojson.org/geojson-ld/geojson-context.jsonld",
        {
            "@version": "1.1",
            "wx": "https://api.weather.gov/ontology#",
            "@vocab": "https://api.weather.gov/ontology#"
        }
    ],
    "type": "FeatureCollection",
    "features": [
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.4703d66c56a0477858a52457f61d8e184b166eed.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.4703d66c56a0477858a52457f61d8e184b166eed.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.4703d66c56a0477858a52457f61d8e184b166eed.001.1",
                "areaDesc": "Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County",
                "geocode": {
                    "SAME": [
                        "012099",
                        "012011",
                        "012086"
                    ],
                    "UGC": [
                        "FLZ168",
                        "FLZ172",
                        "FLZ173"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3109fcdd9d541465bfa7b3d45303e8114b3adc6f.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.3109fcdd9d541465bfa7b3d45303e8114b3adc6f.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-16T07:02:00-05:00"
                    }
                ],
                "sent": "2022-02-16T14:24:00-05:00",
                "effective": "2022-02-16T14:24:00-05:00",
                "onset": "2022-02-16T14:24:00-05:00",
                "expires": "2022-02-17T07:00:00-05:00",
                "ends": "2022-02-17T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 16 at 2:24PM EST until February 17 at 7:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 161924"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH THURSDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMFL.RP.S.0008.000000T0000Z-220218T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.25e23196da312bd570c5edfb6bb432bdcdf55fd4.001.1,2022-02-15T15:22:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e1730c96e04e901b3239271e0eced1fdefa12dad.001.1,2022-02-15T07:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.002.1,2022-02-14T15:40:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1,2022-02-14T02:30:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.32f7cdbd31677f845ec644c57988b9258c896deb.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.32f7cdbd31677f845ec644c57988b9258c896deb.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.32f7cdbd31677f845ec644c57988b9258c896deb.001.1",
                "areaDesc": "Choctaw; Washington; Clarke; Wilcox; Monroe; Conecuh; Butler; Crenshaw; Escambia; Covington; Mobile Inland; Baldwin Inland; Mobile Central; Baldwin Central; Mobile Coastal; Baldwin Coastal; Escambia Inland; Escambia Coastal; Santa Rosa Inland; Santa Rosa Coastal; Okaloosa Inland; Okaloosa Coastal; Wayne; Perry; Greene; Stone; George",
                "geocode": {
                    "SAME": [
                        "001023",
                        "001129",
                        "001025",
                        "001131",
                        "001099",
                        "001035",
                        "001013",
                        "001041",
                        "001053",
                        "001039",
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091",
                        "028153",
                        "028111",
                        "028041",
                        "028131",
                        "028039"
                    ],
                    "UGC": [
                        "ALZ051",
                        "ALZ052",
                        "ALZ053",
                        "ALZ054",
                        "ALZ055",
                        "ALZ056",
                        "ALZ057",
                        "ALZ058",
                        "ALZ059",
                        "ALZ060",
                        "ALZ261",
                        "ALZ262",
                        "ALZ263",
                        "ALZ264",
                        "ALZ265",
                        "ALZ266",
                        "FLZ201",
                        "FLZ202",
                        "FLZ203",
                        "FLZ204",
                        "FLZ205",
                        "FLZ206",
                        "MSZ067",
                        "MSZ075",
                        "MSZ076",
                        "MSZ078",
                        "MSZ079"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ051",
                    "https://api.weather.gov/zones/forecast/ALZ052",
                    "https://api.weather.gov/zones/forecast/ALZ053",
                    "https://api.weather.gov/zones/forecast/ALZ054",
                    "https://api.weather.gov/zones/forecast/ALZ055",
                    "https://api.weather.gov/zones/forecast/ALZ056",
                    "https://api.weather.gov/zones/forecast/ALZ057",
                    "https://api.weather.gov/zones/forecast/ALZ058",
                    "https://api.weather.gov/zones/forecast/ALZ059",
                    "https://api.weather.gov/zones/forecast/ALZ060",
                    "https://api.weather.gov/zones/forecast/ALZ261",
                    "https://api.weather.gov/zones/forecast/ALZ262",
                    "https://api.weather.gov/zones/forecast/ALZ263",
                    "https://api.weather.gov/zones/forecast/ALZ264",
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ201",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ203",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ205",
                    "https://api.weather.gov/zones/forecast/FLZ206",
                    "https://api.weather.gov/zones/forecast/MSZ067",
                    "https://api.weather.gov/zones/forecast/MSZ075",
                    "https://api.weather.gov/zones/forecast/MSZ076",
                    "https://api.weather.gov/zones/forecast/MSZ078",
                    "https://api.weather.gov/zones/forecast/MSZ079"
                ],
                "references": [],
                "sent": "2022-02-16T12:10:00-06:00",
                "effective": "2022-02-16T12:10:00-06:00",
                "onset": "2022-02-17T08:00:00-06:00",
                "expires": "2022-02-16T21:00:00-06:00",
                "ends": "2022-02-17T18:00:00-06:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Wind Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Wind Advisory issued February 16 at 12:10PM CST until February 17 at 6:00PM CST by NWS Mobile AL",
                "description": "* WHAT...South winds 15 to 20 mph with gusts up to 40 mph\nexpected.\n\n* WHERE...Portions of south central and southwest Alabama,\nnorthwest Florida and southeast Mississippi.\n\n* WHEN...From 8 AM to 6 PM CST Thursday.\n\n* IMPACTS...Gusty winds could blow around unsecured objects.\nTree limbs could be blown down and a few power outages may\nresult.",
                "instruction": "Use extra caution when driving, especially if operating a high\nprofile vehicle. Secure outdoor objects.",
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWMOB"
                    ],
                    "WMOidentifier": [
                        "WWUS74 KMOB 161810"
                    ],
                    "NWSheadline": [
                        "WIND ADVISORY IN EFFECT FROM 8 AM TO 6 PM CST THURSDAY"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMOB.WI.Y.0004.220217T1400Z-220218T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T00:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.745a0f206c1113401ceadbffc0cf5dae1c297f32.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.745a0f206c1113401ceadbffc0cf5dae1c297f32.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.745a0f206c1113401ceadbffc0cf5dae1c297f32.001.1",
                "areaDesc": "Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal",
                "geocode": {
                    "SAME": [
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091"
                    ],
                    "UGC": [
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-16T05:18:00-06:00"
                    }
                ],
                "sent": "2022-02-16T12:09:00-06:00",
                "effective": "2022-02-16T12:09:00-06:00",
                "onset": "2022-02-16T12:09:00-06:00",
                "expires": "2022-02-16T21:00:00-06:00",
                "ends": "2022-02-18T09:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Minor",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "High Surf Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "High Surf Advisory issued February 16 at 12:09PM CST until February 18 at 9:00AM CST by NWS Mobile AL",
                "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet in the surf zone. For the High Rip Current Risk,\ndangerous rip currents.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...For the High Surf Advisory, until 9 AM CST Friday. For\nthe High Rip Current Risk, through Friday afternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
                "instruction": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMOB"
                    ],
                    "WMOidentifier": [
                        "WHUS44 KMOB 161809"
                    ],
                    "NWSheadline": [
                        "HIGH SURF ADVISORY REMAINS IN EFFECT UNTIL 9 AM CST FRIDAY... ...HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH FRIDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMOB.SU.Y.0003.000000T0000Z-220218T1500Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T15:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b23c3ac1cb53686e2c4e16f4e7b9077ab00a7032.001.1,2021-03-25T23:20:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b23c3ac1cb53686e2c4e16f4e7b9077ab00a7032.002.1,2021-03-25T23:20:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ddacacf3d797e88decc159c88470e082b57a838e.001.1,2021-03-26T03:47:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6664afbd51c64c56af24c212d26920e36e67e5a6.003.1,2021-03-25T15:23:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6664afbd51c64c56af24c212d26920e36e67e5a6.001.2,2021-03-25T15:23:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c4c59c781bfb78bd1d81ca9e78e4afc457cd58ec.001.2,2021-03-25T07:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c4c59c781bfb78bd1d81ca9e78e4afc457cd58ec.003.1,2021-03-25T07:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1a670f19aa2ad6faa13a8276955ea10cd8b46e39.003.1,2021-03-24T23:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1a670f19aa2ad6faa13a8276955ea10cd8b46e39.001.2,2021-03-24T23:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.d9f3681792f395fdca175f43cd28ebed39d02096.003.1,2021-03-24T22:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.d9f3681792f395fdca175f43cd28ebed39d02096.001.2,2021-03-24T22:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b25cc46937b422c3740c45753ceba3f869cc545d.001.2,2021-03-24T11:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b25cc46937b422c3740c45753ceba3f869cc545d.003.2,2021-03-24T11:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.0c13a71051ed7db6bdc3ee017bc5f556709079d4.003.1,2021-03-24T03:00:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.0c13a71051ed7db6bdc3ee017bc5f556709079d4.001.1,2021-03-24T03:00:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.05cb0a407d1f0fb83bda3b864c631d42154c3137.002.1,2021-03-23T17:39:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.05cb0a407d1f0fb83bda3b864c631d42154c3137.003.1,2021-03-23T17:39:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f9d8f679ba6b2eeabb07a5c2a42ebfa518052f8c.002.1,2021-03-23T12:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f9d8f679ba6b2eeabb07a5c2a42ebfa518052f8c.003.1,2021-03-23T12:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f9d8f679ba6b2eeabb07a5c2a42ebfa518052f8c.001.1,2021-03-23T12:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.05cb0a407d1f0fb83bda3b864c631d42154c3137.001.1,2021-03-23T17:39:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.745a0f206c1113401ceadbffc0cf5dae1c297f32.001.2",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.745a0f206c1113401ceadbffc0cf5dae1c297f32.001.2",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.745a0f206c1113401ceadbffc0cf5dae1c297f32.001.2",
                "areaDesc": "Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal",
                "geocode": {
                    "SAME": [
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091"
                    ],
                    "UGC": [
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.2",
                        "identifier": "urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.2",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-16T05:18:00-06:00"
                    }
                ],
                "sent": "2022-02-16T12:09:00-06:00",
                "effective": "2022-02-16T12:09:00-06:00",
                "onset": "2022-02-16T12:09:00-06:00",
                "expires": "2022-02-16T21:00:00-06:00",
                "ends": "2022-02-18T18:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Rip Current Statement issued February 16 at 12:09PM CST until February 18 at 6:00PM CST by NWS Mobile AL",
                "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet in the surf zone. For the High Rip Current Risk,\ndangerous rip currents.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...For the High Surf Advisory, until 9 AM CST Friday. For\nthe High Rip Current Risk, through Friday afternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
                "instruction": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMOB"
                    ],
                    "WMOidentifier": [
                        "WHUS44 KMOB 161809"
                    ],
                    "NWSheadline": [
                        "HIGH SURF ADVISORY REMAINS IN EFFECT UNTIL 9 AM CST FRIDAY... ...HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH FRIDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMOB.RP.S.0008.000000T0000Z-220219T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-19T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3dc429c9b00a0c60ff397138ce9352800945410e.001.1,2022-02-15T20:53:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aecb79b21e5fbd18a1bbecf8d527f5f5305e33d7.001.1,2022-02-15T14:36:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1,2022-02-15T06:16:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2a5305b47160a52512bb3ba7395c69fa71a97af2.001.1,2021-03-18T20:30:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6bb3237b64ca652552e1ca327bb3f50e3873e0b0.001.1,2021-03-18T13:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.07d5f679a07ad0bdb6dea479165b33a737b48c49.001.1,2021-03-18T07:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de4036736c640d6f0b5f9d9e87e98695c23d319e.001.2,2021-03-18T04:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de2fc2fd97fc7c528c45cc305461931ba9382bc4.001.2,2021-03-17T22:32:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.399e571834bd4ae4951f93da451716fe365f339e.001.2,2021-03-17T22:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4a5af68c839bb4e99c278a27445f04eebfd0a2a0.001.2,2021-03-17T13:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010d1308bcc6460b454359bc55c7060148ca43f2.001.2,2021-03-17T04:18:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.463b9cffee7aefb8f8c95022757378ae9c34fabb.001.1,2021-03-16T22:54:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.14d179a0c147ef83c597c9e785ea2370be07c49f.001.1,2021-03-16T12:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7c901d193de77de5596dcff79fca265f2439f98d.001.1,2021-03-16T04:07:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fda97091cb9d050a8a99d3f7fdb92530e2fed683.001.1,2021-03-15T21:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b90a10b25b8a5b8e4fcd9d01b9fec3e637816d18.001.1,2021-03-15T11:10:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3109fcdd9d541465bfa7b3d45303e8114b3adc6f.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3109fcdd9d541465bfa7b3d45303e8114b3adc6f.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.3109fcdd9d541465bfa7b3d45303e8114b3adc6f.001.1",
                "areaDesc": "Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County",
                "geocode": {
                    "SAME": [
                        "012099",
                        "012011",
                        "012086"
                    ],
                    "UGC": [
                        "FLZ168",
                        "FLZ172",
                        "FLZ173"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173"
                ],
                "references": [],
                "sent": "2022-02-16T07:02:00-05:00",
                "effective": "2022-02-16T07:02:00-05:00",
                "onset": "2022-02-16T07:02:00-05:00",
                "expires": "2022-02-17T07:00:00-05:00",
                "ends": "2022-02-17T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 16 at 7:02AM EST until February 17 at 7:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 161202"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH THURSDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMFL.RP.S.0008.000000T0000Z-220218T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.25e23196da312bd570c5edfb6bb432bdcdf55fd4.001.1,2022-02-15T15:22:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e1730c96e04e901b3239271e0eced1fdefa12dad.001.1,2022-02-15T07:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.002.1,2022-02-14T15:40:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1,2022-02-14T02:30:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.1",
                "areaDesc": "Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal",
                "geocode": {
                    "SAME": [
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091"
                    ],
                    "UGC": [
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206"
                ],
                "references": [],
                "sent": "2022-02-16T05:18:00-06:00",
                "effective": "2022-02-16T05:18:00-06:00",
                "onset": "2022-02-16T12:00:00-06:00",
                "expires": "2022-02-16T13:30:00-06:00",
                "ends": "2022-02-18T09:00:00-06:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Minor",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "High Surf Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "High Surf Advisory issued February 16 at 5:18AM CST until February 18 at 9:00AM CST by NWS Mobile AL",
                "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet expected in the surf zone. For the High Rip Current\nRisk, dangerous rip currents.\n\n* WHERE...In Alabama, Mobile Coastal and Baldwin Coastal\nCounties. In Florida, Escambia Coastal, Santa Rosa Coastal and\nOkaloosa Coastal Counties.\n\n* WHEN...For the High Surf Advisory, from noon today to 9 AM CST\nFriday. For the High Rip Current Risk, through Friday\nafternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
                "instruction": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMOB"
                    ],
                    "WMOidentifier": [
                        "WHUS44 KMOB 161118"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH FRIDAY AFTERNOON... ...HIGH SURF ADVISORY IN EFFECT FROM NOON TODAY TO 9 AM CST FRIDAY"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMOB.SU.Y.0003.220216T1800Z-220218T1500Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T15:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.2",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.2",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.207e12ea3bf20a31a8e2d793cc997cf75823bbd0.001.2",
                "areaDesc": "Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal",
                "geocode": {
                    "SAME": [
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091"
                    ],
                    "UGC": [
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206"
                ],
                "references": [],
                "sent": "2022-02-16T05:18:00-06:00",
                "effective": "2022-02-16T05:18:00-06:00",
                "onset": "2022-02-16T05:18:00-06:00",
                "expires": "2022-02-16T13:30:00-06:00",
                "ends": "2022-02-18T18:00:00-06:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Rip Current Statement issued February 16 at 5:18AM CST until February 18 at 6:00PM CST by NWS Mobile AL",
                "description": "* WHAT...For the High Surf Advisory, large breaking waves of 4\nto 6 feet expected in the surf zone. For the High Rip Current\nRisk, dangerous rip currents.\n\n* WHERE...In Alabama, Mobile Coastal and Baldwin Coastal\nCounties. In Florida, Escambia Coastal, Santa Rosa Coastal and\nOkaloosa Coastal Counties.\n\n* WHEN...For the High Surf Advisory, from noon today to 9 AM CST\nFriday. For the High Rip Current Risk, through Friday\nafternoon.\n\n* IMPACTS...Dangerous swimming and surfing conditions and\nlocalized beach erosion. Rip currents can sweep even the best\nswimmers away from shore into deeper water.",
                "instruction": "Inexperienced swimmers should remain out of the water due to\ndangerous surf conditions.\n\nSwim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMOB"
                    ],
                    "WMOidentifier": [
                        "WHUS44 KMOB 161118"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH FRIDAY AFTERNOON... ...HIGH SURF ADVISORY IN EFFECT FROM NOON TODAY TO 9 AM CST FRIDAY"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMOB.RP.S.0008.000000T0000Z-220219T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-19T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3dc429c9b00a0c60ff397138ce9352800945410e.001.1,2022-02-15T20:53:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aecb79b21e5fbd18a1bbecf8d527f5f5305e33d7.001.1,2022-02-15T14:36:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1,2022-02-15T06:16:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2a5305b47160a52512bb3ba7395c69fa71a97af2.001.1,2021-03-18T20:30:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6bb3237b64ca652552e1ca327bb3f50e3873e0b0.001.1,2021-03-18T13:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.07d5f679a07ad0bdb6dea479165b33a737b48c49.001.1,2021-03-18T07:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de4036736c640d6f0b5f9d9e87e98695c23d319e.001.2,2021-03-18T04:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de2fc2fd97fc7c528c45cc305461931ba9382bc4.001.2,2021-03-17T22:32:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.399e571834bd4ae4951f93da451716fe365f339e.001.2,2021-03-17T22:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4a5af68c839bb4e99c278a27445f04eebfd0a2a0.001.2,2021-03-17T13:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010d1308bcc6460b454359bc55c7060148ca43f2.001.2,2021-03-17T04:18:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.463b9cffee7aefb8f8c95022757378ae9c34fabb.001.1,2021-03-16T22:54:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.14d179a0c147ef83c597c9e785ea2370be07c49f.001.1,2021-03-16T12:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7c901d193de77de5596dcff79fca265f2439f98d.001.1,2021-03-16T04:07:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fda97091cb9d050a8a99d3f7fdb92530e2fed683.001.1,2021-03-15T21:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b90a10b25b8a5b8e4fcd9d01b9fec3e637816d18.001.1,2021-03-15T11:10:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.bfa10e359e331ba4c7bfa95dd7925ebabdfd4442.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.bfa10e359e331ba4c7bfa95dd7925ebabdfd4442.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.bfa10e359e331ba4c7bfa95dd7925ebabdfd4442.001.1",
                "areaDesc": "Southern Brevard County; Indian River; St. Lucie; Martin; Coastal Volusia; Northern Brevard County",
                "geocode": {
                    "SAME": [
                        "012009",
                        "012061",
                        "012111",
                        "012085",
                        "012127"
                    ],
                    "UGC": [
                        "FLZ047",
                        "FLZ054",
                        "FLZ059",
                        "FLZ064",
                        "FLZ141",
                        "FLZ147"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ047",
                    "https://api.weather.gov/zones/forecast/FLZ054",
                    "https://api.weather.gov/zones/forecast/FLZ059",
                    "https://api.weather.gov/zones/forecast/FLZ064",
                    "https://api.weather.gov/zones/forecast/FLZ141",
                    "https://api.weather.gov/zones/forecast/FLZ147"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.07a0de5b9de04c8ce7ac72d539c10be88cc91e76.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.07a0de5b9de04c8ce7ac72d539c10be88cc91e76.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-15T02:55:00-05:00"
                    }
                ],
                "sent": "2022-02-16T02:46:00-05:00",
                "effective": "2022-02-16T02:46:00-05:00",
                "onset": "2022-02-16T02:46:00-05:00",
                "expires": "2022-02-17T04:00:00-05:00",
                "ends": "2022-02-16T03:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Beach Hazards Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Melbourne FL",
                "headline": "Beach Hazards Statement issued February 16 at 2:46AM EST until February 16 at 3:00AM EST by NWS Melbourne FL",
                "description": "* WHAT...Numerous strong, potentially life threatening rip\ncurrents, and gusty easterly winds producing rough surf and\nbreaking waves of 5 to 6 feet.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through late tonight.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water. Entering the surf is strongly\ndiscouraged due to the dangerous surf and rip current\nconditions.",
                "instruction": "Rip currents are powerful channels of water flowing quickly away\nfrom shore, which occur most often at low spots or breaks in the\nsandbar and in the vicinity of structures such as jetties and\npiers. Heed the advice of lifeguards, beach patrol flags and\nsigns.\n\nIf caught in a rip current, relax and float. Don't swim against\nthe current. If able, swim in a direction following the shoreline.\nIf unable to escape, face the shore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMLB"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMLB 160746"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT THROUGH LATE TONIGHT"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXP.KMLB.BH.S.0006.000000T0000Z-220216T0800Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-16T08:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.bfa10e359e331ba4c7bfa95dd7925ebabdfd4442.001.2",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.bfa10e359e331ba4c7bfa95dd7925ebabdfd4442.001.2",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.bfa10e359e331ba4c7bfa95dd7925ebabdfd4442.001.2",
                "areaDesc": "Southern Brevard County; Indian River; St. Lucie; Martin; Coastal Volusia; Northern Brevard County",
                "geocode": {
                    "SAME": [
                        "012009",
                        "012061",
                        "012111",
                        "012085",
                        "012127"
                    ],
                    "UGC": [
                        "FLZ047",
                        "FLZ054",
                        "FLZ059",
                        "FLZ064",
                        "FLZ141",
                        "FLZ147"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ047",
                    "https://api.weather.gov/zones/forecast/FLZ054",
                    "https://api.weather.gov/zones/forecast/FLZ059",
                    "https://api.weather.gov/zones/forecast/FLZ064",
                    "https://api.weather.gov/zones/forecast/FLZ141",
                    "https://api.weather.gov/zones/forecast/FLZ147"
                ],
                "references": [],
                "sent": "2022-02-16T02:46:00-05:00",
                "effective": "2022-02-16T02:46:00-05:00",
                "onset": "2022-02-16T03:00:00-05:00",
                "expires": "2022-02-17T04:00:00-05:00",
                "ends": "2022-02-17T04:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Melbourne FL",
                "headline": "Rip Current Statement issued February 16 at 2:46AM EST until February 17 at 4:00AM EST by NWS Melbourne FL",
                "description": "* WHAT...Numerous strong, potentially life threatening rip\ncurrents, and gusty easterly winds producing rough surf and\nbreaking waves of 5 to 6 feet.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through late tonight.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water. Entering the surf is strongly\ndiscouraged due to the dangerous surf and rip current\nconditions.",
                "instruction": "Rip currents are powerful channels of water flowing quickly away\nfrom shore, which occur most often at low spots or breaks in the\nsandbar and in the vicinity of structures such as jetties and\npiers. Heed the advice of lifeguards, beach patrol flags and\nsigns.\n\nIf caught in a rip current, relax and float. Don't swim against\nthe current. If able, swim in a direction following the shoreline.\nIf unable to escape, face the shore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMLB"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMLB 160746"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT THROUGH LATE TONIGHT"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMLB.RP.S.0008.220216T0800Z-220217T0900Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-17T09:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.c73ce71ff9d352be4da1aaab5fbfeb9c0f85eade.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.c73ce71ff9d352be4da1aaab5fbfeb9c0f85eade.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.c73ce71ff9d352be4da1aaab5fbfeb9c0f85eade.001.1",
                "areaDesc": "Coastal Nassau; Coastal Duval; Coastal St. Johns; Coastal Flagler",
                "geocode": {
                    "SAME": [
                        "012089",
                        "012031",
                        "012109",
                        "012035"
                    ],
                    "UGC": [
                        "FLZ124",
                        "FLZ125",
                        "FLZ133",
                        "FLZ138"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ124",
                    "https://api.weather.gov/zones/forecast/FLZ125",
                    "https://api.weather.gov/zones/forecast/FLZ133",
                    "https://api.weather.gov/zones/forecast/FLZ138"
                ],
                "references": [],
                "sent": "2022-02-16T02:09:00-05:00",
                "effective": "2022-02-16T02:09:00-05:00",
                "onset": "2022-02-16T06:00:00-05:00",
                "expires": "2022-02-17T04:00:00-05:00",
                "ends": "2022-02-17T18:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Jacksonville FL",
                "headline": "Rip Current Statement issued February 16 at 2:09AM EST until February 17 at 6:00PM EST by NWS Jacksonville FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Northeast Florida Beaches.\n\n* WHEN...From 6 AM EST this morning through Thursday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWJAX"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KJAX 160709"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT FROM 6 AM EST THIS MORNING THROUGH THURSDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KJAX.RP.S.0007.220216T1100Z-220217T2300Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-17T23:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.28179b68b7d71a1228361516f69b673a4fe915d9.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.28179b68b7d71a1228361516f69b673a4fe915d9.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.28179b68b7d71a1228361516f69b673a4fe915d9.001.1",
                "areaDesc": "South Walton; Coastal Bay; Coastal Gulf; Coastal Franklin",
                "geocode": {
                    "SAME": [
                        "012131",
                        "012005",
                        "012045",
                        "012037"
                    ],
                    "UGC": [
                        "FLZ108",
                        "FLZ112",
                        "FLZ114",
                        "FLZ115"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ108",
                    "https://api.weather.gov/zones/forecast/FLZ112",
                    "https://api.weather.gov/zones/forecast/FLZ114",
                    "https://api.weather.gov/zones/forecast/FLZ115"
                ],
                "references": [],
                "sent": "2022-02-16T01:51:00-05:00",
                "effective": "2022-02-16T01:51:00-05:00",
                "onset": "2022-02-16T01:51:00-05:00",
                "expires": "2022-02-16T10:00:00-05:00",
                "ends": "2022-02-18T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tallahassee FL",
                "headline": "Rip Current Statement issued February 16 at 1:51AM EST until February 18 at 7:00PM EST by NWS Tallahassee FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Walton, Bay, Gulf, and Franklin County Beaches.\n\n* WHEN...Through Friday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTAE"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTAE 160651"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT THROUGH FRIDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KTAE.RP.S.0021.220216T0651Z-220219T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-19T00:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3dc429c9b00a0c60ff397138ce9352800945410e.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3dc429c9b00a0c60ff397138ce9352800945410e.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.3dc429c9b00a0c60ff397138ce9352800945410e.001.1",
                "areaDesc": "Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal",
                "geocode": {
                    "SAME": [
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091"
                    ],
                    "UGC": [
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.aecb79b21e5fbd18a1bbecf8d527f5f5305e33d7.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.aecb79b21e5fbd18a1bbecf8d527f5f5305e33d7.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-15T14:36:00-06:00"
                    }
                ],
                "sent": "2022-02-15T20:53:00-06:00",
                "effective": "2022-02-15T20:53:00-06:00",
                "onset": "2022-02-15T20:53:00-06:00",
                "expires": "2022-02-16T05:00:00-06:00",
                "ends": "2022-02-18T18:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Rip Current Statement issued February 15 at 8:53PM CST until February 18 at 6:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...From 6 PM CST this evening through Friday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMOB"
                    ],
                    "WMOidentifier": [
                        "WHUS44 KMOB 160253"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH FRIDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMOB.RP.S.0008.000000T0000Z-220219T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-19T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1,2022-02-15T06:16:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2a5305b47160a52512bb3ba7395c69fa71a97af2.001.1,2021-03-18T20:30:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6bb3237b64ca652552e1ca327bb3f50e3873e0b0.001.1,2021-03-18T13:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.07d5f679a07ad0bdb6dea479165b33a737b48c49.001.1,2021-03-18T07:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de4036736c640d6f0b5f9d9e87e98695c23d319e.001.2,2021-03-18T04:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de2fc2fd97fc7c528c45cc305461931ba9382bc4.001.2,2021-03-17T22:32:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.399e571834bd4ae4951f93da451716fe365f339e.001.2,2021-03-17T22:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4a5af68c839bb4e99c278a27445f04eebfd0a2a0.001.2,2021-03-17T13:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010d1308bcc6460b454359bc55c7060148ca43f2.001.2,2021-03-17T04:18:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.463b9cffee7aefb8f8c95022757378ae9c34fabb.001.1,2021-03-16T22:54:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.14d179a0c147ef83c597c9e785ea2370be07c49f.001.1,2021-03-16T12:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7c901d193de77de5596dcff79fca265f2439f98d.001.1,2021-03-16T04:07:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fda97091cb9d050a8a99d3f7fdb92530e2fed683.001.1,2021-03-15T21:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b90a10b25b8a5b8e4fcd9d01b9fec3e637816d18.001.1,2021-03-15T11:10:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.aecb79b21e5fbd18a1bbecf8d527f5f5305e33d7.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.aecb79b21e5fbd18a1bbecf8d527f5f5305e33d7.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.aecb79b21e5fbd18a1bbecf8d527f5f5305e33d7.001.1",
                "areaDesc": "Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal",
                "geocode": {
                    "SAME": [
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091"
                    ],
                    "UGC": [
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-15T06:16:00-06:00"
                    }
                ],
                "sent": "2022-02-15T14:36:00-06:00",
                "effective": "2022-02-15T14:36:00-06:00",
                "onset": "2022-02-15T18:00:00-06:00",
                "expires": "2022-02-15T22:45:00-06:00",
                "ends": "2022-02-18T18:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Rip Current Statement issued February 15 at 2:36PM CST until February 18 at 6:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...In Alabama, Baldwin Coastal and Mobile Coastal\nCounties. In Florida, Santa Rosa Coastal, Okaloosa Coastal and\nEscambia Coastal Counties.\n\n* WHEN...From 6 PM CST this evening through Friday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMOB"
                    ],
                    "WMOidentifier": [
                        "WHUS44 KMOB 152036"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT FROM 6 PM CST THIS EVENING THROUGH FRIDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMOB.RP.S.0008.220216T0000Z-220219T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-19T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2a5305b47160a52512bb3ba7395c69fa71a97af2.001.1,2021-03-18T20:30:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6bb3237b64ca652552e1ca327bb3f50e3873e0b0.001.1,2021-03-18T13:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.07d5f679a07ad0bdb6dea479165b33a737b48c49.001.1,2021-03-18T07:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de4036736c640d6f0b5f9d9e87e98695c23d319e.001.2,2021-03-18T04:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.399e571834bd4ae4951f93da451716fe365f339e.001.2,2021-03-17T22:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.de2fc2fd97fc7c528c45cc305461931ba9382bc4.001.2,2021-03-17T22:32:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4a5af68c839bb4e99c278a27445f04eebfd0a2a0.001.2,2021-03-17T13:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010d1308bcc6460b454359bc55c7060148ca43f2.001.2,2021-03-17T04:18:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.463b9cffee7aefb8f8c95022757378ae9c34fabb.001.1,2021-03-16T22:54:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.14d179a0c147ef83c597c9e785ea2370be07c49f.001.1,2021-03-16T12:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7c901d193de77de5596dcff79fca265f2439f98d.001.1,2021-03-16T04:07:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fda97091cb9d050a8a99d3f7fdb92530e2fed683.001.1,2021-03-15T21:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b90a10b25b8a5b8e4fcd9d01b9fec3e637816d18.001.1,2021-03-15T11:10:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.25e23196da312bd570c5edfb6bb432bdcdf55fd4.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.25e23196da312bd570c5edfb6bb432bdcdf55fd4.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.25e23196da312bd570c5edfb6bb432bdcdf55fd4.001.1",
                "areaDesc": "Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County",
                "geocode": {
                    "SAME": [
                        "012099",
                        "012011",
                        "012086"
                    ],
                    "UGC": [
                        "FLZ168",
                        "FLZ172",
                        "FLZ173"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e1730c96e04e901b3239271e0eced1fdefa12dad.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.e1730c96e04e901b3239271e0eced1fdefa12dad.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-15T07:19:00-05:00"
                    }
                ],
                "sent": "2022-02-15T15:22:00-05:00",
                "effective": "2022-02-15T15:22:00-05:00",
                "onset": "2022-02-15T15:22:00-05:00",
                "expires": "2022-02-16T07:00:00-05:00",
                "ends": "2022-02-17T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 15 at 3:22PM EST until February 17 at 7:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 152022"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH THURSDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMFL.RP.S.0008.000000T0000Z-220218T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.002.1,2022-02-14T15:40:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1,2022-02-14T02:30:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.6349658ee2a6d3624f8ce4d5eee0a807ef6b62ca.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.6349658ee2a6d3624f8ce4d5eee0a807ef6b62ca.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.6349658ee2a6d3624f8ce4d5eee0a807ef6b62ca.001.1",
                "areaDesc": "Laguna Madre From the Port Of Brownsville to the Arroyo Colorado; Coastal waters from Port Mansfield TX to the Rio Grande River out 20 NM; Coastal waters from Baffin Bay to Port Mansfield TX out 20 NM; Bays and Waterways from Baffin Bay to Port Aransas; Bays and Waterways from Port Aransas to Port O'Connor; Coastal waters from Baffin Bay to Port Aransas out 20 NM; Coastal waters from Port Aransas to Matagorda Ship Channel out 20 NM; Matagorda Bay; Galveston Bay; Coastal waters from Freeport to Matagorda Ship Channel TX out 20 NM; Coastal waters from High Island to Freeport TX out 20 NM; Coastal waters from Cameron LA to High Island TX out 20 NM; Coastal waters from Intracoastal City to Cameron LA out 20 NM; Coastal waters from Lower Atchafalaya River to Intracoastal City LA out 20 NM; Mississippi Sound; Lake Borgne; Chandeleur Sound; Breton Sound; Coastal Waters from Port Fourchon LA to Lower Atchafalaya River LA out 20 nm; Coastal waters from the Southwest Pass of the Mississippi River to Port Fourchon Louisiana out 20 NM; Coastal Waters from Boothville LA to Southwest Pass of the Mississippi River out 20 nm; North Mobile Bay; South Mobile Bay; Mississippi Sound; Perdido Bay Area; Pensacola Bay Area including Santa Rosa Sound; Western Choctawhatchee Bay; Coastal waters from Pensacola FL to Pascagoula MS out 20 NM; Coastal waters from Okaloosa-Walton County Line to Pensacola FL out 20 NM; Apalachee Bay or Coastal Waters From Keaton Beach to Ochlockonee River Fl out to 20 Nm; Coastal waters from Okaloosa-Walton County Line to Mexico Beach out 20 NM; Coastal Waters From  Ochlockonee River to Apalachicola FL out to 20 Nm; Coastal waters from  Suwannee River to Keaton Beach out 20 NM; Tampa Bay waters; Charlotte Harbor and Pine Island Sound; Coastal waters from Tarpon Springs to Suwannee River FL out 20 NM; Coastal waters from Englewood to Tarpon Springs FL out 20 NM; Coastal waters from Bonita Beach to Englewood FL out 20 NM; Coastal waters from Chokoloskee to Bonita Beach FL out 20 NM; Coastal waters from East Cape Sable to Chokoloskee FL out 20 NM; Florida Bay including Barnes Sound, Blackwater Sound, and Buttonwood Sound; Bayside and Gulf side from Craig Key to West End of Seven Mile Bridge; Gulf of Mexico including Dry Tortugas and Rebecca Shoal Channel; Gulf of Mexico from West End of Seven Mile Bridge to Halfmoon Shoal out to 5 Fathoms; Hawk Channel from Ocean Reef to Craig Key out to the reef; Hawk Channel from Craig Key to west end of Seven Mile Bridge out to the reef; Hawk Channel from west end of Seven Mile Bridge to Halfmoon Shoal out to the reef; Straits of Florida from Ocean Reef to Craig Key out 20 NM; Straits of Florida from Craig Key to west end of Seven Mile Bridge out 20 NM; Straits of Florida from west end of Seven Mile Bridge to south of Halfmoon Shoal out 20 NM; Straits of Florida from Halfmoon Shoal to 20 NM west of Dry Tortugas out 20 NM; Biscayne Bay; Coastal waters from Jupiter Inlet to Deerfield Beach FL out 20 NM; Coastal waters from Deerfield Beach to Ocean Reef FL out 20 NM; Flagler Beach to Volusia-Brevard County Line 0-20 nm; Volusia-Brevard County Line to Sebastian Inlet 0-20 nm; Sebastian Inlet to Jupiter Inlet 0-20 nm; Coastal waters from Altamaha Sound to Fernandina Beach FL out 20 NM; Coastal waters from Fernandina Beach to St. Augustine FL out 20 NM; Coastal waters from St. Augustine to Flagler Beach FL out 20 NM; Charleston Harbor; Coastal waters from South Santee River to Edisto Beach SC out 20 nm; Coastal waters from Edisto Beach SC to Savannah GA out 20 nm; Coastal waters from Savannah GA to Altamaha Sound GA out 20 nm ...including Grays Reef National Marine Sanctuary; Coastal waters from Surf City to Cape Fear NC out 20 nm; Coastal waters from Cape Fear NC to Little River Inlet SC out 20 nm; Coastal waters from Little River Inlet to Murrells Inlet SC out 20 nm; Coastal waters from Murrells Inlet to South Santee River SC out 20 nm; Pamlico Sound; S of Currituck Beach Light NC to Oregon Inlet NC out to 20 nm; S of Oregon Inlet NC to Cape Hatteras NC out to 20 nm; S of Cape Hatteras NC to Ocracoke Inlet NC out to 20 nm; S of Ocracoke Inlet NC to Cape Lookout NC out to 20 nm; S of Cape Lookout NC to Surf City NC out to 20 nm; Chesapeake Bay from Windmill Point to New Point Comfort VA; Chesapeake Bay from New Point Comfort to Little Creek VA; Currituck Sound; Chesapeake Bay from Little Creek VA to Cape Henry VA including the Chesapeake Bay Bridge Tunnel; Rappahannock River from Urbanna to Windmill Point; York River; James River from Jamestown to the James River Bridge; James River from James River Bridge to Hampton Roads Bridge-Tunnel; Coastal Waters from Cape Charles Light to Virginia-North Carolina border out to 20 nm; Coastal waters from NC VA border to Currituck Beach Light NC out 20 nm; Coastal waters from Fenwick Island DE to Chincoteague VA out 20 nm; Coastal waters from Chincoteague to Parramore Island VA out 20 nm; Coastal waters from Parramore Island to Cape Charles Light VA out 20 nm; Delaware Bay waters north of East Point NJ to Slaughter Beach DE; Delaware Bay waters south of East Point NJ to Slaughter Beach DE; Coastal waters from Sandy Hook to Manasquan Inlet NJ out 20 nm; Coastal waters from Manasquan Inlet to Little Egg Inlet NJ out 20 nm; Coastal waters from Little Egg Inlet to Great Egg Inlet NJ out 20 nm; Coastal waters from Great Egg Inlet to Cape May NJ out 20 nm; Coastal waters from Cape May NJ to Cape Henlopen DE out 20 nm; Coastal waters from Cape Henlopen to Fenwick Island DE out 20 nm; Long Island Sound West of New Haven CT/Port Jefferson NY; New York Harbor; Peconic and Gardiners Bays; South Shore Bays from Jones Inlet through Shinnecock Bay; Moriches Inlet NY to Montauk Point NY out 20 nm; Fire Island Inlet NY to Moriches Inlet NY out 20 nm; Sandy Hook NJ to Fire Island Inlet NY out 20 nm; Boston Harbor; Cape Cod Bay; Nantucket Sound; Vineyard Sound; Buzzards Bay; Rhode Island Sound; Narragansett Bay; Block Island Sound; Coastal waters east of Ipswich Bay and the Stellwagen Bank National Marine Sanctuary; Coastal waters from Provincetown MA to Chatham MA to Nantucket MA out 20 nm; Coastal Waters extending out to 25 nm South of Marthas Vineyard and Nantucket; Coastal Waters from Montauk NY to Marthas Vineyard extending out to 20 nm South of Block Island; Coastal Waters from Stonington, ME to Port Clyde, ME out 25 NM; Coastal Waters from Eastport, ME to Schoodic Point, ME out 25 NM; Coastal Waters from Schoodic Point, ME to Stonington, ME out 25 NM; Intra Coastal Waters from Schoodic Point, ME to Stonington, ME; Inland Kenedy; Coastal Willacy; Coastal Cameron; Inland Kleberg; Inland Nueces; Inland San Patricio; Coastal Aransas; Inland Refugio; Inland Calhoun; Inland Harris; Chambers; Inland Matagorda; Inland Brazoria; Inland Galveston; Jefferson; Orange; Vermilion; Iberia; St. Mary; West Cameron; East Cameron; Upper St. Bernard; Lower Terrebonne; Lower Lafourche; Lower Jefferson; Lower Plaquemines; Lower St. Bernard; Hancock; Harrison; Jackson; Mobile Central; Baldwin Central; Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal; Central Walton; Inland Bay; Inland Gulf; Inland Franklin; Inland Jefferson; Inland Wakulla; Inland Taylor; Inland Dixie; Coastal Levy; Coastal Citrus; Coastal Hernando; Coastal Pasco; Pinellas; Coastal Hillsborough; Coastal Manatee; Coastal Sarasota; Coastal Charlotte; Coastal Lee; Coastal Collier County; Inland Collier County; Mainland Monroe; Monroe Upper Keys; Monroe Middle Keys; Monroe Lower Keys; Far South Miami-Dade County; Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County; Southern Brevard County; Indian River; St. Lucie; Martin; Coastal Volusia; Northern Brevard County; Coastal Nassau; Coastal Duval; Inland St. Johns; Inland Flagler; Coastal Glynn; Coastal Camden; Coastal Bryan; Coastal Chatham; Coastal Liberty; Coastal McIntosh; Beaufort; Coastal Colleton; Charleston; Coastal Jasper; Tidal Berkeley; Coastal Horry; Coastal Georgetown; Coastal Pender; Coastal New Hanover; Coastal Brunswick; Washington; Tyrrell; Mainland Dare; Beaufort; Mainland Hyde; Pamlico; Pasquotank; Camden; Western Currituck; Bertie; Chowan; Perquimans; Eastern Currituck; Gloucester; Mathews; Norfolk/Portsmouth; Virginia Beach; Accomack; Northampton; Maryland Beaches; Kent; Inland Sussex; Delaware Beaches; Hudson; Middlesex; Western Monmouth; Eastern Monmouth; Cumberland; Cape May; Atlantic Coastal Cape May; Coastal Atlantic; Coastal Ocean; Eastern Essex; Eastern Union; Southern Westchester; New York (Manhattan); Bronx; Richmond (Staten Is.); Kings (Brooklyn); Northwest Suffolk; Northeast Suffolk; Southwest Suffolk; Southeast Suffolk; Northern Queens; Northern Nassau; Southern Queens; Southern Nassau; Southern Fairfield; Southern New Haven; Southern Middlesex; Southern New London; Southeast Providence; Eastern Kent; Bristol; Washington; Newport; Block Island; Eastern Essex; Eastern Norfolk; Eastern Plymouth; Southern Bristol; Southern Plymouth; Barnstable; Dukes; Nantucket; Coastal Rockingham; Interior Waldo; Coastal York; Coastal Cumberland; Sagadahoc; Lincoln; Knox; Coastal Waldo; Coastal Hancock; Coastal Washington",
                "geocode": {
                    "SAME": [
                        "077130",
                        "077150",
                        "077155",
                        "077230",
                        "077235",
                        "077250",
                        "077255",
                        "077330",
                        "077335",
                        "077350",
                        "077355",
                        "077450",
                        "077452",
                        "077455",
                        "077532",
                        "077534",
                        "077536",
                        "077538",
                        "077550",
                        "077552",
                        "077555",
                        "077630",
                        "077631",
                        "077632",
                        "077633",
                        "077634",
                        "077635",
                        "077650",
                        "077655",
                        "077730",
                        "077750",
                        "077755",
                        "077765",
                        "077830",
                        "077836",
                        "077850",
                        "077853",
                        "077856",
                        "077656",
                        "077657",
                        "077031",
                        "077032",
                        "077034",
                        "077035",
                        "077042",
                        "077043",
                        "077044",
                        "077052",
                        "077053",
                        "077054",
                        "077055",
                        "075630",
                        "075650",
                        "075651",
                        "075550",
                        "075552",
                        "075555",
                        "075450",
                        "075452",
                        "075454",
                        "075330",
                        "075350",
                        "075352",
                        "075354",
                        "075250",
                        "075252",
                        "075254",
                        "075256",
                        "075135",
                        "075150",
                        "075152",
                        "075154",
                        "075156",
                        "075158",
                        "073631",
                        "073632",
                        "073633",
                        "073634",
                        "073635",
                        "073636",
                        "073637",
                        "073638",
                        "073656",
                        "073658",
                        "073650",
                        "073652",
                        "073654",
                        "073430",
                        "073431",
                        "073450",
                        "073451",
                        "073452",
                        "073453",
                        "073454",
                        "073455",
                        "073335",
                        "073338",
                        "073340",
                        "073345",
                        "073350",
                        "073353",
                        "073355",
                        "073230",
                        "073231",
                        "073232",
                        "073233",
                        "073234",
                        "073235",
                        "073236",
                        "073237",
                        "073250",
                        "073254",
                        "073255",
                        "073256",
                        "073150",
                        "073050",
                        "073051",
                        "073052",
                        "048261",
                        "048489",
                        "048061",
                        "048273",
                        "048355",
                        "048409",
                        "048007",
                        "048391",
                        "048057",
                        "048201",
                        "048071",
                        "048321",
                        "048039",
                        "048167",
                        "048245",
                        "048361",
                        "022113",
                        "022045",
                        "022101",
                        "022023",
                        "022087",
                        "022109",
                        "022057",
                        "022051",
                        "022075",
                        "028045",
                        "028047",
                        "028059",
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091",
                        "012131",
                        "012005",
                        "012045",
                        "012037",
                        "012065",
                        "012129",
                        "012123",
                        "012029",
                        "012075",
                        "012017",
                        "012053",
                        "012101",
                        "012103",
                        "012057",
                        "012081",
                        "012115",
                        "012015",
                        "012071",
                        "012021",
                        "012087",
                        "012086",
                        "012099",
                        "012011",
                        "012009",
                        "012061",
                        "012111",
                        "012085",
                        "012127",
                        "012089",
                        "012031",
                        "012109",
                        "012035",
                        "013127",
                        "013039",
                        "013029",
                        "013051",
                        "013179",
                        "013191",
                        "045013",
                        "045029",
                        "045019",
                        "045053",
                        "045015",
                        "045051",
                        "045043",
                        "037141",
                        "037129",
                        "037019",
                        "037187",
                        "037177",
                        "037055",
                        "037013",
                        "037095",
                        "037137",
                        "037139",
                        "037029",
                        "037053",
                        "037015",
                        "037041",
                        "037143",
                        "051073",
                        "051115",
                        "051710",
                        "051740",
                        "051810",
                        "051001",
                        "051131",
                        "024047",
                        "010001",
                        "010005",
                        "034017",
                        "034023",
                        "034025",
                        "034011",
                        "034009",
                        "034001",
                        "034029",
                        "034013",
                        "034039",
                        "036119",
                        "036061",
                        "036005",
                        "036085",
                        "036047",
                        "036103",
                        "036081",
                        "036059",
                        "009001",
                        "009009",
                        "009007",
                        "009011",
                        "044007",
                        "044003",
                        "044001",
                        "044009",
                        "044005",
                        "025009",
                        "025021",
                        "025023",
                        "025005",
                        "025001",
                        "025007",
                        "025019",
                        "033015",
                        "023027",
                        "023031",
                        "023005",
                        "023023",
                        "023015",
                        "023013",
                        "023009",
                        "023029"
                    ],
                    "UGC": [
                        "GMZ130",
                        "GMZ150",
                        "GMZ155",
                        "GMZ230",
                        "GMZ235",
                        "GMZ250",
                        "GMZ255",
                        "GMZ330",
                        "GMZ335",
                        "GMZ350",
                        "GMZ355",
                        "GMZ450",
                        "GMZ452",
                        "GMZ455",
                        "GMZ532",
                        "GMZ534",
                        "GMZ536",
                        "GMZ538",
                        "GMZ550",
                        "GMZ552",
                        "GMZ555",
                        "GMZ630",
                        "GMZ631",
                        "GMZ632",
                        "GMZ633",
                        "GMZ634",
                        "GMZ635",
                        "GMZ650",
                        "GMZ655",
                        "GMZ730",
                        "GMZ750",
                        "GMZ755",
                        "GMZ765",
                        "GMZ830",
                        "GMZ836",
                        "GMZ850",
                        "GMZ853",
                        "GMZ856",
                        "GMZ656",
                        "GMZ657",
                        "GMZ031",
                        "GMZ032",
                        "GMZ034",
                        "GMZ035",
                        "GMZ042",
                        "GMZ043",
                        "GMZ044",
                        "GMZ052",
                        "GMZ053",
                        "GMZ054",
                        "GMZ055",
                        "AMZ630",
                        "AMZ650",
                        "AMZ651",
                        "AMZ550",
                        "AMZ552",
                        "AMZ555",
                        "AMZ450",
                        "AMZ452",
                        "AMZ454",
                        "AMZ330",
                        "AMZ350",
                        "AMZ352",
                        "AMZ354",
                        "AMZ250",
                        "AMZ252",
                        "AMZ254",
                        "AMZ256",
                        "AMZ130",
                        "AMZ135",
                        "AMZ150",
                        "AMZ152",
                        "AMZ154",
                        "AMZ156",
                        "AMZ158",
                        "ANZ631",
                        "ANZ632",
                        "ANZ633",
                        "ANZ634",
                        "ANZ635",
                        "ANZ636",
                        "ANZ637",
                        "ANZ638",
                        "ANZ656",
                        "ANZ658",
                        "ANZ650",
                        "ANZ652",
                        "ANZ654",
                        "ANZ430",
                        "ANZ431",
                        "ANZ450",
                        "ANZ451",
                        "ANZ452",
                        "ANZ453",
                        "ANZ454",
                        "ANZ455",
                        "ANZ330",
                        "ANZ335",
                        "ANZ338",
                        "ANZ340",
                        "ANZ345",
                        "ANZ350",
                        "ANZ353",
                        "ANZ355",
                        "ANZ230",
                        "ANZ231",
                        "ANZ232",
                        "ANZ233",
                        "ANZ234",
                        "ANZ235",
                        "ANZ236",
                        "ANZ237",
                        "ANZ250",
                        "ANZ254",
                        "ANZ255",
                        "ANZ256",
                        "ANZ150",
                        "ANZ050",
                        "ANZ051",
                        "ANZ052",
                        "TXZ251",
                        "TXZ256",
                        "TXZ257",
                        "TXZ242",
                        "TXZ243",
                        "TXZ244",
                        "TXZ245",
                        "TXZ246",
                        "TXZ247",
                        "TXZ213",
                        "TXZ214",
                        "TXZ236",
                        "TXZ237",
                        "TXZ238",
                        "TXZ215",
                        "TXZ216",
                        "LAZ052",
                        "LAZ053",
                        "LAZ054",
                        "LAZ073",
                        "LAZ074",
                        "LAZ040",
                        "LAZ062",
                        "LAZ064",
                        "LAZ066",
                        "LAZ067",
                        "LAZ068",
                        "LAZ069",
                        "LAZ070",
                        "MSZ080",
                        "MSZ081",
                        "MSZ082",
                        "ALZ263",
                        "ALZ264",
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206",
                        "FLZ008",
                        "FLZ012",
                        "FLZ014",
                        "FLZ015",
                        "FLZ018",
                        "FLZ027",
                        "FLZ028",
                        "FLZ034",
                        "FLZ139",
                        "FLZ142",
                        "FLZ148",
                        "FLZ149",
                        "FLZ050",
                        "FLZ151",
                        "FLZ155",
                        "FLZ160",
                        "FLZ162",
                        "FLZ165",
                        "FLZ069",
                        "FLZ070",
                        "FLZ075",
                        "FLZ076",
                        "FLZ077",
                        "FLZ078",
                        "FLZ174",
                        "FLZ168",
                        "FLZ172",
                        "FLZ173",
                        "FLZ047",
                        "FLZ054",
                        "FLZ059",
                        "FLZ064",
                        "FLZ141",
                        "FLZ147",
                        "FLZ124",
                        "FLZ125",
                        "FLZ033",
                        "FLZ038",
                        "GAZ154",
                        "GAZ166",
                        "GAZ117",
                        "GAZ119",
                        "GAZ139",
                        "GAZ141",
                        "SCZ048",
                        "SCZ049",
                        "SCZ050",
                        "SCZ051",
                        "SCZ052",
                        "SCZ054",
                        "SCZ056",
                        "NCZ106",
                        "NCZ108",
                        "NCZ110",
                        "NCZ045",
                        "NCZ046",
                        "NCZ047",
                        "NCZ080",
                        "NCZ081",
                        "NCZ094",
                        "NCZ095",
                        "NCZ098",
                        "NCZ103",
                        "NCZ104",
                        "NCZ015",
                        "NCZ016",
                        "NCZ017",
                        "NCZ030",
                        "NCZ031",
                        "NCZ032",
                        "NCZ102",
                        "VAZ084",
                        "VAZ086",
                        "VAZ091",
                        "VAZ094",
                        "VAZ095",
                        "VAZ098",
                        "VAZ099",
                        "VAZ100",
                        "MDZ025",
                        "DEZ002",
                        "DEZ003",
                        "DEZ004",
                        "NJZ006",
                        "NJZ012",
                        "NJZ013",
                        "NJZ014",
                        "NJZ021",
                        "NJZ023",
                        "NJZ024",
                        "NJZ025",
                        "NJZ026",
                        "NJZ106",
                        "NJZ108",
                        "NYZ071",
                        "NYZ072",
                        "NYZ073",
                        "NYZ074",
                        "NYZ075",
                        "NYZ078",
                        "NYZ079",
                        "NYZ080",
                        "NYZ081",
                        "NYZ176",
                        "NYZ177",
                        "NYZ178",
                        "NYZ179",
                        "CTZ009",
                        "CTZ010",
                        "CTZ011",
                        "CTZ012",
                        "RIZ002",
                        "RIZ004",
                        "RIZ005",
                        "RIZ006",
                        "RIZ007",
                        "RIZ008",
                        "MAZ007",
                        "MAZ016",
                        "MAZ019",
                        "MAZ020",
                        "MAZ021",
                        "MAZ022",
                        "MAZ023",
                        "MAZ024",
                        "NHZ014",
                        "MEZ022",
                        "MEZ023",
                        "MEZ024",
                        "MEZ025",
                        "MEZ026",
                        "MEZ027",
                        "MEZ028",
                        "MEZ029",
                        "MEZ030",
                        "NBZ570",
                        "NBZ550",
                        "NBZ660",
                        "NBZ641",
                        "NSZ210",
                        "NSZ230",
                        "NSZ260",
                        "NSZ250",
                        "NSZ110",
                        "NSZ120",
                        "NSZ130",
                        "NSZ170",
                        "NSZ160",
                        "NSZ150",
                        "NSZ140",
                        "NSZ270",
                        "NSZ280",
                        "NSZ320",
                        "NSZ410",
                        "NSZ450",
                        "NSZ440",
                        "NSZ430",
                        "QCZ670",
                        "QCZ680",
                        "NLZ340",
                        "NLZ220",
                        "NLZ230",
                        "NLZ210",
                        "NLZ120",
                        "NLZ132",
                        "NLZ140",
                        "NLZ241",
                        "NLZ242",
                        "NLZ110",
                        "NLZ131",
                        "NLZ540",
                        "NLZ530",
                        "NLZ570",
                        "NLZ520",
                        "NLZ510",
                        "NLZ560",
                        "NLZ610",
                        "NLZ720",
                        "NLZ710",
                        "NLZ730",
                        "NLZ740",
                        "NLZ750",
                        "NLZ760",
                        "NLZ770"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/GMZ130",
                    "https://api.weather.gov/zones/forecast/GMZ150",
                    "https://api.weather.gov/zones/forecast/GMZ155",
                    "https://api.weather.gov/zones/forecast/GMZ230",
                    "https://api.weather.gov/zones/forecast/GMZ235",
                    "https://api.weather.gov/zones/forecast/GMZ250",
                    "https://api.weather.gov/zones/forecast/GMZ255",
                    "https://api.weather.gov/zones/forecast/GMZ330",
                    "https://api.weather.gov/zones/forecast/GMZ335",
                    "https://api.weather.gov/zones/forecast/GMZ350",
                    "https://api.weather.gov/zones/forecast/GMZ355",
                    "https://api.weather.gov/zones/forecast/GMZ450",
                    "https://api.weather.gov/zones/forecast/GMZ452",
                    "https://api.weather.gov/zones/forecast/GMZ455",
                    "https://api.weather.gov/zones/forecast/GMZ532",
                    "https://api.weather.gov/zones/forecast/GMZ534",
                    "https://api.weather.gov/zones/forecast/GMZ536",
                    "https://api.weather.gov/zones/forecast/GMZ538",
                    "https://api.weather.gov/zones/forecast/GMZ550",
                    "https://api.weather.gov/zones/forecast/GMZ552",
                    "https://api.weather.gov/zones/forecast/GMZ555",
                    "https://api.weather.gov/zones/forecast/GMZ630",
                    "https://api.weather.gov/zones/forecast/GMZ631",
                    "https://api.weather.gov/zones/forecast/GMZ632",
                    "https://api.weather.gov/zones/forecast/GMZ633",
                    "https://api.weather.gov/zones/forecast/GMZ634",
                    "https://api.weather.gov/zones/forecast/GMZ635",
                    "https://api.weather.gov/zones/forecast/GMZ650",
                    "https://api.weather.gov/zones/forecast/GMZ655",
                    "https://api.weather.gov/zones/forecast/GMZ730",
                    "https://api.weather.gov/zones/forecast/GMZ750",
                    "https://api.weather.gov/zones/forecast/GMZ755",
                    "https://api.weather.gov/zones/forecast/GMZ765",
                    "https://api.weather.gov/zones/forecast/GMZ830",
                    "https://api.weather.gov/zones/forecast/GMZ836",
                    "https://api.weather.gov/zones/forecast/GMZ850",
                    "https://api.weather.gov/zones/forecast/GMZ853",
                    "https://api.weather.gov/zones/forecast/GMZ856",
                    "https://api.weather.gov/zones/forecast/GMZ656",
                    "https://api.weather.gov/zones/forecast/GMZ657",
                    "https://api.weather.gov/zones/forecast/GMZ031",
                    "https://api.weather.gov/zones/forecast/GMZ032",
                    "https://api.weather.gov/zones/forecast/GMZ034",
                    "https://api.weather.gov/zones/forecast/GMZ035",
                    "https://api.weather.gov/zones/forecast/GMZ042",
                    "https://api.weather.gov/zones/forecast/GMZ043",
                    "https://api.weather.gov/zones/forecast/GMZ044",
                    "https://api.weather.gov/zones/forecast/GMZ052",
                    "https://api.weather.gov/zones/forecast/GMZ053",
                    "https://api.weather.gov/zones/forecast/GMZ054",
                    "https://api.weather.gov/zones/forecast/GMZ055",
                    "https://api.weather.gov/zones/forecast/AMZ630",
                    "https://api.weather.gov/zones/forecast/AMZ650",
                    "https://api.weather.gov/zones/forecast/AMZ651",
                    "https://api.weather.gov/zones/forecast/AMZ550",
                    "https://api.weather.gov/zones/forecast/AMZ552",
                    "https://api.weather.gov/zones/forecast/AMZ555",
                    "https://api.weather.gov/zones/forecast/AMZ450",
                    "https://api.weather.gov/zones/forecast/AMZ452",
                    "https://api.weather.gov/zones/forecast/AMZ454",
                    "https://api.weather.gov/zones/forecast/AMZ330",
                    "https://api.weather.gov/zones/forecast/AMZ350",
                    "https://api.weather.gov/zones/forecast/AMZ352",
                    "https://api.weather.gov/zones/forecast/AMZ354",
                    "https://api.weather.gov/zones/forecast/AMZ250",
                    "https://api.weather.gov/zones/forecast/AMZ252",
                    "https://api.weather.gov/zones/forecast/AMZ254",
                    "https://api.weather.gov/zones/forecast/AMZ256",
                    "https://api.weather.gov/zones/forecast/AMZ130",
                    "https://api.weather.gov/zones/forecast/AMZ135",
                    "https://api.weather.gov/zones/forecast/AMZ150",
                    "https://api.weather.gov/zones/forecast/AMZ152",
                    "https://api.weather.gov/zones/forecast/AMZ154",
                    "https://api.weather.gov/zones/forecast/AMZ156",
                    "https://api.weather.gov/zones/forecast/AMZ158",
                    "https://api.weather.gov/zones/forecast/ANZ631",
                    "https://api.weather.gov/zones/forecast/ANZ632",
                    "https://api.weather.gov/zones/forecast/ANZ633",
                    "https://api.weather.gov/zones/forecast/ANZ634",
                    "https://api.weather.gov/zones/forecast/ANZ635",
                    "https://api.weather.gov/zones/forecast/ANZ636",
                    "https://api.weather.gov/zones/forecast/ANZ637",
                    "https://api.weather.gov/zones/forecast/ANZ638",
                    "https://api.weather.gov/zones/forecast/ANZ656",
                    "https://api.weather.gov/zones/forecast/ANZ658",
                    "https://api.weather.gov/zones/forecast/ANZ650",
                    "https://api.weather.gov/zones/forecast/ANZ652",
                    "https://api.weather.gov/zones/forecast/ANZ654",
                    "https://api.weather.gov/zones/forecast/ANZ430",
                    "https://api.weather.gov/zones/forecast/ANZ431",
                    "https://api.weather.gov/zones/forecast/ANZ450",
                    "https://api.weather.gov/zones/forecast/ANZ451",
                    "https://api.weather.gov/zones/forecast/ANZ452",
                    "https://api.weather.gov/zones/forecast/ANZ453",
                    "https://api.weather.gov/zones/forecast/ANZ454",
                    "https://api.weather.gov/zones/forecast/ANZ455",
                    "https://api.weather.gov/zones/forecast/ANZ330",
                    "https://api.weather.gov/zones/forecast/ANZ335",
                    "https://api.weather.gov/zones/forecast/ANZ338",
                    "https://api.weather.gov/zones/forecast/ANZ340",
                    "https://api.weather.gov/zones/forecast/ANZ345",
                    "https://api.weather.gov/zones/forecast/ANZ350",
                    "https://api.weather.gov/zones/forecast/ANZ353",
                    "https://api.weather.gov/zones/forecast/ANZ355",
                    "https://api.weather.gov/zones/forecast/ANZ230",
                    "https://api.weather.gov/zones/forecast/ANZ231",
                    "https://api.weather.gov/zones/forecast/ANZ232",
                    "https://api.weather.gov/zones/forecast/ANZ233",
                    "https://api.weather.gov/zones/forecast/ANZ234",
                    "https://api.weather.gov/zones/forecast/ANZ235",
                    "https://api.weather.gov/zones/forecast/ANZ236",
                    "https://api.weather.gov/zones/forecast/ANZ237",
                    "https://api.weather.gov/zones/forecast/ANZ250",
                    "https://api.weather.gov/zones/forecast/ANZ254",
                    "https://api.weather.gov/zones/forecast/ANZ255",
                    "https://api.weather.gov/zones/forecast/ANZ256",
                    "https://api.weather.gov/zones/forecast/ANZ150",
                    "https://api.weather.gov/zones/forecast/ANZ050",
                    "https://api.weather.gov/zones/forecast/ANZ051",
                    "https://api.weather.gov/zones/forecast/ANZ052",
                    "https://api.weather.gov/zones/forecast/TXZ251",
                    "https://api.weather.gov/zones/forecast/TXZ256",
                    "https://api.weather.gov/zones/forecast/TXZ257",
                    "https://api.weather.gov/zones/forecast/TXZ242",
                    "https://api.weather.gov/zones/forecast/TXZ243",
                    "https://api.weather.gov/zones/forecast/TXZ244",
                    "https://api.weather.gov/zones/forecast/TXZ245",
                    "https://api.weather.gov/zones/forecast/TXZ246",
                    "https://api.weather.gov/zones/forecast/TXZ247",
                    "https://api.weather.gov/zones/forecast/TXZ213",
                    "https://api.weather.gov/zones/forecast/TXZ214",
                    "https://api.weather.gov/zones/forecast/TXZ236",
                    "https://api.weather.gov/zones/forecast/TXZ237",
                    "https://api.weather.gov/zones/forecast/TXZ238",
                    "https://api.weather.gov/zones/forecast/TXZ215",
                    "https://api.weather.gov/zones/forecast/TXZ216",
                    "https://api.weather.gov/zones/forecast/LAZ052",
                    "https://api.weather.gov/zones/forecast/LAZ053",
                    "https://api.weather.gov/zones/forecast/LAZ054",
                    "https://api.weather.gov/zones/forecast/LAZ073",
                    "https://api.weather.gov/zones/forecast/LAZ074",
                    "https://api.weather.gov/zones/forecast/LAZ040",
                    "https://api.weather.gov/zones/forecast/LAZ062",
                    "https://api.weather.gov/zones/forecast/LAZ064",
                    "https://api.weather.gov/zones/forecast/LAZ066",
                    "https://api.weather.gov/zones/forecast/LAZ067",
                    "https://api.weather.gov/zones/forecast/LAZ068",
                    "https://api.weather.gov/zones/forecast/LAZ069",
                    "https://api.weather.gov/zones/forecast/LAZ070",
                    "https://api.weather.gov/zones/forecast/MSZ080",
                    "https://api.weather.gov/zones/forecast/MSZ081",
                    "https://api.weather.gov/zones/forecast/MSZ082",
                    "https://api.weather.gov/zones/forecast/ALZ263",
                    "https://api.weather.gov/zones/forecast/ALZ264",
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206",
                    "https://api.weather.gov/zones/forecast/FLZ008",
                    "https://api.weather.gov/zones/forecast/FLZ012",
                    "https://api.weather.gov/zones/forecast/FLZ014",
                    "https://api.weather.gov/zones/forecast/FLZ015",
                    "https://api.weather.gov/zones/forecast/FLZ018",
                    "https://api.weather.gov/zones/forecast/FLZ027",
                    "https://api.weather.gov/zones/forecast/FLZ028",
                    "https://api.weather.gov/zones/forecast/FLZ034",
                    "https://api.weather.gov/zones/forecast/FLZ139",
                    "https://api.weather.gov/zones/forecast/FLZ142",
                    "https://api.weather.gov/zones/forecast/FLZ148",
                    "https://api.weather.gov/zones/forecast/FLZ149",
                    "https://api.weather.gov/zones/forecast/FLZ050",
                    "https://api.weather.gov/zones/forecast/FLZ151",
                    "https://api.weather.gov/zones/forecast/FLZ155",
                    "https://api.weather.gov/zones/forecast/FLZ160",
                    "https://api.weather.gov/zones/forecast/FLZ162",
                    "https://api.weather.gov/zones/forecast/FLZ165",
                    "https://api.weather.gov/zones/forecast/FLZ069",
                    "https://api.weather.gov/zones/forecast/FLZ070",
                    "https://api.weather.gov/zones/forecast/FLZ075",
                    "https://api.weather.gov/zones/forecast/FLZ076",
                    "https://api.weather.gov/zones/forecast/FLZ077",
                    "https://api.weather.gov/zones/forecast/FLZ078",
                    "https://api.weather.gov/zones/forecast/FLZ174",
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173",
                    "https://api.weather.gov/zones/forecast/FLZ047",
                    "https://api.weather.gov/zones/forecast/FLZ054",
                    "https://api.weather.gov/zones/forecast/FLZ059",
                    "https://api.weather.gov/zones/forecast/FLZ064",
                    "https://api.weather.gov/zones/forecast/FLZ141",
                    "https://api.weather.gov/zones/forecast/FLZ147",
                    "https://api.weather.gov/zones/forecast/FLZ124",
                    "https://api.weather.gov/zones/forecast/FLZ125",
                    "https://api.weather.gov/zones/forecast/FLZ033",
                    "https://api.weather.gov/zones/forecast/FLZ038",
                    "https://api.weather.gov/zones/forecast/GAZ154",
                    "https://api.weather.gov/zones/forecast/GAZ166",
                    "https://api.weather.gov/zones/forecast/GAZ117",
                    "https://api.weather.gov/zones/forecast/GAZ119",
                    "https://api.weather.gov/zones/forecast/GAZ139",
                    "https://api.weather.gov/zones/forecast/GAZ141",
                    "https://api.weather.gov/zones/forecast/SCZ048",
                    "https://api.weather.gov/zones/forecast/SCZ049",
                    "https://api.weather.gov/zones/forecast/SCZ050",
                    "https://api.weather.gov/zones/forecast/SCZ051",
                    "https://api.weather.gov/zones/forecast/SCZ052",
                    "https://api.weather.gov/zones/forecast/SCZ054",
                    "https://api.weather.gov/zones/forecast/SCZ056",
                    "https://api.weather.gov/zones/forecast/NCZ106",
                    "https://api.weather.gov/zones/forecast/NCZ108",
                    "https://api.weather.gov/zones/forecast/NCZ110",
                    "https://api.weather.gov/zones/forecast/NCZ045",
                    "https://api.weather.gov/zones/forecast/NCZ046",
                    "https://api.weather.gov/zones/forecast/NCZ047",
                    "https://api.weather.gov/zones/forecast/NCZ080",
                    "https://api.weather.gov/zones/forecast/NCZ081",
                    "https://api.weather.gov/zones/forecast/NCZ094",
                    "https://api.weather.gov/zones/forecast/NCZ095",
                    "https://api.weather.gov/zones/forecast/NCZ098",
                    "https://api.weather.gov/zones/forecast/NCZ103",
                    "https://api.weather.gov/zones/forecast/NCZ104",
                    "https://api.weather.gov/zones/forecast/NCZ015",
                    "https://api.weather.gov/zones/forecast/NCZ016",
                    "https://api.weather.gov/zones/forecast/NCZ017",
                    "https://api.weather.gov/zones/forecast/NCZ030",
                    "https://api.weather.gov/zones/forecast/NCZ031",
                    "https://api.weather.gov/zones/forecast/NCZ032",
                    "https://api.weather.gov/zones/forecast/NCZ102",
                    "https://api.weather.gov/zones/forecast/VAZ084",
                    "https://api.weather.gov/zones/forecast/VAZ086",
                    "https://api.weather.gov/zones/forecast/VAZ091",
                    "https://api.weather.gov/zones/forecast/VAZ094",
                    "https://api.weather.gov/zones/forecast/VAZ095",
                    "https://api.weather.gov/zones/forecast/VAZ098",
                    "https://api.weather.gov/zones/forecast/VAZ099",
                    "https://api.weather.gov/zones/forecast/VAZ100",
                    "https://api.weather.gov/zones/forecast/MDZ025",
                    "https://api.weather.gov/zones/forecast/DEZ002",
                    "https://api.weather.gov/zones/forecast/DEZ003",
                    "https://api.weather.gov/zones/forecast/DEZ004",
                    "https://api.weather.gov/zones/forecast/NJZ006",
                    "https://api.weather.gov/zones/forecast/NJZ012",
                    "https://api.weather.gov/zones/forecast/NJZ013",
                    "https://api.weather.gov/zones/forecast/NJZ014",
                    "https://api.weather.gov/zones/forecast/NJZ021",
                    "https://api.weather.gov/zones/forecast/NJZ023",
                    "https://api.weather.gov/zones/forecast/NJZ024",
                    "https://api.weather.gov/zones/forecast/NJZ025",
                    "https://api.weather.gov/zones/forecast/NJZ026",
                    "https://api.weather.gov/zones/forecast/NJZ106",
                    "https://api.weather.gov/zones/forecast/NJZ108",
                    "https://api.weather.gov/zones/forecast/NYZ071",
                    "https://api.weather.gov/zones/forecast/NYZ072",
                    "https://api.weather.gov/zones/forecast/NYZ073",
                    "https://api.weather.gov/zones/forecast/NYZ074",
                    "https://api.weather.gov/zones/forecast/NYZ075",
                    "https://api.weather.gov/zones/forecast/NYZ078",
                    "https://api.weather.gov/zones/forecast/NYZ079",
                    "https://api.weather.gov/zones/forecast/NYZ080",
                    "https://api.weather.gov/zones/forecast/NYZ081",
                    "https://api.weather.gov/zones/forecast/NYZ176",
                    "https://api.weather.gov/zones/forecast/NYZ177",
                    "https://api.weather.gov/zones/forecast/NYZ178",
                    "https://api.weather.gov/zones/forecast/NYZ179",
                    "https://api.weather.gov/zones/forecast/CTZ009",
                    "https://api.weather.gov/zones/forecast/CTZ010",
                    "https://api.weather.gov/zones/forecast/CTZ011",
                    "https://api.weather.gov/zones/forecast/CTZ012",
                    "https://api.weather.gov/zones/forecast/RIZ002",
                    "https://api.weather.gov/zones/forecast/RIZ004",
                    "https://api.weather.gov/zones/forecast/RIZ005",
                    "https://api.weather.gov/zones/forecast/RIZ006",
                    "https://api.weather.gov/zones/forecast/RIZ007",
                    "https://api.weather.gov/zones/forecast/RIZ008",
                    "https://api.weather.gov/zones/forecast/MAZ007",
                    "https://api.weather.gov/zones/forecast/MAZ016",
                    "https://api.weather.gov/zones/forecast/MAZ019",
                    "https://api.weather.gov/zones/forecast/MAZ020",
                    "https://api.weather.gov/zones/forecast/MAZ021",
                    "https://api.weather.gov/zones/forecast/MAZ022",
                    "https://api.weather.gov/zones/forecast/MAZ023",
                    "https://api.weather.gov/zones/forecast/MAZ024",
                    "https://api.weather.gov/zones/forecast/NHZ014",
                    "https://api.weather.gov/zones/forecast/MEZ022",
                    "https://api.weather.gov/zones/forecast/MEZ023",
                    "https://api.weather.gov/zones/forecast/MEZ024",
                    "https://api.weather.gov/zones/forecast/MEZ025",
                    "https://api.weather.gov/zones/forecast/MEZ026",
                    "https://api.weather.gov/zones/forecast/MEZ027",
                    "https://api.weather.gov/zones/forecast/MEZ028",
                    "https://api.weather.gov/zones/forecast/MEZ029",
                    "https://api.weather.gov/zones/forecast/MEZ030",
                    "https://api.weather.gov/zones/forecast/NBZ570",
                    "https://api.weather.gov/zones/forecast/NBZ550",
                    "https://api.weather.gov/zones/forecast/NBZ660",
                    "https://api.weather.gov/zones/forecast/NBZ641",
                    "https://api.weather.gov/zones/forecast/NSZ210",
                    "https://api.weather.gov/zones/forecast/NSZ230",
                    "https://api.weather.gov/zones/forecast/NSZ260",
                    "https://api.weather.gov/zones/forecast/NSZ250",
                    "https://api.weather.gov/zones/forecast/NSZ110",
                    "https://api.weather.gov/zones/forecast/NSZ120",
                    "https://api.weather.gov/zones/forecast/NSZ130",
                    "https://api.weather.gov/zones/forecast/NSZ170",
                    "https://api.weather.gov/zones/forecast/NSZ160",
                    "https://api.weather.gov/zones/forecast/NSZ150",
                    "https://api.weather.gov/zones/forecast/NSZ140",
                    "https://api.weather.gov/zones/forecast/NSZ270",
                    "https://api.weather.gov/zones/forecast/NSZ280",
                    "https://api.weather.gov/zones/forecast/NSZ320",
                    "https://api.weather.gov/zones/forecast/NSZ410",
                    "https://api.weather.gov/zones/forecast/NSZ450",
                    "https://api.weather.gov/zones/forecast/NSZ440",
                    "https://api.weather.gov/zones/forecast/NSZ430",
                    "https://api.weather.gov/zones/forecast/QCZ670",
                    "https://api.weather.gov/zones/forecast/QCZ680",
                    "https://api.weather.gov/zones/forecast/NLZ340",
                    "https://api.weather.gov/zones/forecast/NLZ220",
                    "https://api.weather.gov/zones/forecast/NLZ230",
                    "https://api.weather.gov/zones/forecast/NLZ210",
                    "https://api.weather.gov/zones/forecast/NLZ120",
                    "https://api.weather.gov/zones/forecast/NLZ132",
                    "https://api.weather.gov/zones/forecast/NLZ140",
                    "https://api.weather.gov/zones/forecast/NLZ241",
                    "https://api.weather.gov/zones/forecast/NLZ242",
                    "https://api.weather.gov/zones/forecast/NLZ110",
                    "https://api.weather.gov/zones/forecast/NLZ131",
                    "https://api.weather.gov/zones/forecast/NLZ540",
                    "https://api.weather.gov/zones/forecast/NLZ530",
                    "https://api.weather.gov/zones/forecast/NLZ570",
                    "https://api.weather.gov/zones/forecast/NLZ520",
                    "https://api.weather.gov/zones/forecast/NLZ510",
                    "https://api.weather.gov/zones/forecast/NLZ560",
                    "https://api.weather.gov/zones/forecast/NLZ610",
                    "https://api.weather.gov/zones/forecast/NLZ720",
                    "https://api.weather.gov/zones/forecast/NLZ710",
                    "https://api.weather.gov/zones/forecast/NLZ730",
                    "https://api.weather.gov/zones/forecast/NLZ740",
                    "https://api.weather.gov/zones/forecast/NLZ750",
                    "https://api.weather.gov/zones/forecast/NLZ760",
                    "https://api.weather.gov/zones/forecast/NLZ770"
                ],
                "references": [],
                "sent": "2022-02-15T12:16:00-05:00",
                "effective": "2022-02-15T12:16:00-05:00",
                "onset": "2022-02-15T12:16:00-05:00",
                "expires": "2022-02-15T13:16:00-05:00",
                "ends": "2022-02-15T13:16:00-05:00",
                "status": "Test",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Extreme",
                "certainty": "Likely",
                "urgency": "Immediate",
                "event": "Tsunami Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS National Tsunami Warning Center",
                "headline": "TEST Tsunami Warning issued February 15 at 12:16PM EST until February 15 at 1:16PM EST by NWS National Tsunami Warning Center",
                "description": "GMZ130-150-155-230-235-250-255-330-335-350-355-450-452-455-\n532-534-536-538-550-552-555-630>635-650-655-730-750-755-765-\n830-836-850-853-856-656-657-031-032-034-035-042>044-052>055-\nAMZ630-650-651-550-552-555-450-452-454-330-350-352-354-250-\n252-254-256-130-135-150-152-154-156-158-ANZ631>638-656-658-\n650-652-654-430-431-450>455-330-335-338-340-345-350-353-355-\n230>237-250-254-255-256-150-050>052-TXZ251-256-257-242>247-\n213-214-236>238-215-216-LAZ052>054-073-074-040-062-064-066>070-\nMSZ080>082-ALZ263>266-FLZ202-204-206-008-012-014-015-018-027-\n028-034-139-142-148-149-050-151-155-160-162-165-069-070-075-\n076>078-174-168-172-173-047-054-059-064-141-147-124-125-033-\n038-GAZ154-166-117-119-139-141-SCZ048>052-054-056-NCZ106-108-\n110-045>047-080-081-094-095-098-103-104-015>017-030>032-102-\nVAZ084-086-091-094-095-098-099-100-MDZ025-DEZ002>004-NJZ006-\n012>014-021-023>026-106-108-NYZ071>075-078>081-176>179-CTZ009>012-\nRIZ002-004>008-MAZ007-016-019>024-NHZ014-MEZ022>028-029-030-\nNBZ570-550-660-641-NSZ210-230-260-250-110-120-130-170-160-\n150-140-270-280-320-410-450-440-430-QCZ670-680-NLZ340-220-\n230-210-120-132-140-241-242-110-131-540-530-570-520-510-560-\n610-720-710-730-740-750-760-770-151816-\n/T.NEW.PAAQ.TS.W.9004.220215T1716Z-220215T1816Z/\nThe U.S. east coast, Gulf of Mexico coasts, and Eastern\nCanadian coastal areas\n\n...THIS_MESSAGE_IS_FOR_TEST_PURPOSES_ONLY...\n\n...THIS IS A TEST TO DETERMINE TRANSMISSION TIMES INVOLVED IN THE",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "TSUAT1"
                    ],
                    "WMOidentifier": [
                        "WEXX20 PAAQ 151716"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/T.NEW.PAAQ.TS.W.9004.220215T1716Z-220215T1816Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-15T18:16:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.9cdc765359d17ae865ec67cd8d8e1fe205faf21e.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.9cdc765359d17ae865ec67cd8d8e1fe205faf21e.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.9cdc765359d17ae865ec67cd8d8e1fe205faf21e.001.1",
                "areaDesc": "Laguna Madre From the Port Of Brownsville to the Arroyo Colorado; Coastal waters from Port Mansfield TX to the Rio Grande River out 20 NM; Coastal waters from Baffin Bay to Port Mansfield TX out 20 NM; Bays and Waterways from Baffin Bay to Port Aransas; Bays and Waterways from Port Aransas to Port O'Connor; Coastal waters from Baffin Bay to Port Aransas out 20 NM; Coastal waters from Port Aransas to Matagorda Ship Channel out 20 NM; Matagorda Bay; Galveston Bay; Coastal waters from Freeport to Matagorda Ship Channel TX out 20 NM; Coastal waters from High Island to Freeport TX out 20 NM; Coastal waters from Cameron LA to High Island TX out 20 NM; Coastal waters from Intracoastal City to Cameron LA out 20 NM; Coastal waters from Lower Atchafalaya River to Intracoastal City LA out 20 NM; Mississippi Sound; Lake Borgne; Chandeleur Sound; Breton Sound; Coastal Waters from Port Fourchon LA to Lower Atchafalaya River LA out 20 nm; Coastal waters from the Southwest Pass of the Mississippi River to Port Fourchon Louisiana out 20 NM; Coastal Waters from Boothville LA to Southwest Pass of the Mississippi River out 20 nm; North Mobile Bay; South Mobile Bay; Mississippi Sound; Perdido Bay Area; Pensacola Bay Area including Santa Rosa Sound; Western Choctawhatchee Bay; Coastal waters from Pensacola FL to Pascagoula MS out 20 NM; Coastal waters from Okaloosa-Walton County Line to Pensacola FL out 20 NM; Apalachee Bay or Coastal Waters From Keaton Beach to Ochlockonee River Fl out to 20 Nm; Coastal waters from Okaloosa-Walton County Line to Mexico Beach out 20 NM; Coastal Waters From  Ochlockonee River to Apalachicola FL out to 20 Nm; Coastal waters from  Suwannee River to Keaton Beach out 20 NM; Tampa Bay waters; Charlotte Harbor and Pine Island Sound; Coastal waters from Tarpon Springs to Suwannee River FL out 20 NM; Coastal waters from Englewood to Tarpon Springs FL out 20 NM; Coastal waters from Bonita Beach to Englewood FL out 20 NM; Coastal waters from Chokoloskee to Bonita Beach FL out 20 NM; Coastal waters from East Cape Sable to Chokoloskee FL out 20 NM; Florida Bay including Barnes Sound, Blackwater Sound, and Buttonwood Sound; Bayside and Gulf side from Craig Key to West End of Seven Mile Bridge; Gulf of Mexico including Dry Tortugas and Rebecca Shoal Channel; Gulf of Mexico from West End of Seven Mile Bridge to Halfmoon Shoal out to 5 Fathoms; Hawk Channel from Ocean Reef to Craig Key out to the reef; Hawk Channel from Craig Key to west end of Seven Mile Bridge out to the reef; Hawk Channel from west end of Seven Mile Bridge to Halfmoon Shoal out to the reef; Straits of Florida from Ocean Reef to Craig Key out 20 NM; Straits of Florida from Craig Key to west end of Seven Mile Bridge out 20 NM; Straits of Florida from west end of Seven Mile Bridge to south of Halfmoon Shoal out 20 NM; Straits of Florida from Halfmoon Shoal to 20 NM west of Dry Tortugas out 20 NM; Biscayne Bay; Coastal waters from Jupiter Inlet to Deerfield Beach FL out 20 NM; Coastal waters from Deerfield Beach to Ocean Reef FL out 20 NM; Flagler Beach to Volusia-Brevard County Line 0-20 nm; Volusia-Brevard County Line to Sebastian Inlet 0-20 nm; Sebastian Inlet to Jupiter Inlet 0-20 nm; Coastal waters from Altamaha Sound to Fernandina Beach FL out 20 NM; Coastal waters from Fernandina Beach to St. Augustine FL out 20 NM; Coastal waters from St. Augustine to Flagler Beach FL out 20 NM; Charleston Harbor; Coastal waters from South Santee River to Edisto Beach SC out 20 nm; Coastal waters from Edisto Beach SC to Savannah GA out 20 nm; Coastal waters from Savannah GA to Altamaha Sound GA out 20 nm ...including Grays Reef National Marine Sanctuary; Coastal waters from Surf City to Cape Fear NC out 20 nm; Coastal waters from Cape Fear NC to Little River Inlet SC out 20 nm; Coastal waters from Little River Inlet to Murrells Inlet SC out 20 nm; Coastal waters from Murrells Inlet to South Santee River SC out 20 nm; Pamlico Sound; S of Currituck Beach Light NC to Oregon Inlet NC out to 20 nm; S of Oregon Inlet NC to Cape Hatteras NC out to 20 nm; S of Cape Hatteras NC to Ocracoke Inlet NC out to 20 nm; S of Ocracoke Inlet NC to Cape Lookout NC out to 20 nm; S of Cape Lookout NC to Surf City NC out to 20 nm; Chesapeake Bay from Windmill Point to New Point Comfort VA; Chesapeake Bay from New Point Comfort to Little Creek VA; Currituck Sound; Chesapeake Bay from Little Creek VA to Cape Henry VA including the Chesapeake Bay Bridge Tunnel; Rappahannock River from Urbanna to Windmill Point; York River; James River from Jamestown to the James River Bridge; James River from James River Bridge to Hampton Roads Bridge-Tunnel; Coastal Waters from Cape Charles Light to Virginia-North Carolina border out to 20 nm; Coastal waters from NC VA border to Currituck Beach Light NC out 20 nm; Coastal waters from Fenwick Island DE to Chincoteague VA out 20 nm; Coastal waters from Chincoteague to Parramore Island VA out 20 nm; Coastal waters from Parramore Island to Cape Charles Light VA out 20 nm; Delaware Bay waters north of East Point NJ to Slaughter Beach DE; Delaware Bay waters south of East Point NJ to Slaughter Beach DE; Coastal waters from Sandy Hook to Manasquan Inlet NJ out 20 nm; Coastal waters from Manasquan Inlet to Little Egg Inlet NJ out 20 nm; Coastal waters from Little Egg Inlet to Great Egg Inlet NJ out 20 nm; Coastal waters from Great Egg Inlet to Cape May NJ out 20 nm; Coastal waters from Cape May NJ to Cape Henlopen DE out 20 nm; Coastal waters from Cape Henlopen to Fenwick Island DE out 20 nm; Long Island Sound West of New Haven CT/Port Jefferson NY; New York Harbor; Peconic and Gardiners Bays; South Shore Bays from Jones Inlet through Shinnecock Bay; Moriches Inlet NY to Montauk Point NY out 20 nm; Fire Island Inlet NY to Moriches Inlet NY out 20 nm; Sandy Hook NJ to Fire Island Inlet NY out 20 nm; Boston Harbor; Cape Cod Bay; Nantucket Sound; Vineyard Sound; Buzzards Bay; Rhode Island Sound; Narragansett Bay; Block Island Sound; Coastal waters east of Ipswich Bay and the Stellwagen Bank National Marine Sanctuary; Coastal waters from Provincetown MA to Chatham MA to Nantucket MA out 20 nm; Coastal Waters extending out to 25 nm South of Marthas Vineyard and Nantucket; Coastal Waters from Montauk NY to Marthas Vineyard extending out to 20 nm South of Block Island; Coastal Waters from Stonington, ME to Port Clyde, ME out 25 NM; Coastal Waters from Eastport, ME to Schoodic Point, ME out 25 NM; Coastal Waters from Schoodic Point, ME to Stonington, ME out 25 NM; Intra Coastal Waters from Schoodic Point, ME to Stonington, ME; Inland Kenedy; Coastal Willacy; Coastal Cameron; Inland Kleberg; Inland Nueces; Inland San Patricio; Coastal Aransas; Inland Refugio; Inland Calhoun; Inland Harris; Chambers; Inland Matagorda; Inland Brazoria; Inland Galveston; Jefferson; Orange; Vermilion; Iberia; St. Mary; West Cameron; East Cameron; Upper St. Bernard; Lower Terrebonne; Lower Lafourche; Lower Jefferson; Lower Plaquemines; Lower St. Bernard; Hancock; Harrison; Jackson; Mobile Central; Baldwin Central; Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal; Central Walton; Inland Bay; Inland Gulf; Inland Franklin; Inland Jefferson; Inland Wakulla; Inland Taylor; Inland Dixie; Coastal Levy; Coastal Citrus; Coastal Hernando; Coastal Pasco; Pinellas; Coastal Hillsborough; Coastal Manatee; Coastal Sarasota; Coastal Charlotte; Coastal Lee; Coastal Collier County; Inland Collier County; Mainland Monroe; Monroe Upper Keys; Monroe Middle Keys; Monroe Lower Keys; Far South Miami-Dade County; Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County; Southern Brevard County; Indian River; St. Lucie; Martin; Coastal Volusia; Northern Brevard County; Coastal Nassau; Coastal Duval; Inland St. Johns; Inland Flagler; Coastal Glynn; Coastal Camden; Coastal Bryan; Coastal Chatham; Coastal Liberty; Coastal McIntosh; Beaufort; Coastal Colleton; Charleston; Coastal Jasper; Tidal Berkeley; Coastal Horry; Coastal Georgetown; Coastal Pender; Coastal New Hanover; Coastal Brunswick; Washington; Tyrrell; Mainland Dare; Beaufort; Mainland Hyde; Pamlico; Pasquotank; Camden; Western Currituck; Bertie; Chowan; Perquimans; Eastern Currituck; Gloucester; Mathews; Norfolk/Portsmouth; Virginia Beach; Accomack; Northampton; Maryland Beaches; Kent; Inland Sussex; Delaware Beaches; Hudson; Middlesex; Western Monmouth; Eastern Monmouth; Cumberland; Cape May; Atlantic Coastal Cape May; Coastal Atlantic; Coastal Ocean; Eastern Essex; Eastern Union; Southern Westchester; New York (Manhattan); Bronx; Richmond (Staten Is.); Kings (Brooklyn); Northwest Suffolk; Northeast Suffolk; Southwest Suffolk; Southeast Suffolk; Northern Queens; Northern Nassau; Southern Queens; Southern Nassau; Southern Fairfield; Southern New Haven; Southern Middlesex; Southern New London; Southeast Providence; Eastern Kent; Bristol; Washington; Newport; Block Island; Eastern Essex; Eastern Norfolk; Eastern Plymouth; Southern Bristol; Southern Plymouth; Barnstable; Dukes; Nantucket; Coastal Rockingham; Interior Waldo; Coastal York; Coastal Cumberland; Sagadahoc; Lincoln; Knox; Coastal Waldo; Coastal Hancock; Coastal Washington",
                "geocode": {
                    "SAME": [
                        "077130",
                        "077150",
                        "077155",
                        "077230",
                        "077235",
                        "077250",
                        "077255",
                        "077330",
                        "077335",
                        "077350",
                        "077355",
                        "077450",
                        "077452",
                        "077455",
                        "077532",
                        "077534",
                        "077536",
                        "077538",
                        "077550",
                        "077552",
                        "077555",
                        "077630",
                        "077631",
                        "077632",
                        "077633",
                        "077634",
                        "077635",
                        "077650",
                        "077655",
                        "077730",
                        "077750",
                        "077755",
                        "077765",
                        "077830",
                        "077836",
                        "077850",
                        "077853",
                        "077856",
                        "077656",
                        "077657",
                        "077031",
                        "077032",
                        "077034",
                        "077035",
                        "077042",
                        "077043",
                        "077044",
                        "077052",
                        "077053",
                        "077054",
                        "077055",
                        "075630",
                        "075650",
                        "075651",
                        "075550",
                        "075552",
                        "075555",
                        "075450",
                        "075452",
                        "075454",
                        "075330",
                        "075350",
                        "075352",
                        "075354",
                        "075250",
                        "075252",
                        "075254",
                        "075256",
                        "075135",
                        "075150",
                        "075152",
                        "075154",
                        "075156",
                        "075158",
                        "073631",
                        "073632",
                        "073633",
                        "073634",
                        "073635",
                        "073636",
                        "073637",
                        "073638",
                        "073656",
                        "073658",
                        "073650",
                        "073652",
                        "073654",
                        "073430",
                        "073431",
                        "073450",
                        "073451",
                        "073452",
                        "073453",
                        "073454",
                        "073455",
                        "073335",
                        "073338",
                        "073340",
                        "073345",
                        "073350",
                        "073353",
                        "073355",
                        "073230",
                        "073231",
                        "073232",
                        "073233",
                        "073234",
                        "073235",
                        "073236",
                        "073237",
                        "073250",
                        "073254",
                        "073255",
                        "073256",
                        "073150",
                        "073050",
                        "073051",
                        "073052",
                        "048261",
                        "048489",
                        "048061",
                        "048273",
                        "048355",
                        "048409",
                        "048007",
                        "048391",
                        "048057",
                        "048201",
                        "048071",
                        "048321",
                        "048039",
                        "048167",
                        "048245",
                        "048361",
                        "022113",
                        "022045",
                        "022101",
                        "022023",
                        "022087",
                        "022109",
                        "022057",
                        "022051",
                        "022075",
                        "028045",
                        "028047",
                        "028059",
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091",
                        "012131",
                        "012005",
                        "012045",
                        "012037",
                        "012065",
                        "012129",
                        "012123",
                        "012029",
                        "012075",
                        "012017",
                        "012053",
                        "012101",
                        "012103",
                        "012057",
                        "012081",
                        "012115",
                        "012015",
                        "012071",
                        "012021",
                        "012087",
                        "012086",
                        "012099",
                        "012011",
                        "012009",
                        "012061",
                        "012111",
                        "012085",
                        "012127",
                        "012089",
                        "012031",
                        "012109",
                        "012035",
                        "013127",
                        "013039",
                        "013029",
                        "013051",
                        "013179",
                        "013191",
                        "045013",
                        "045029",
                        "045019",
                        "045053",
                        "045015",
                        "045051",
                        "045043",
                        "037141",
                        "037129",
                        "037019",
                        "037187",
                        "037177",
                        "037055",
                        "037013",
                        "037095",
                        "037137",
                        "037139",
                        "037029",
                        "037053",
                        "037015",
                        "037041",
                        "037143",
                        "051073",
                        "051115",
                        "051710",
                        "051740",
                        "051810",
                        "051001",
                        "051131",
                        "024047",
                        "010001",
                        "010005",
                        "034017",
                        "034023",
                        "034025",
                        "034011",
                        "034009",
                        "034001",
                        "034029",
                        "034013",
                        "034039",
                        "036119",
                        "036061",
                        "036005",
                        "036085",
                        "036047",
                        "036103",
                        "036081",
                        "036059",
                        "009001",
                        "009009",
                        "009007",
                        "009011",
                        "044007",
                        "044003",
                        "044001",
                        "044009",
                        "044005",
                        "025009",
                        "025021",
                        "025023",
                        "025005",
                        "025001",
                        "025007",
                        "025019",
                        "033015",
                        "023027",
                        "023031",
                        "023005",
                        "023023",
                        "023015",
                        "023013",
                        "023009",
                        "023029"
                    ],
                    "UGC": [
                        "GMZ130",
                        "GMZ150",
                        "GMZ155",
                        "GMZ230",
                        "GMZ235",
                        "GMZ250",
                        "GMZ255",
                        "GMZ330",
                        "GMZ335",
                        "GMZ350",
                        "GMZ355",
                        "GMZ450",
                        "GMZ452",
                        "GMZ455",
                        "GMZ532",
                        "GMZ534",
                        "GMZ536",
                        "GMZ538",
                        "GMZ550",
                        "GMZ552",
                        "GMZ555",
                        "GMZ630",
                        "GMZ631",
                        "GMZ632",
                        "GMZ633",
                        "GMZ634",
                        "GMZ635",
                        "GMZ650",
                        "GMZ655",
                        "GMZ730",
                        "GMZ750",
                        "GMZ755",
                        "GMZ765",
                        "GMZ830",
                        "GMZ836",
                        "GMZ850",
                        "GMZ853",
                        "GMZ856",
                        "GMZ656",
                        "GMZ657",
                        "GMZ031",
                        "GMZ032",
                        "GMZ034",
                        "GMZ035",
                        "GMZ042",
                        "GMZ043",
                        "GMZ044",
                        "GMZ052",
                        "GMZ053",
                        "GMZ054",
                        "GMZ055",
                        "AMZ630",
                        "AMZ650",
                        "AMZ651",
                        "AMZ550",
                        "AMZ552",
                        "AMZ555",
                        "AMZ450",
                        "AMZ452",
                        "AMZ454",
                        "AMZ330",
                        "AMZ350",
                        "AMZ352",
                        "AMZ354",
                        "AMZ250",
                        "AMZ252",
                        "AMZ254",
                        "AMZ256",
                        "AMZ130",
                        "AMZ135",
                        "AMZ150",
                        "AMZ152",
                        "AMZ154",
                        "AMZ156",
                        "AMZ158",
                        "ANZ631",
                        "ANZ632",
                        "ANZ633",
                        "ANZ634",
                        "ANZ635",
                        "ANZ636",
                        "ANZ637",
                        "ANZ638",
                        "ANZ656",
                        "ANZ658",
                        "ANZ650",
                        "ANZ652",
                        "ANZ654",
                        "ANZ430",
                        "ANZ431",
                        "ANZ450",
                        "ANZ451",
                        "ANZ452",
                        "ANZ453",
                        "ANZ454",
                        "ANZ455",
                        "ANZ330",
                        "ANZ335",
                        "ANZ338",
                        "ANZ340",
                        "ANZ345",
                        "ANZ350",
                        "ANZ353",
                        "ANZ355",
                        "ANZ230",
                        "ANZ231",
                        "ANZ232",
                        "ANZ233",
                        "ANZ234",
                        "ANZ235",
                        "ANZ236",
                        "ANZ237",
                        "ANZ250",
                        "ANZ254",
                        "ANZ255",
                        "ANZ256",
                        "ANZ150",
                        "ANZ050",
                        "ANZ051",
                        "ANZ052",
                        "TXZ251",
                        "TXZ256",
                        "TXZ257",
                        "TXZ242",
                        "TXZ243",
                        "TXZ244",
                        "TXZ245",
                        "TXZ246",
                        "TXZ247",
                        "TXZ213",
                        "TXZ214",
                        "TXZ236",
                        "TXZ237",
                        "TXZ238",
                        "TXZ215",
                        "TXZ216",
                        "LAZ052",
                        "LAZ053",
                        "LAZ054",
                        "LAZ073",
                        "LAZ074",
                        "LAZ040",
                        "LAZ062",
                        "LAZ064",
                        "LAZ066",
                        "LAZ067",
                        "LAZ068",
                        "LAZ069",
                        "LAZ070",
                        "MSZ080",
                        "MSZ081",
                        "MSZ082",
                        "ALZ263",
                        "ALZ264",
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206",
                        "FLZ008",
                        "FLZ012",
                        "FLZ014",
                        "FLZ015",
                        "FLZ018",
                        "FLZ027",
                        "FLZ028",
                        "FLZ034",
                        "FLZ139",
                        "FLZ142",
                        "FLZ148",
                        "FLZ149",
                        "FLZ050",
                        "FLZ151",
                        "FLZ155",
                        "FLZ160",
                        "FLZ162",
                        "FLZ165",
                        "FLZ069",
                        "FLZ070",
                        "FLZ075",
                        "FLZ076",
                        "FLZ077",
                        "FLZ078",
                        "FLZ174",
                        "FLZ168",
                        "FLZ172",
                        "FLZ173",
                        "FLZ047",
                        "FLZ054",
                        "FLZ059",
                        "FLZ064",
                        "FLZ141",
                        "FLZ147",
                        "FLZ124",
                        "FLZ125",
                        "FLZ033",
                        "FLZ038",
                        "GAZ154",
                        "GAZ166",
                        "GAZ117",
                        "GAZ119",
                        "GAZ139",
                        "GAZ141",
                        "SCZ048",
                        "SCZ049",
                        "SCZ050",
                        "SCZ051",
                        "SCZ052",
                        "SCZ054",
                        "SCZ056",
                        "NCZ106",
                        "NCZ108",
                        "NCZ110",
                        "NCZ045",
                        "NCZ046",
                        "NCZ047",
                        "NCZ080",
                        "NCZ081",
                        "NCZ094",
                        "NCZ095",
                        "NCZ098",
                        "NCZ103",
                        "NCZ104",
                        "NCZ015",
                        "NCZ016",
                        "NCZ017",
                        "NCZ030",
                        "NCZ031",
                        "NCZ032",
                        "NCZ102",
                        "VAZ084",
                        "VAZ086",
                        "VAZ091",
                        "VAZ094",
                        "VAZ095",
                        "VAZ098",
                        "VAZ099",
                        "VAZ100",
                        "MDZ025",
                        "DEZ002",
                        "DEZ003",
                        "DEZ004",
                        "NJZ006",
                        "NJZ012",
                        "NJZ013",
                        "NJZ014",
                        "NJZ021",
                        "NJZ023",
                        "NJZ024",
                        "NJZ025",
                        "NJZ026",
                        "NJZ106",
                        "NJZ108",
                        "NYZ071",
                        "NYZ072",
                        "NYZ073",
                        "NYZ074",
                        "NYZ075",
                        "NYZ078",
                        "NYZ079",
                        "NYZ080",
                        "NYZ081",
                        "NYZ176",
                        "NYZ177",
                        "NYZ178",
                        "NYZ179",
                        "CTZ009",
                        "CTZ010",
                        "CTZ011",
                        "CTZ012",
                        "RIZ002",
                        "RIZ004",
                        "RIZ005",
                        "RIZ006",
                        "RIZ007",
                        "RIZ008",
                        "MAZ007",
                        "MAZ016",
                        "MAZ019",
                        "MAZ020",
                        "MAZ021",
                        "MAZ022",
                        "MAZ023",
                        "MAZ024",
                        "NHZ014",
                        "MEZ022",
                        "MEZ023",
                        "MEZ024",
                        "MEZ025",
                        "MEZ026",
                        "MEZ027",
                        "MEZ028",
                        "MEZ029",
                        "MEZ030",
                        "NBZ570",
                        "NBZ550",
                        "NBZ660",
                        "NBZ641",
                        "NSZ210",
                        "NSZ230",
                        "NSZ260",
                        "NSZ250",
                        "NSZ110",
                        "NSZ120",
                        "NSZ130",
                        "NSZ170",
                        "NSZ160",
                        "NSZ150",
                        "NSZ140",
                        "NSZ270",
                        "NSZ280",
                        "NSZ320",
                        "NSZ410",
                        "NSZ450",
                        "NSZ440",
                        "NSZ430",
                        "QCZ670",
                        "QCZ680",
                        "NLZ340",
                        "NLZ220",
                        "NLZ230",
                        "NLZ210",
                        "NLZ120",
                        "NLZ132",
                        "NLZ140",
                        "NLZ241",
                        "NLZ242",
                        "NLZ110",
                        "NLZ131",
                        "NLZ540",
                        "NLZ530",
                        "NLZ570",
                        "NLZ520",
                        "NLZ510",
                        "NLZ560",
                        "NLZ610",
                        "NLZ720",
                        "NLZ710",
                        "NLZ730",
                        "NLZ740",
                        "NLZ750",
                        "NLZ760",
                        "NLZ770"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/GMZ130",
                    "https://api.weather.gov/zones/forecast/GMZ150",
                    "https://api.weather.gov/zones/forecast/GMZ155",
                    "https://api.weather.gov/zones/forecast/GMZ230",
                    "https://api.weather.gov/zones/forecast/GMZ235",
                    "https://api.weather.gov/zones/forecast/GMZ250",
                    "https://api.weather.gov/zones/forecast/GMZ255",
                    "https://api.weather.gov/zones/forecast/GMZ330",
                    "https://api.weather.gov/zones/forecast/GMZ335",
                    "https://api.weather.gov/zones/forecast/GMZ350",
                    "https://api.weather.gov/zones/forecast/GMZ355",
                    "https://api.weather.gov/zones/forecast/GMZ450",
                    "https://api.weather.gov/zones/forecast/GMZ452",
                    "https://api.weather.gov/zones/forecast/GMZ455",
                    "https://api.weather.gov/zones/forecast/GMZ532",
                    "https://api.weather.gov/zones/forecast/GMZ534",
                    "https://api.weather.gov/zones/forecast/GMZ536",
                    "https://api.weather.gov/zones/forecast/GMZ538",
                    "https://api.weather.gov/zones/forecast/GMZ550",
                    "https://api.weather.gov/zones/forecast/GMZ552",
                    "https://api.weather.gov/zones/forecast/GMZ555",
                    "https://api.weather.gov/zones/forecast/GMZ630",
                    "https://api.weather.gov/zones/forecast/GMZ631",
                    "https://api.weather.gov/zones/forecast/GMZ632",
                    "https://api.weather.gov/zones/forecast/GMZ633",
                    "https://api.weather.gov/zones/forecast/GMZ634",
                    "https://api.weather.gov/zones/forecast/GMZ635",
                    "https://api.weather.gov/zones/forecast/GMZ650",
                    "https://api.weather.gov/zones/forecast/GMZ655",
                    "https://api.weather.gov/zones/forecast/GMZ730",
                    "https://api.weather.gov/zones/forecast/GMZ750",
                    "https://api.weather.gov/zones/forecast/GMZ755",
                    "https://api.weather.gov/zones/forecast/GMZ765",
                    "https://api.weather.gov/zones/forecast/GMZ830",
                    "https://api.weather.gov/zones/forecast/GMZ836",
                    "https://api.weather.gov/zones/forecast/GMZ850",
                    "https://api.weather.gov/zones/forecast/GMZ853",
                    "https://api.weather.gov/zones/forecast/GMZ856",
                    "https://api.weather.gov/zones/forecast/GMZ656",
                    "https://api.weather.gov/zones/forecast/GMZ657",
                    "https://api.weather.gov/zones/forecast/GMZ031",
                    "https://api.weather.gov/zones/forecast/GMZ032",
                    "https://api.weather.gov/zones/forecast/GMZ034",
                    "https://api.weather.gov/zones/forecast/GMZ035",
                    "https://api.weather.gov/zones/forecast/GMZ042",
                    "https://api.weather.gov/zones/forecast/GMZ043",
                    "https://api.weather.gov/zones/forecast/GMZ044",
                    "https://api.weather.gov/zones/forecast/GMZ052",
                    "https://api.weather.gov/zones/forecast/GMZ053",
                    "https://api.weather.gov/zones/forecast/GMZ054",
                    "https://api.weather.gov/zones/forecast/GMZ055",
                    "https://api.weather.gov/zones/forecast/AMZ630",
                    "https://api.weather.gov/zones/forecast/AMZ650",
                    "https://api.weather.gov/zones/forecast/AMZ651",
                    "https://api.weather.gov/zones/forecast/AMZ550",
                    "https://api.weather.gov/zones/forecast/AMZ552",
                    "https://api.weather.gov/zones/forecast/AMZ555",
                    "https://api.weather.gov/zones/forecast/AMZ450",
                    "https://api.weather.gov/zones/forecast/AMZ452",
                    "https://api.weather.gov/zones/forecast/AMZ454",
                    "https://api.weather.gov/zones/forecast/AMZ330",
                    "https://api.weather.gov/zones/forecast/AMZ350",
                    "https://api.weather.gov/zones/forecast/AMZ352",
                    "https://api.weather.gov/zones/forecast/AMZ354",
                    "https://api.weather.gov/zones/forecast/AMZ250",
                    "https://api.weather.gov/zones/forecast/AMZ252",
                    "https://api.weather.gov/zones/forecast/AMZ254",
                    "https://api.weather.gov/zones/forecast/AMZ256",
                    "https://api.weather.gov/zones/forecast/AMZ130",
                    "https://api.weather.gov/zones/forecast/AMZ135",
                    "https://api.weather.gov/zones/forecast/AMZ150",
                    "https://api.weather.gov/zones/forecast/AMZ152",
                    "https://api.weather.gov/zones/forecast/AMZ154",
                    "https://api.weather.gov/zones/forecast/AMZ156",
                    "https://api.weather.gov/zones/forecast/AMZ158",
                    "https://api.weather.gov/zones/forecast/ANZ631",
                    "https://api.weather.gov/zones/forecast/ANZ632",
                    "https://api.weather.gov/zones/forecast/ANZ633",
                    "https://api.weather.gov/zones/forecast/ANZ634",
                    "https://api.weather.gov/zones/forecast/ANZ635",
                    "https://api.weather.gov/zones/forecast/ANZ636",
                    "https://api.weather.gov/zones/forecast/ANZ637",
                    "https://api.weather.gov/zones/forecast/ANZ638",
                    "https://api.weather.gov/zones/forecast/ANZ656",
                    "https://api.weather.gov/zones/forecast/ANZ658",
                    "https://api.weather.gov/zones/forecast/ANZ650",
                    "https://api.weather.gov/zones/forecast/ANZ652",
                    "https://api.weather.gov/zones/forecast/ANZ654",
                    "https://api.weather.gov/zones/forecast/ANZ430",
                    "https://api.weather.gov/zones/forecast/ANZ431",
                    "https://api.weather.gov/zones/forecast/ANZ450",
                    "https://api.weather.gov/zones/forecast/ANZ451",
                    "https://api.weather.gov/zones/forecast/ANZ452",
                    "https://api.weather.gov/zones/forecast/ANZ453",
                    "https://api.weather.gov/zones/forecast/ANZ454",
                    "https://api.weather.gov/zones/forecast/ANZ455",
                    "https://api.weather.gov/zones/forecast/ANZ330",
                    "https://api.weather.gov/zones/forecast/ANZ335",
                    "https://api.weather.gov/zones/forecast/ANZ338",
                    "https://api.weather.gov/zones/forecast/ANZ340",
                    "https://api.weather.gov/zones/forecast/ANZ345",
                    "https://api.weather.gov/zones/forecast/ANZ350",
                    "https://api.weather.gov/zones/forecast/ANZ353",
                    "https://api.weather.gov/zones/forecast/ANZ355",
                    "https://api.weather.gov/zones/forecast/ANZ230",
                    "https://api.weather.gov/zones/forecast/ANZ231",
                    "https://api.weather.gov/zones/forecast/ANZ232",
                    "https://api.weather.gov/zones/forecast/ANZ233",
                    "https://api.weather.gov/zones/forecast/ANZ234",
                    "https://api.weather.gov/zones/forecast/ANZ235",
                    "https://api.weather.gov/zones/forecast/ANZ236",
                    "https://api.weather.gov/zones/forecast/ANZ237",
                    "https://api.weather.gov/zones/forecast/ANZ250",
                    "https://api.weather.gov/zones/forecast/ANZ254",
                    "https://api.weather.gov/zones/forecast/ANZ255",
                    "https://api.weather.gov/zones/forecast/ANZ256",
                    "https://api.weather.gov/zones/forecast/ANZ150",
                    "https://api.weather.gov/zones/forecast/ANZ050",
                    "https://api.weather.gov/zones/forecast/ANZ051",
                    "https://api.weather.gov/zones/forecast/ANZ052",
                    "https://api.weather.gov/zones/forecast/TXZ251",
                    "https://api.weather.gov/zones/forecast/TXZ256",
                    "https://api.weather.gov/zones/forecast/TXZ257",
                    "https://api.weather.gov/zones/forecast/TXZ242",
                    "https://api.weather.gov/zones/forecast/TXZ243",
                    "https://api.weather.gov/zones/forecast/TXZ244",
                    "https://api.weather.gov/zones/forecast/TXZ245",
                    "https://api.weather.gov/zones/forecast/TXZ246",
                    "https://api.weather.gov/zones/forecast/TXZ247",
                    "https://api.weather.gov/zones/forecast/TXZ213",
                    "https://api.weather.gov/zones/forecast/TXZ214",
                    "https://api.weather.gov/zones/forecast/TXZ236",
                    "https://api.weather.gov/zones/forecast/TXZ237",
                    "https://api.weather.gov/zones/forecast/TXZ238",
                    "https://api.weather.gov/zones/forecast/TXZ215",
                    "https://api.weather.gov/zones/forecast/TXZ216",
                    "https://api.weather.gov/zones/forecast/LAZ052",
                    "https://api.weather.gov/zones/forecast/LAZ053",
                    "https://api.weather.gov/zones/forecast/LAZ054",
                    "https://api.weather.gov/zones/forecast/LAZ073",
                    "https://api.weather.gov/zones/forecast/LAZ074",
                    "https://api.weather.gov/zones/forecast/LAZ040",
                    "https://api.weather.gov/zones/forecast/LAZ062",
                    "https://api.weather.gov/zones/forecast/LAZ064",
                    "https://api.weather.gov/zones/forecast/LAZ066",
                    "https://api.weather.gov/zones/forecast/LAZ067",
                    "https://api.weather.gov/zones/forecast/LAZ068",
                    "https://api.weather.gov/zones/forecast/LAZ069",
                    "https://api.weather.gov/zones/forecast/LAZ070",
                    "https://api.weather.gov/zones/forecast/MSZ080",
                    "https://api.weather.gov/zones/forecast/MSZ081",
                    "https://api.weather.gov/zones/forecast/MSZ082",
                    "https://api.weather.gov/zones/forecast/ALZ263",
                    "https://api.weather.gov/zones/forecast/ALZ264",
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206",
                    "https://api.weather.gov/zones/forecast/FLZ008",
                    "https://api.weather.gov/zones/forecast/FLZ012",
                    "https://api.weather.gov/zones/forecast/FLZ014",
                    "https://api.weather.gov/zones/forecast/FLZ015",
                    "https://api.weather.gov/zones/forecast/FLZ018",
                    "https://api.weather.gov/zones/forecast/FLZ027",
                    "https://api.weather.gov/zones/forecast/FLZ028",
                    "https://api.weather.gov/zones/forecast/FLZ034",
                    "https://api.weather.gov/zones/forecast/FLZ139",
                    "https://api.weather.gov/zones/forecast/FLZ142",
                    "https://api.weather.gov/zones/forecast/FLZ148",
                    "https://api.weather.gov/zones/forecast/FLZ149",
                    "https://api.weather.gov/zones/forecast/FLZ050",
                    "https://api.weather.gov/zones/forecast/FLZ151",
                    "https://api.weather.gov/zones/forecast/FLZ155",
                    "https://api.weather.gov/zones/forecast/FLZ160",
                    "https://api.weather.gov/zones/forecast/FLZ162",
                    "https://api.weather.gov/zones/forecast/FLZ165",
                    "https://api.weather.gov/zones/forecast/FLZ069",
                    "https://api.weather.gov/zones/forecast/FLZ070",
                    "https://api.weather.gov/zones/forecast/FLZ075",
                    "https://api.weather.gov/zones/forecast/FLZ076",
                    "https://api.weather.gov/zones/forecast/FLZ077",
                    "https://api.weather.gov/zones/forecast/FLZ078",
                    "https://api.weather.gov/zones/forecast/FLZ174",
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173",
                    "https://api.weather.gov/zones/forecast/FLZ047",
                    "https://api.weather.gov/zones/forecast/FLZ054",
                    "https://api.weather.gov/zones/forecast/FLZ059",
                    "https://api.weather.gov/zones/forecast/FLZ064",
                    "https://api.weather.gov/zones/forecast/FLZ141",
                    "https://api.weather.gov/zones/forecast/FLZ147",
                    "https://api.weather.gov/zones/forecast/FLZ124",
                    "https://api.weather.gov/zones/forecast/FLZ125",
                    "https://api.weather.gov/zones/forecast/FLZ033",
                    "https://api.weather.gov/zones/forecast/FLZ038",
                    "https://api.weather.gov/zones/forecast/GAZ154",
                    "https://api.weather.gov/zones/forecast/GAZ166",
                    "https://api.weather.gov/zones/forecast/GAZ117",
                    "https://api.weather.gov/zones/forecast/GAZ119",
                    "https://api.weather.gov/zones/forecast/GAZ139",
                    "https://api.weather.gov/zones/forecast/GAZ141",
                    "https://api.weather.gov/zones/forecast/SCZ048",
                    "https://api.weather.gov/zones/forecast/SCZ049",
                    "https://api.weather.gov/zones/forecast/SCZ050",
                    "https://api.weather.gov/zones/forecast/SCZ051",
                    "https://api.weather.gov/zones/forecast/SCZ052",
                    "https://api.weather.gov/zones/forecast/SCZ054",
                    "https://api.weather.gov/zones/forecast/SCZ056",
                    "https://api.weather.gov/zones/forecast/NCZ106",
                    "https://api.weather.gov/zones/forecast/NCZ108",
                    "https://api.weather.gov/zones/forecast/NCZ110",
                    "https://api.weather.gov/zones/forecast/NCZ045",
                    "https://api.weather.gov/zones/forecast/NCZ046",
                    "https://api.weather.gov/zones/forecast/NCZ047",
                    "https://api.weather.gov/zones/forecast/NCZ080",
                    "https://api.weather.gov/zones/forecast/NCZ081",
                    "https://api.weather.gov/zones/forecast/NCZ094",
                    "https://api.weather.gov/zones/forecast/NCZ095",
                    "https://api.weather.gov/zones/forecast/NCZ098",
                    "https://api.weather.gov/zones/forecast/NCZ103",
                    "https://api.weather.gov/zones/forecast/NCZ104",
                    "https://api.weather.gov/zones/forecast/NCZ015",
                    "https://api.weather.gov/zones/forecast/NCZ016",
                    "https://api.weather.gov/zones/forecast/NCZ017",
                    "https://api.weather.gov/zones/forecast/NCZ030",
                    "https://api.weather.gov/zones/forecast/NCZ031",
                    "https://api.weather.gov/zones/forecast/NCZ032",
                    "https://api.weather.gov/zones/forecast/NCZ102",
                    "https://api.weather.gov/zones/forecast/VAZ084",
                    "https://api.weather.gov/zones/forecast/VAZ086",
                    "https://api.weather.gov/zones/forecast/VAZ091",
                    "https://api.weather.gov/zones/forecast/VAZ094",
                    "https://api.weather.gov/zones/forecast/VAZ095",
                    "https://api.weather.gov/zones/forecast/VAZ098",
                    "https://api.weather.gov/zones/forecast/VAZ099",
                    "https://api.weather.gov/zones/forecast/VAZ100",
                    "https://api.weather.gov/zones/forecast/MDZ025",
                    "https://api.weather.gov/zones/forecast/DEZ002",
                    "https://api.weather.gov/zones/forecast/DEZ003",
                    "https://api.weather.gov/zones/forecast/DEZ004",
                    "https://api.weather.gov/zones/forecast/NJZ006",
                    "https://api.weather.gov/zones/forecast/NJZ012",
                    "https://api.weather.gov/zones/forecast/NJZ013",
                    "https://api.weather.gov/zones/forecast/NJZ014",
                    "https://api.weather.gov/zones/forecast/NJZ021",
                    "https://api.weather.gov/zones/forecast/NJZ023",
                    "https://api.weather.gov/zones/forecast/NJZ024",
                    "https://api.weather.gov/zones/forecast/NJZ025",
                    "https://api.weather.gov/zones/forecast/NJZ026",
                    "https://api.weather.gov/zones/forecast/NJZ106",
                    "https://api.weather.gov/zones/forecast/NJZ108",
                    "https://api.weather.gov/zones/forecast/NYZ071",
                    "https://api.weather.gov/zones/forecast/NYZ072",
                    "https://api.weather.gov/zones/forecast/NYZ073",
                    "https://api.weather.gov/zones/forecast/NYZ074",
                    "https://api.weather.gov/zones/forecast/NYZ075",
                    "https://api.weather.gov/zones/forecast/NYZ078",
                    "https://api.weather.gov/zones/forecast/NYZ079",
                    "https://api.weather.gov/zones/forecast/NYZ080",
                    "https://api.weather.gov/zones/forecast/NYZ081",
                    "https://api.weather.gov/zones/forecast/NYZ176",
                    "https://api.weather.gov/zones/forecast/NYZ177",
                    "https://api.weather.gov/zones/forecast/NYZ178",
                    "https://api.weather.gov/zones/forecast/NYZ179",
                    "https://api.weather.gov/zones/forecast/CTZ009",
                    "https://api.weather.gov/zones/forecast/CTZ010",
                    "https://api.weather.gov/zones/forecast/CTZ011",
                    "https://api.weather.gov/zones/forecast/CTZ012",
                    "https://api.weather.gov/zones/forecast/RIZ002",
                    "https://api.weather.gov/zones/forecast/RIZ004",
                    "https://api.weather.gov/zones/forecast/RIZ005",
                    "https://api.weather.gov/zones/forecast/RIZ006",
                    "https://api.weather.gov/zones/forecast/RIZ007",
                    "https://api.weather.gov/zones/forecast/RIZ008",
                    "https://api.weather.gov/zones/forecast/MAZ007",
                    "https://api.weather.gov/zones/forecast/MAZ016",
                    "https://api.weather.gov/zones/forecast/MAZ019",
                    "https://api.weather.gov/zones/forecast/MAZ020",
                    "https://api.weather.gov/zones/forecast/MAZ021",
                    "https://api.weather.gov/zones/forecast/MAZ022",
                    "https://api.weather.gov/zones/forecast/MAZ023",
                    "https://api.weather.gov/zones/forecast/MAZ024",
                    "https://api.weather.gov/zones/forecast/NHZ014",
                    "https://api.weather.gov/zones/forecast/MEZ022",
                    "https://api.weather.gov/zones/forecast/MEZ023",
                    "https://api.weather.gov/zones/forecast/MEZ024",
                    "https://api.weather.gov/zones/forecast/MEZ025",
                    "https://api.weather.gov/zones/forecast/MEZ026",
                    "https://api.weather.gov/zones/forecast/MEZ027",
                    "https://api.weather.gov/zones/forecast/MEZ028",
                    "https://api.weather.gov/zones/forecast/MEZ029",
                    "https://api.weather.gov/zones/forecast/MEZ030",
                    "https://api.weather.gov/zones/forecast/NBZ570",
                    "https://api.weather.gov/zones/forecast/NBZ550",
                    "https://api.weather.gov/zones/forecast/NBZ660",
                    "https://api.weather.gov/zones/forecast/NBZ641",
                    "https://api.weather.gov/zones/forecast/NSZ210",
                    "https://api.weather.gov/zones/forecast/NSZ230",
                    "https://api.weather.gov/zones/forecast/NSZ260",
                    "https://api.weather.gov/zones/forecast/NSZ250",
                    "https://api.weather.gov/zones/forecast/NSZ110",
                    "https://api.weather.gov/zones/forecast/NSZ120",
                    "https://api.weather.gov/zones/forecast/NSZ130",
                    "https://api.weather.gov/zones/forecast/NSZ170",
                    "https://api.weather.gov/zones/forecast/NSZ160",
                    "https://api.weather.gov/zones/forecast/NSZ150",
                    "https://api.weather.gov/zones/forecast/NSZ140",
                    "https://api.weather.gov/zones/forecast/NSZ270",
                    "https://api.weather.gov/zones/forecast/NSZ280",
                    "https://api.weather.gov/zones/forecast/NSZ320",
                    "https://api.weather.gov/zones/forecast/NSZ410",
                    "https://api.weather.gov/zones/forecast/NSZ450",
                    "https://api.weather.gov/zones/forecast/NSZ440",
                    "https://api.weather.gov/zones/forecast/NSZ430",
                    "https://api.weather.gov/zones/forecast/QCZ670",
                    "https://api.weather.gov/zones/forecast/QCZ680",
                    "https://api.weather.gov/zones/forecast/NLZ340",
                    "https://api.weather.gov/zones/forecast/NLZ220",
                    "https://api.weather.gov/zones/forecast/NLZ230",
                    "https://api.weather.gov/zones/forecast/NLZ210",
                    "https://api.weather.gov/zones/forecast/NLZ120",
                    "https://api.weather.gov/zones/forecast/NLZ132",
                    "https://api.weather.gov/zones/forecast/NLZ140",
                    "https://api.weather.gov/zones/forecast/NLZ241",
                    "https://api.weather.gov/zones/forecast/NLZ242",
                    "https://api.weather.gov/zones/forecast/NLZ110",
                    "https://api.weather.gov/zones/forecast/NLZ131",
                    "https://api.weather.gov/zones/forecast/NLZ540",
                    "https://api.weather.gov/zones/forecast/NLZ530",
                    "https://api.weather.gov/zones/forecast/NLZ570",
                    "https://api.weather.gov/zones/forecast/NLZ520",
                    "https://api.weather.gov/zones/forecast/NLZ510",
                    "https://api.weather.gov/zones/forecast/NLZ560",
                    "https://api.weather.gov/zones/forecast/NLZ610",
                    "https://api.weather.gov/zones/forecast/NLZ720",
                    "https://api.weather.gov/zones/forecast/NLZ710",
                    "https://api.weather.gov/zones/forecast/NLZ730",
                    "https://api.weather.gov/zones/forecast/NLZ740",
                    "https://api.weather.gov/zones/forecast/NLZ750",
                    "https://api.weather.gov/zones/forecast/NLZ760",
                    "https://api.weather.gov/zones/forecast/NLZ770"
                ],
                "references": [],
                "sent": "2022-02-15T12:16:00-05:00",
                "effective": "2022-02-15T12:16:00-05:00",
                "onset": "2022-02-15T12:16:00-05:00",
                "expires": "2022-02-15T13:16:00-05:00",
                "ends": "2022-02-15T13:16:00-05:00",
                "status": "Test",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Extreme",
                "certainty": "Likely",
                "urgency": "Immediate",
                "event": "Tsunami Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS National Tsunami Warning Center",
                "headline": "TEST Tsunami Warning issued February 15 at 12:16PM EST until February 15 at 1:16PM EST by NWS National Tsunami Warning Center",
                "description": "GMZ130-150-155-230-235-250-255-330-335-350-355-450-452-455-\n532-534-536-538-550-552-555-630>635-650-655-730-750-755-765-\n830-836-850-853-856-656-657-031-032-034-035-042>044-052>055-\nAMZ630-650-651-550-552-555-450-452-454-330-350-352-354-250-\n252-254-256-130-135-150-152-154-156-158-ANZ631>638-656-658-\n650-652-654-430-431-450>455-330-335-338-340-345-350-353-355-\n230>237-250-254-255-256-150-050>052-TXZ251-256-257-242>247-\n213-214-236>238-215-216-LAZ052>054-073-074-040-062-064-066>070-\nMSZ080>082-ALZ263>266-FLZ202-204-206-008-012-014-015-018-027-\n028-034-139-142-148-149-050-151-155-160-162-165-069-070-075-\n076>078-174-168-172-173-047-054-059-064-141-147-124-125-033-\n038-GAZ154-166-117-119-139-141-SCZ048>052-054-056-NCZ106-108-\n110-045>047-080-081-094-095-098-103-104-015>017-030>032-102-\nVAZ084-086-091-094-095-098-099-100-MDZ025-DEZ002>004-NJZ006-\n012>014-021-023>026-106-108-NYZ071>075-078>081-176>179-CTZ009>012-\nRIZ002-004>008-MAZ007-016-019>024-NHZ014-MEZ022>028-029-030-\nNBZ570-550-660-641-NSZ210-230-260-250-110-120-130-170-160-\n150-140-270-280-320-410-450-440-430-QCZ670-680-NLZ340-220-\n230-210-120-132-140-241-242-110-131-540-530-570-520-510-560-\n610-720-710-730-740-750-760-770-151816-\n/T.NEW.PAAQ.TS.W.9004.220215T1716Z-220215T1816Z/\nThe U.S. east coast, Gulf of Mexico coasts, and Eastern\nCanadian coastal areas\n\n...THIS_MESSAGE_IS_FOR_TEST_PURPOSES_ONLY...\n\n...THIS IS A TEST TO DETERMINE TRANSMISSION TIMES INVOLVED IN THE\nDISSEMINATION OF TSUNAMI INFORMATION...\n\n...ESTO ES UNA PRUEBA PARA DETERMINAR LOS TIEMPOS DE TRANSMISION\nENVUELTOS EN LA DISEMINACION DE INFORMACION SOBRE TSUNAMIS...\n\n\nRESPONSES ARE REQUIRED FROM\n---------------------------\n* All Coastal Weather Forecast Offices in the Eastern and\nSouthern Regions - respond using tsunami message\nacknowledgment (TMA) procedures. Emergency alert systems and\nNOAA Weather Radio are NOT to be activated.\n\n* State and Territorial Warning Points in ME, NH, MA, CT -\nRI, NY, NJ, DE, MD, PA, VA, NC, SC, GA, FL, AL -\nMS, LA, and TX.\n\n* Joint Typhoon Warning Center in Hawaii\n\n* Atlantic Storm Prediction Center NS, Government of Canada\nOperations Center, and Saint-Pierre et Miquelon.\n\n\nRESPONSES SHOULD INCLUDE\n------------------------\n* Time-of-receipt\n* Agency name\n* Email address\n* Phone number\n\n\nWeather Service Offices should respond in accordance with local\ndirectives. All others should reply by one of the available methods\nbelow.\n\n\nSEND RESPONSE BY\n----------------\n* Web - ntwc.arh.noaa.gov/commtest/index.html\n* Email address - ntwc@noaa.gov\n* AFTN address  - PAAQYQYX\n* AWIPS         - TMA\n* Fax           - 907-745-6071\n\n...THIS_MESSAGE_IS_FOR_TEST_PURPOSES_ONLY...\n\n...THIS IS A TEST TO DETERMINE TRANSMISSION TIMES INVOLVED IN THE\nDISSEMINATION OF TSUNAMI INFORMATION...",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "TSUAT1"
                    ],
                    "WMOidentifier": [
                        "WEXX20 PAAQ 151716"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/T.NEW.PAAQ.TS.W.9004.220215T1716Z-220215T1816Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-15T18:16:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3e3665edebfc8dfce5db10ca2db732de166d8e47.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3e3665edebfc8dfce5db10ca2db732de166d8e47.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.3e3665edebfc8dfce5db10ca2db732de166d8e47.001.1",
                "areaDesc": "Coastal Franklin",
                "geocode": {
                    "SAME": [
                        "012037"
                    ],
                    "UGC": [
                        "FLZ115"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ115"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.47fc3f5a035eb3bbcbe4d0274fffd0fb66d5213c.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.47fc3f5a035eb3bbcbe4d0274fffd0fb66d5213c.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-15T05:36:00-05:00"
                    }
                ],
                "sent": "2022-02-15T11:48:00-05:00",
                "effective": "2022-02-15T11:48:00-05:00",
                "onset": "2022-02-15T12:00:00-05:00",
                "expires": "2022-02-15T19:00:00-05:00",
                "ends": "2022-02-15T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tallahassee FL",
                "headline": "Rip Current Statement issued February 15 at 11:48AM EST until February 15 at 7:00PM EST by NWS Tallahassee FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Franklin County Beaches.\n\n* WHEN...From noon EST today through this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTAE"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTAE 151648"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT UNTIL 7 PM EST THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KTAE.RP.S.0020.220215T1700Z-220216T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-16T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.20686a5bdc10cb44e9b23afdda5536b76967b274.002.1,2021-03-11T03:28:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.20686a5bdc10cb44e9b23afdda5536b76967b274.001.1,2021-03-11T03:28:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.56e5dd27a7456d2f82888ec0663f46bc508b4bf7.001.1,2021-03-10T03:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4f46ae8f96429ce070cab50f2829d679deee143e.001.1,2021-03-10T19:16:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.5b17974030cc2f0de0244ecf9f973057a2964947.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.5b17974030cc2f0de0244ecf9f973057a2964947.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.5b17974030cc2f0de0244ecf9f973057a2964947.001.1",
                "areaDesc": "Hamilton; Suwannee; Baker; Inland Nassau; Union; Bradford; Gilchrist; Northern Columbia; Eastern Alachua; Southern Columbia; Trout River; Western Clay; Western Alachua; Western Putnam; Central Marion; Western Marion; Western Duval; Coffee; Jeff Davis; Bacon; Appling; Wayne; Atkinson; Pierce; Brantley; Inland Glynn; Echols; Clinch; Inland Camden; Northern Ware; Northeastern Charlton; Southern Ware; Western Charlton",
                "geocode": {
                    "SAME": [
                        "012047",
                        "012121",
                        "012003",
                        "012089",
                        "012125",
                        "012007",
                        "012041",
                        "012023",
                        "012001",
                        "012031",
                        "012019",
                        "012107",
                        "012083",
                        "013069",
                        "013161",
                        "013005",
                        "013001",
                        "013305",
                        "013003",
                        "013229",
                        "013025",
                        "013127",
                        "013101",
                        "013065",
                        "013039",
                        "013299",
                        "013049"
                    ],
                    "UGC": [
                        "FLZ020",
                        "FLZ021",
                        "FLZ023",
                        "FLZ024",
                        "FLZ030",
                        "FLZ031",
                        "FLZ035",
                        "FLZ122",
                        "FLZ136",
                        "FLZ222",
                        "FLZ225",
                        "FLZ232",
                        "FLZ236",
                        "FLZ237",
                        "FLZ240",
                        "FLZ340",
                        "FLZ425",
                        "GAZ132",
                        "GAZ133",
                        "GAZ134",
                        "GAZ135",
                        "GAZ136",
                        "GAZ149",
                        "GAZ151",
                        "GAZ152",
                        "GAZ153",
                        "GAZ162",
                        "GAZ163",
                        "GAZ165",
                        "GAZ250",
                        "GAZ264",
                        "GAZ350",
                        "GAZ364"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ020",
                    "https://api.weather.gov/zones/forecast/FLZ021",
                    "https://api.weather.gov/zones/forecast/FLZ023",
                    "https://api.weather.gov/zones/forecast/FLZ024",
                    "https://api.weather.gov/zones/forecast/FLZ030",
                    "https://api.weather.gov/zones/forecast/FLZ031",
                    "https://api.weather.gov/zones/forecast/FLZ035",
                    "https://api.weather.gov/zones/forecast/FLZ122",
                    "https://api.weather.gov/zones/forecast/FLZ136",
                    "https://api.weather.gov/zones/forecast/FLZ222",
                    "https://api.weather.gov/zones/forecast/FLZ225",
                    "https://api.weather.gov/zones/forecast/FLZ232",
                    "https://api.weather.gov/zones/forecast/FLZ236",
                    "https://api.weather.gov/zones/forecast/FLZ237",
                    "https://api.weather.gov/zones/forecast/FLZ240",
                    "https://api.weather.gov/zones/forecast/FLZ340",
                    "https://api.weather.gov/zones/forecast/FLZ425",
                    "https://api.weather.gov/zones/forecast/GAZ132",
                    "https://api.weather.gov/zones/forecast/GAZ133",
                    "https://api.weather.gov/zones/forecast/GAZ134",
                    "https://api.weather.gov/zones/forecast/GAZ135",
                    "https://api.weather.gov/zones/forecast/GAZ136",
                    "https://api.weather.gov/zones/forecast/GAZ149",
                    "https://api.weather.gov/zones/forecast/GAZ151",
                    "https://api.weather.gov/zones/forecast/GAZ152",
                    "https://api.weather.gov/zones/forecast/GAZ153",
                    "https://api.weather.gov/zones/forecast/GAZ162",
                    "https://api.weather.gov/zones/forecast/GAZ163",
                    "https://api.weather.gov/zones/forecast/GAZ165",
                    "https://api.weather.gov/zones/forecast/GAZ250",
                    "https://api.weather.gov/zones/forecast/GAZ264",
                    "https://api.weather.gov/zones/forecast/GAZ350",
                    "https://api.weather.gov/zones/forecast/GAZ364"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.a56c4c627476907a785b1978cd8ff828c4c974f5.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.a56c4c627476907a785b1978cd8ff828c4c974f5.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-14T14:11:00-05:00"
                    }
                ],
                "sent": "2022-02-15T07:58:00-05:00",
                "effective": "2022-02-15T07:58:00-05:00",
                "onset": "2022-02-15T07:58:00-05:00",
                "expires": "2022-02-15T08:13:40-05:00",
                "ends": "2022-02-15T09:00:00-05:00",
                "status": "Actual",
                "messageType": "Cancel",
                "category": "Met",
                "severity": "Minor",
                "certainty": "Observed",
                "urgency": "Past",
                "event": "Frost Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Jacksonville FL",
                "headline": "The Frost Advisory has been cancelled.",
                "description": "The Frost Advisory has been cancelled and is no longer in effect.",
                "instruction": null,
                "response": "AllClear",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWJAX"
                    ],
                    "WMOidentifier": [
                        "WWUS72 KJAX 151258"
                    ],
                    "NWSheadline": [
                        "FROST ADVISORY IS CANCELLED"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CAN.KJAX.FR.Y.0007.000000T0000Z-220215T1400Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-15T14:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.24c80f97b466e27a7ecf342bee19748d8cb19f40.002.1,2021-04-03T09:00:00-04:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce427079ebfa88a4bc19c788e4ede6ed1b9682dc.004.1,2021-04-02T15:55:00-04:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce427079ebfa88a4bc19c788e4ede6ed1b9682dc.003.1,2021-04-02T15:55:00-04:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.526047ae89a837358befbc95ab32fe9cd6003e17.002.1,2021-04-02T02:49:00-04:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce427079ebfa88a4bc19c788e4ede6ed1b9682dc.001.1,2021-04-02T15:55:00-04:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e1730c96e04e901b3239271e0eced1fdefa12dad.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e1730c96e04e901b3239271e0eced1fdefa12dad.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.e1730c96e04e901b3239271e0eced1fdefa12dad.001.1",
                "areaDesc": "Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County",
                "geocode": {
                    "SAME": [
                        "012099",
                        "012011",
                        "012086"
                    ],
                    "UGC": [
                        "FLZ168",
                        "FLZ172",
                        "FLZ173"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173"
                ],
                "references": [],
                "sent": "2022-02-15T07:19:00-05:00",
                "effective": "2022-02-15T07:19:00-05:00",
                "onset": "2022-02-15T07:19:00-05:00",
                "expires": "2022-02-16T07:00:00-05:00",
                "ends": "2022-02-17T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 15 at 7:19AM EST until February 17 at 7:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Broward, Coastal Palm Beach and Coastal Miami-\nDade Counties.\n\n* WHEN...Through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 151219"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH THURSDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMFL.RP.S.0008.000000T0000Z-220218T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.002.1,2022-02-14T15:40:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1,2022-02-14T02:30:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.5ab9a74add04e09ee6d096504b66cd124a4f112e.001.1",
                "areaDesc": "Mobile Coastal; Baldwin Coastal; Escambia Coastal; Santa Rosa Coastal; Okaloosa Coastal",
                "geocode": {
                    "SAME": [
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091"
                    ],
                    "UGC": [
                        "ALZ265",
                        "ALZ266",
                        "FLZ202",
                        "FLZ204",
                        "FLZ206"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ206"
                ],
                "references": [],
                "sent": "2022-02-15T06:16:00-06:00",
                "effective": "2022-02-15T06:16:00-06:00",
                "onset": "2022-02-15T18:00:00-06:00",
                "expires": "2022-02-15T15:00:00-06:00",
                "ends": "2022-02-18T18:00:00-06:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Rip Current Statement issued February 15 at 6:16AM CST until February 18 at 6:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...In Alabama, Mobile Coastal and Baldwin Coastal\nCounties. In Florida, Escambia Coastal, Santa Rosa Coastal and\nOkaloosa Coastal Counties.\n\n* WHEN...From 6 PM CST this evening through Friday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMOB"
                    ],
                    "WMOidentifier": [
                        "WHUS44 KMOB 151216"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT FROM 6 PM CST THIS EVENING THROUGH FRIDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMOB.RP.S.0008.220216T0000Z-220219T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-19T00:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.47fc3f5a035eb3bbcbe4d0274fffd0fb66d5213c.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.47fc3f5a035eb3bbcbe4d0274fffd0fb66d5213c.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.47fc3f5a035eb3bbcbe4d0274fffd0fb66d5213c.001.1",
                "areaDesc": "Coastal Franklin",
                "geocode": {
                    "SAME": [
                        "012037"
                    ],
                    "UGC": [
                        "FLZ115"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ115"
                ],
                "references": [],
                "sent": "2022-02-15T05:36:00-05:00",
                "effective": "2022-02-15T05:36:00-05:00",
                "onset": "2022-02-15T12:00:00-05:00",
                "expires": "2022-02-15T19:00:00-05:00",
                "ends": "2022-02-15T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tallahassee FL",
                "headline": "Rip Current Statement issued February 15 at 5:36AM EST until February 15 at 7:00PM EST by NWS Tallahassee FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Franklin County Beaches.\n\n* WHEN...From noon EST today through this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTAE"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTAE 151036"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT FROM NOON EST TODAY THROUGH THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KTAE.RP.S.0020.220215T1700Z-220216T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-16T00:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.07a0de5b9de04c8ce7ac72d539c10be88cc91e76.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.07a0de5b9de04c8ce7ac72d539c10be88cc91e76.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.07a0de5b9de04c8ce7ac72d539c10be88cc91e76.001.1",
                "areaDesc": "Southern Brevard County; Indian River; St. Lucie; Martin; Coastal Volusia; Northern Brevard County",
                "geocode": {
                    "SAME": [
                        "012009",
                        "012061",
                        "012111",
                        "012085",
                        "012127"
                    ],
                    "UGC": [
                        "FLZ047",
                        "FLZ054",
                        "FLZ059",
                        "FLZ064",
                        "FLZ141",
                        "FLZ147"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ047",
                    "https://api.weather.gov/zones/forecast/FLZ054",
                    "https://api.weather.gov/zones/forecast/FLZ059",
                    "https://api.weather.gov/zones/forecast/FLZ064",
                    "https://api.weather.gov/zones/forecast/FLZ141",
                    "https://api.weather.gov/zones/forecast/FLZ147"
                ],
                "references": [],
                "sent": "2022-02-15T02:55:00-05:00",
                "effective": "2022-02-15T02:55:00-05:00",
                "onset": "2022-02-15T03:00:00-05:00",
                "expires": "2022-02-16T03:00:00-05:00",
                "ends": "2022-02-16T03:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Beach Hazards Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Melbourne FL",
                "headline": "Beach Hazards Statement issued February 15 at 2:55AM EST until February 16 at 3:00AM EST by NWS Melbourne FL",
                "description": "* WHAT...Gusty easterly winds will produce rough surf and breaking\nwaves of 3 to 5 feet. There will also be a Moderate risk of\ndangerous rip currents.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through late tonight.\n\n* IMPACTS...Large breaking waves can knock you off your feet and\nmake you even more susceptible to being caught in the seaward\npull of a rip current.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMLB"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMLB 150755"
                    ],
                    "NWSheadline": [
                        "BEACH HAZARDS STATEMENT IN EFFECT THROUGH LATE TONIGHT"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMLB.BH.S.0006.220215T0800Z-220216T0800Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-16T08:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.001.1",
                "areaDesc": "Coastal Collier County",
                "geocode": {
                    "SAME": [
                        "012021"
                    ],
                    "UGC": [
                        "FLZ069"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ069"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.002.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.002.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-14T02:30:00-05:00"
                    },
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.12506781f9acb1936d781f5821586809196ae4fe.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.12506781f9acb1936d781f5821586809196ae4fe.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-13T15:13:00-05:00"
                    }
                ],
                "sent": "2022-02-14T15:40:00-05:00",
                "effective": "2022-02-14T15:40:00-05:00",
                "onset": "2022-02-14T15:40:00-05:00",
                "expires": "2022-02-14T16:45:00-05:00",
                "ends": "2022-02-14T16:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 14 at 3:40PM EST until February 14 at 4:00PM EST by NWS Miami FL",
                "description": "A moderate risk of rip currents will remain through the rest of\nthe day. Exercise caution across the area beaches.",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 142040"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK WILL EXPIRE AT 4 PM EST THIS AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXP.KMFL.RP.S.0008.000000T0000Z-220214T2100Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T21:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.002.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.002.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.8ab1daa258f2b5001adcf6ce278194982a73f559.002.1",
                "areaDesc": "Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County",
                "geocode": {
                    "SAME": [
                        "012099",
                        "012011",
                        "012086"
                    ],
                    "UGC": [
                        "FLZ168",
                        "FLZ172",
                        "FLZ173"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-14T02:30:00-05:00"
                    }
                ],
                "sent": "2022-02-14T15:40:00-05:00",
                "effective": "2022-02-14T15:40:00-05:00",
                "onset": "2022-02-14T15:40:00-05:00",
                "expires": "2022-02-15T04:00:00-05:00",
                "ends": "2022-02-17T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 14 at 3:40PM EST until February 17 at 7:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Palm Beach, Coastal Broward and Coastal Miami-\nDade Counties.\n\n* WHEN...From 7 AM EST this morning through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 142040"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH THURSDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMFL.RP.S.0008.000000T0000Z-220218T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T00:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.a56c4c627476907a785b1978cd8ff828c4c974f5.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.a56c4c627476907a785b1978cd8ff828c4c974f5.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.a56c4c627476907a785b1978cd8ff828c4c974f5.001.1",
                "areaDesc": "Hamilton; Suwannee; Baker; Inland Nassau; Union; Bradford; Gilchrist; Northern Columbia; Eastern Alachua; Southern Columbia; Trout River; Western Clay; Western Alachua; Western Putnam; Central Marion; Western Marion; Western Duval; Coffee; Jeff Davis; Bacon; Appling; Wayne; Atkinson; Pierce; Brantley; Inland Glynn; Echols; Clinch; Inland Camden; Northern Ware; Northeastern Charlton; Southern Ware; Western Charlton",
                "geocode": {
                    "SAME": [
                        "012047",
                        "012121",
                        "012003",
                        "012089",
                        "012125",
                        "012007",
                        "012041",
                        "012023",
                        "012001",
                        "012031",
                        "012019",
                        "012107",
                        "012083",
                        "013069",
                        "013161",
                        "013005",
                        "013001",
                        "013305",
                        "013003",
                        "013229",
                        "013025",
                        "013127",
                        "013101",
                        "013065",
                        "013039",
                        "013299",
                        "013049"
                    ],
                    "UGC": [
                        "FLZ020",
                        "FLZ021",
                        "FLZ023",
                        "FLZ024",
                        "FLZ030",
                        "FLZ031",
                        "FLZ035",
                        "FLZ122",
                        "FLZ136",
                        "FLZ222",
                        "FLZ225",
                        "FLZ232",
                        "FLZ236",
                        "FLZ237",
                        "FLZ240",
                        "FLZ340",
                        "FLZ425",
                        "GAZ132",
                        "GAZ133",
                        "GAZ134",
                        "GAZ135",
                        "GAZ136",
                        "GAZ149",
                        "GAZ151",
                        "GAZ152",
                        "GAZ153",
                        "GAZ162",
                        "GAZ163",
                        "GAZ165",
                        "GAZ250",
                        "GAZ264",
                        "GAZ350",
                        "GAZ364"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ020",
                    "https://api.weather.gov/zones/forecast/FLZ021",
                    "https://api.weather.gov/zones/forecast/FLZ023",
                    "https://api.weather.gov/zones/forecast/FLZ024",
                    "https://api.weather.gov/zones/forecast/FLZ030",
                    "https://api.weather.gov/zones/forecast/FLZ031",
                    "https://api.weather.gov/zones/forecast/FLZ035",
                    "https://api.weather.gov/zones/forecast/FLZ122",
                    "https://api.weather.gov/zones/forecast/FLZ136",
                    "https://api.weather.gov/zones/forecast/FLZ222",
                    "https://api.weather.gov/zones/forecast/FLZ225",
                    "https://api.weather.gov/zones/forecast/FLZ232",
                    "https://api.weather.gov/zones/forecast/FLZ236",
                    "https://api.weather.gov/zones/forecast/FLZ237",
                    "https://api.weather.gov/zones/forecast/FLZ240",
                    "https://api.weather.gov/zones/forecast/FLZ340",
                    "https://api.weather.gov/zones/forecast/FLZ425",
                    "https://api.weather.gov/zones/forecast/GAZ132",
                    "https://api.weather.gov/zones/forecast/GAZ133",
                    "https://api.weather.gov/zones/forecast/GAZ134",
                    "https://api.weather.gov/zones/forecast/GAZ135",
                    "https://api.weather.gov/zones/forecast/GAZ136",
                    "https://api.weather.gov/zones/forecast/GAZ149",
                    "https://api.weather.gov/zones/forecast/GAZ151",
                    "https://api.weather.gov/zones/forecast/GAZ152",
                    "https://api.weather.gov/zones/forecast/GAZ153",
                    "https://api.weather.gov/zones/forecast/GAZ162",
                    "https://api.weather.gov/zones/forecast/GAZ163",
                    "https://api.weather.gov/zones/forecast/GAZ165",
                    "https://api.weather.gov/zones/forecast/GAZ250",
                    "https://api.weather.gov/zones/forecast/GAZ264",
                    "https://api.weather.gov/zones/forecast/GAZ350",
                    "https://api.weather.gov/zones/forecast/GAZ364"
                ],
                "references": [],
                "sent": "2022-02-14T14:11:00-05:00",
                "effective": "2022-02-14T14:11:00-05:00",
                "onset": "2022-02-15T03:00:00-05:00",
                "expires": "2022-02-15T09:00:00-05:00",
                "ends": "2022-02-15T09:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Minor",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Frost Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Jacksonville FL",
                "headline": "Frost Advisory issued February 14 at 2:11PM EST until February 15 at 9:00AM EST by NWS Jacksonville FL",
                "description": "* WHAT...Temperatures as low as 33 will result in frost\nformation.\n\n* WHERE...Portions of southeast Georgia and northeast and\nnorthern Florida.\n\n* WHEN...From 3 AM to 9 AM EST Tuesday.\n\n* IMPACTS...Frost could kill sensitive outdoor vegetation if\nleft uncovered.",
                "instruction": "Take steps now to protect tender plants from the cold.",
                "response": "Prepare",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWJAX"
                    ],
                    "WMOidentifier": [
                        "WWUS72 KJAX 141911"
                    ],
                    "NWSheadline": [
                        "FROST ADVISORY IN EFFECT FROM 3 AM TO 9 AM EST TUESDAY"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KJAX.FR.Y.0007.220215T0800Z-220215T1400Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-15T14:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.7ea42f34e0dd6fe560556f3433696643980a2f67.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.7ea42f34e0dd6fe560556f3433696643980a2f67.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.7ea42f34e0dd6fe560556f3433696643980a2f67.001.1",
                "areaDesc": "Pinellas; Coastal Manatee; Coastal Sarasota; Coastal Charlotte; Coastal Lee",
                "geocode": {
                    "SAME": [
                        "012103",
                        "012081",
                        "012115",
                        "012015",
                        "012071"
                    ],
                    "UGC": [
                        "FLZ050",
                        "FLZ155",
                        "FLZ160",
                        "FLZ162",
                        "FLZ165"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ050",
                    "https://api.weather.gov/zones/forecast/FLZ155",
                    "https://api.weather.gov/zones/forecast/FLZ160",
                    "https://api.weather.gov/zones/forecast/FLZ162",
                    "https://api.weather.gov/zones/forecast/FLZ165"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.2e8157f01187bcffcddaa1d26798c57c8bc2d419.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.2e8157f01187bcffcddaa1d26798c57c8bc2d419.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-14T04:28:00-05:00"
                    }
                ],
                "sent": "2022-02-14T14:10:00-05:00",
                "effective": "2022-02-14T14:10:00-05:00",
                "onset": "2022-02-14T14:10:00-05:00",
                "expires": "2022-02-14T19:00:00-05:00",
                "ends": "2022-02-14T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tampa Bay Ruskin FL",
                "headline": "Rip Current Statement issued February 14 at 2:10PM EST until February 14 at 7:00PM EST by NWS Tampa Bay Ruskin FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Manatee, Pinellas, Coastal Charlotte, Coastal\nLee and Coastal Sarasota Counties.\n\n* WHEN...Until 7 PM EST this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTBW"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTBW 141910"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IS IN EFFECT UNTIL 7 PM EST THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXT.KTBW.RP.S.0006.000000T0000Z-220215T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-15T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.0271d8cb9e2d8f5dd9b31370da1763c43f5dca9c.001.1,2022-02-13T13:46:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.6886b9da86413bf671b0cf85c7bf969832284307.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.6886b9da86413bf671b0cf85c7bf969832284307.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.6886b9da86413bf671b0cf85c7bf969832284307.001.1",
                "areaDesc": "Coastal Gulf",
                "geocode": {
                    "SAME": [
                        "012045"
                    ],
                    "UGC": [
                        "FLZ114"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ114"
                ],
                "references": [],
                "sent": "2022-02-14T11:46:00-05:00",
                "effective": "2022-02-14T11:46:00-05:00",
                "onset": "2022-02-14T11:46:00-05:00",
                "expires": "2022-02-14T20:00:00-05:00",
                "ends": "2022-02-14T22:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tallahassee FL",
                "headline": "Rip Current Statement issued February 14 at 11:46AM EST until February 14 at 10:00PM EST by NWS Tallahassee FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Gulf County Beaches.\n\n* WHEN...Until 10 PM EST /9 PM CST/ this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.\n\n* ADDITIONAL DETAILS...St Joseph State Park is flying red flags\nthis morning.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTAE"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTAE 141646"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT UNTIL 10 PM EST /9 PM CST/ THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KTAE.RP.S.0019.220214T1646Z-220215T0300Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-15T03:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.92db0f4007363d10c0db656e4242051c7e034d7e.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.92db0f4007363d10c0db656e4242051c7e034d7e.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.92db0f4007363d10c0db656e4242051c7e034d7e.001.1",
                "areaDesc": "Coastal Lee; Inland Lee",
                "geocode": {
                    "SAME": [
                        "012071"
                    ],
                    "UGC": [
                        "FLZ165",
                        "FLZ265"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/fire/FLZ165",
                    "https://api.weather.gov/zones/fire/FLZ265"
                ],
                "references": [],
                "sent": "2022-02-14T10:28:00-05:00",
                "effective": "2022-02-14T10:28:00-05:00",
                "onset": "2022-02-14T10:28:00-05:00",
                "expires": "2022-02-14T17:00:00-05:00",
                "ends": "2022-02-14T17:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Red Flag Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tampa Bay Ruskin FL",
                "headline": "Red Flag Warning issued February 14 at 10:28AM EST until February 14 at 5:00PM EST by NWS Tampa Bay Ruskin FL",
                "description": "The National Weather Service in Tampa Bay Area - Ruskin FL has\nissued a Red Flag Warning, which is in effect until 5 PM EST this\nafternoon.\n\n* AFFECTED AREA...Coastal Lee...Inland Lee.\n\n* WIND...Northerly winds 15 to 20 mph and gusty.\n\n* HUMIDITY...around 25 percent.\n\n* ERC...36.\n\n* IMPACTS...any fires that develop will likely spread rapidly.\nOutdoor burning is not recommended.",
                "instruction": "A Red Flag Warning means that critical fire weather conditions\nare either occurring now....or will shortly. A combination of\nstrong winds...low relative humidity...and warm temperatures can\ncontribute to extreme fire behavior.",
                "response": "Prepare",
                "parameters": {
                    "AWIPSidentifier": [
                        "RFWTBW"
                    ],
                    "WMOidentifier": [
                        "WWUS82 KTBW 141528"
                    ],
                    "NWSheadline": [
                        "RED FLAG WARNING IN EFFECT UNTIL 5 PM EST THIS AFTERNOON FOR... low humidity, gusty winds, and high ERC values"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KTBW.FW.W.0001.220214T1528Z-220214T2200Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T22:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.6887d1103157f0439271aed7921ce8ce70e30644.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.6887d1103157f0439271aed7921ce8ce70e30644.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.6887d1103157f0439271aed7921ce8ce70e30644.001.1",
                "areaDesc": "Hamilton; Suwannee; Baker; Gilchrist; Northern Columbia; Southern Columbia; Coffee; Jeff Davis; Bacon; Appling; Wayne; Atkinson; Pierce; Brantley; Echols; Clinch; Northern Ware; Northeastern Charlton; Southern Ware; Western Charlton",
                "geocode": {
                    "SAME": [
                        "012047",
                        "012121",
                        "012003",
                        "012041",
                        "012023",
                        "013069",
                        "013161",
                        "013005",
                        "013001",
                        "013305",
                        "013003",
                        "013229",
                        "013025",
                        "013101",
                        "013065",
                        "013299",
                        "013049"
                    ],
                    "UGC": [
                        "FLZ020",
                        "FLZ021",
                        "FLZ023",
                        "FLZ035",
                        "FLZ122",
                        "FLZ222",
                        "GAZ132",
                        "GAZ133",
                        "GAZ134",
                        "GAZ135",
                        "GAZ136",
                        "GAZ149",
                        "GAZ151",
                        "GAZ152",
                        "GAZ162",
                        "GAZ163",
                        "GAZ250",
                        "GAZ264",
                        "GAZ350",
                        "GAZ364"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ020",
                    "https://api.weather.gov/zones/forecast/FLZ021",
                    "https://api.weather.gov/zones/forecast/FLZ023",
                    "https://api.weather.gov/zones/forecast/FLZ035",
                    "https://api.weather.gov/zones/forecast/FLZ122",
                    "https://api.weather.gov/zones/forecast/FLZ222",
                    "https://api.weather.gov/zones/forecast/GAZ132",
                    "https://api.weather.gov/zones/forecast/GAZ133",
                    "https://api.weather.gov/zones/forecast/GAZ134",
                    "https://api.weather.gov/zones/forecast/GAZ135",
                    "https://api.weather.gov/zones/forecast/GAZ136",
                    "https://api.weather.gov/zones/forecast/GAZ149",
                    "https://api.weather.gov/zones/forecast/GAZ151",
                    "https://api.weather.gov/zones/forecast/GAZ152",
                    "https://api.weather.gov/zones/forecast/GAZ162",
                    "https://api.weather.gov/zones/forecast/GAZ163",
                    "https://api.weather.gov/zones/forecast/GAZ250",
                    "https://api.weather.gov/zones/forecast/GAZ264",
                    "https://api.weather.gov/zones/forecast/GAZ350",
                    "https://api.weather.gov/zones/forecast/GAZ364"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.312860ff563f20184a167b8e9e0bcb4c3d3283f9.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.312860ff563f20184a167b8e9e0bcb4c3d3283f9.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-13T14:07:00-05:00"
                    }
                ],
                "sent": "2022-02-14T08:40:00-05:00",
                "effective": "2022-02-14T08:40:00-05:00",
                "onset": "2022-02-14T08:40:00-05:00",
                "expires": "2022-02-14T09:45:00-05:00",
                "ends": "2022-02-14T09:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Freeze Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Jacksonville FL",
                "headline": "Freeze Warning issued February 14 at 8:40AM EST until February 14 at 9:00AM EST by NWS Jacksonville FL",
                "description": null,
                "instruction": null,
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWJAX"
                    ],
                    "WMOidentifier": [
                        "WWUS72 KJAX 141340"
                    ],
                    "NWSheadline": [
                        "FREEZE WARNING WILL EXPIRE AT 9 AM EST THIS MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXP.KJAX.FZ.W.0008.000000T0000Z-220214T1400Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T14:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9ada27a84f05ba213942991dfcea6696abbd6eef.001.1,2021-11-30T08:16:47-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1039181152a52505e8a33cda4e2966866cb7cd3.001.2,2021-11-30T02:55:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.2e8157f01187bcffcddaa1d26798c57c8bc2d419.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.2e8157f01187bcffcddaa1d26798c57c8bc2d419.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.2e8157f01187bcffcddaa1d26798c57c8bc2d419.001.1",
                "areaDesc": "Pinellas; Coastal Manatee; Coastal Sarasota; Coastal Charlotte; Coastal Lee",
                "geocode": {
                    "SAME": [
                        "012103",
                        "012081",
                        "012115",
                        "012015",
                        "012071"
                    ],
                    "UGC": [
                        "FLZ050",
                        "FLZ155",
                        "FLZ160",
                        "FLZ162",
                        "FLZ165"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ050",
                    "https://api.weather.gov/zones/forecast/FLZ155",
                    "https://api.weather.gov/zones/forecast/FLZ160",
                    "https://api.weather.gov/zones/forecast/FLZ162",
                    "https://api.weather.gov/zones/forecast/FLZ165"
                ],
                "references": [],
                "sent": "2022-02-14T04:28:00-05:00",
                "effective": "2022-02-14T04:28:00-05:00",
                "onset": "2022-02-14T04:28:00-05:00",
                "expires": "2022-02-14T15:00:00-05:00",
                "ends": "2022-02-14T15:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tampa Bay Ruskin FL",
                "headline": "Rip Current Statement issued February 14 at 4:28AM EST until February 14 at 3:00PM EST by NWS Tampa Bay Ruskin FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Manatee, Pinellas, Coastal Charlotte, Coastal\nLee and Coastal Sarasota Counties.\n\n* WHEN...Until 3 PM EST this afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTBW"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTBW 140928"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT UNTIL 3 PM EST THIS AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KTBW.RP.S.0006.000000T0000Z-220214T2000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T20:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.0271d8cb9e2d8f5dd9b31370da1763c43f5dca9c.001.1,2022-02-13T13:46:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.dac67706e899518ac288330d52ae9a4b08651499.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.dac67706e899518ac288330d52ae9a4b08651499.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.dac67706e899518ac288330d52ae9a4b08651499.001.1",
                "areaDesc": "Southern Brevard County; Indian River; St. Lucie; Martin; Coastal Volusia; Northern Brevard County",
                "geocode": {
                    "SAME": [
                        "012009",
                        "012061",
                        "012111",
                        "012085",
                        "012127"
                    ],
                    "UGC": [
                        "FLZ047",
                        "FLZ054",
                        "FLZ059",
                        "FLZ064",
                        "FLZ141",
                        "FLZ147"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ047",
                    "https://api.weather.gov/zones/forecast/FLZ054",
                    "https://api.weather.gov/zones/forecast/FLZ059",
                    "https://api.weather.gov/zones/forecast/FLZ064",
                    "https://api.weather.gov/zones/forecast/FLZ141",
                    "https://api.weather.gov/zones/forecast/FLZ147"
                ],
                "references": [],
                "sent": "2022-02-14T03:32:00-05:00",
                "effective": "2022-02-14T03:32:00-05:00",
                "onset": "2022-02-14T03:32:00-05:00",
                "expires": "2022-02-14T19:00:00-05:00",
                "ends": "2022-02-14T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Beach Hazards Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Melbourne FL",
                "headline": "Beach Hazards Statement issued February 14 at 3:32AM EST until February 14 at 7:00PM EST by NWS Melbourne FL",
                "description": "* WHAT...A strong current flowing southward, parallel to the\ncoast, within the surf zone and a moderate risk of life-\nthreatening rip currents.\n\n* WHERE...Southern Brevard, Indian River, St. Lucie, Martin,\nCoastal Volusia and Northern Brevard Counties.\n\n* WHEN...Through this evening.\n\n* IMPACTS...Strong currents can knock you off your feet and make\nyou even more susceptible to becoming caught in the seaward pull\nof a rip current.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMLB"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMLB 140832"
                    ],
                    "NWSheadline": [
                        "BEACH HAZARDS STATEMENT IN EFFECT THROUGH THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMLB.BH.S.0005.220214T0832Z-220215T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-15T00:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.002.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.002.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.002.1",
                "areaDesc": "Coastal Collier County",
                "geocode": {
                    "SAME": [
                        "012021"
                    ],
                    "UGC": [
                        "FLZ069"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ069"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.12506781f9acb1936d781f5821586809196ae4fe.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.12506781f9acb1936d781f5821586809196ae4fe.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-13T15:13:00-05:00"
                    }
                ],
                "sent": "2022-02-14T02:30:00-05:00",
                "effective": "2022-02-14T02:30:00-05:00",
                "onset": "2022-02-14T02:30:00-05:00",
                "expires": "2022-02-14T16:00:00-05:00",
                "ends": "2022-02-14T16:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 14 at 2:30AM EST until February 14 at 4:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Collier County.\n\n* WHEN...From 1 AM EST Monday through Monday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 140730"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH THIS AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMFL.RP.S.0008.000000T0000Z-220214T2100Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T21:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.fec9ddf607fa5a50d57d9de472be30697ed5ba37.001.1",
                "areaDesc": "Coastal Palm Beach County; Coastal Broward County; Coastal Miami Dade County",
                "geocode": {
                    "SAME": [
                        "012099",
                        "012011",
                        "012086"
                    ],
                    "UGC": [
                        "FLZ168",
                        "FLZ172",
                        "FLZ173"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168",
                    "https://api.weather.gov/zones/forecast/FLZ172",
                    "https://api.weather.gov/zones/forecast/FLZ173"
                ],
                "references": [],
                "sent": "2022-02-14T02:30:00-05:00",
                "effective": "2022-02-14T02:30:00-05:00",
                "onset": "2022-02-14T07:00:00-05:00",
                "expires": "2022-02-14T19:00:00-05:00",
                "ends": "2022-02-17T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 14 at 2:30AM EST until February 17 at 7:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Palm Beach, Coastal Broward and Coastal Miami-\nDade Counties.\n\n* WHEN...From 7 AM EST this morning through Thursday evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 140730"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT FROM 7 AM EST THIS MORNING THROUGH THURSDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXB.KMFL.RP.S.0008.220214T1200Z-220218T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-18T00:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.519d072a5fbed6e80aa797e37c63c3b4904ce7c5.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.519d072a5fbed6e80aa797e37c63c3b4904ce7c5.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.519d072a5fbed6e80aa797e37c63c3b4904ce7c5.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.8c06bbed75a2cf82dde03dc79ff56980892dd548.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.8c06bbed75a2cf82dde03dc79ff56980892dd548.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-13T10:15:00-06:00"
                    }
                ],
                "sent": "2022-02-13T18:21:00-06:00",
                "effective": "2022-02-13T18:21:00-06:00",
                "onset": "2022-02-13T18:21:00-06:00",
                "expires": "2022-02-13T18:36:43-06:00",
                "ends": "2022-02-13T18:21:00-06:00",
                "status": "Actual",
                "messageType": "Cancel",
                "category": "Met",
                "severity": "Minor",
                "certainty": "Observed",
                "urgency": "Past",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "The Flood Warning has been cancelled.",
                "description": "The Flood Warning has been cancelled and is no longer in effect.",
                "instruction": null,
                "response": "AllClear",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 140021"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING IS CANCELLED"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.CAN.KMOB.FL.W.0012.000000T0000Z-220214T0021Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T00:21:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.95e3a913ecc133ebbbbdbf517a54d08f65e9c49c.001.1,2022-02-12T19:06:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c20fd90073fab7a2cab0aa098c77d8140331e0ef.001.1,2022-02-12T10:06:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1,2022-02-11T19:17:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1,2022-02-11T09:46:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1,2022-02-10T20:04:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1,2022-02-10T10:01:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1,2022-02-09T19:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1,2022-02-09T10:26:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.12506781f9acb1936d781f5821586809196ae4fe.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.12506781f9acb1936d781f5821586809196ae4fe.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.12506781f9acb1936d781f5821586809196ae4fe.001.1",
                "areaDesc": "Coastal Collier County",
                "geocode": {
                    "SAME": [
                        "012021"
                    ],
                    "UGC": [
                        "FLZ069"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ069"
                ],
                "references": [],
                "sent": "2022-02-13T15:13:00-05:00",
                "effective": "2022-02-13T15:13:00-05:00",
                "onset": "2022-02-14T01:00:00-05:00",
                "expires": "2022-02-14T16:00:00-05:00",
                "ends": "2022-02-14T16:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 13 at 3:13PM EST until February 14 at 4:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Coastal Collier County.\n\n* WHEN...From 1 AM EST Monday through Monday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 132013"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT FROM 1 AM EST MONDAY THROUGH MONDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMFL.RP.S.0008.220214T0600Z-220214T2100Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T21:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.312860ff563f20184a167b8e9e0bcb4c3d3283f9.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.312860ff563f20184a167b8e9e0bcb4c3d3283f9.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.312860ff563f20184a167b8e9e0bcb4c3d3283f9.001.1",
                "areaDesc": "Hamilton; Suwannee; Baker; Gilchrist; Northern Columbia; Southern Columbia; Coffee; Jeff Davis; Bacon; Appling; Wayne; Atkinson; Pierce; Brantley; Echols; Clinch; Northern Ware; Northeastern Charlton; Southern Ware; Western Charlton",
                "geocode": {
                    "SAME": [
                        "012047",
                        "012121",
                        "012003",
                        "012041",
                        "012023",
                        "013069",
                        "013161",
                        "013005",
                        "013001",
                        "013305",
                        "013003",
                        "013229",
                        "013025",
                        "013101",
                        "013065",
                        "013299",
                        "013049"
                    ],
                    "UGC": [
                        "FLZ020",
                        "FLZ021",
                        "FLZ023",
                        "FLZ035",
                        "FLZ122",
                        "FLZ222",
                        "GAZ132",
                        "GAZ133",
                        "GAZ134",
                        "GAZ135",
                        "GAZ136",
                        "GAZ149",
                        "GAZ151",
                        "GAZ152",
                        "GAZ162",
                        "GAZ163",
                        "GAZ250",
                        "GAZ264",
                        "GAZ350",
                        "GAZ364"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ020",
                    "https://api.weather.gov/zones/forecast/FLZ021",
                    "https://api.weather.gov/zones/forecast/FLZ023",
                    "https://api.weather.gov/zones/forecast/FLZ035",
                    "https://api.weather.gov/zones/forecast/FLZ122",
                    "https://api.weather.gov/zones/forecast/FLZ222",
                    "https://api.weather.gov/zones/forecast/GAZ132",
                    "https://api.weather.gov/zones/forecast/GAZ133",
                    "https://api.weather.gov/zones/forecast/GAZ134",
                    "https://api.weather.gov/zones/forecast/GAZ135",
                    "https://api.weather.gov/zones/forecast/GAZ136",
                    "https://api.weather.gov/zones/forecast/GAZ149",
                    "https://api.weather.gov/zones/forecast/GAZ151",
                    "https://api.weather.gov/zones/forecast/GAZ152",
                    "https://api.weather.gov/zones/forecast/GAZ162",
                    "https://api.weather.gov/zones/forecast/GAZ163",
                    "https://api.weather.gov/zones/forecast/GAZ250",
                    "https://api.weather.gov/zones/forecast/GAZ264",
                    "https://api.weather.gov/zones/forecast/GAZ350",
                    "https://api.weather.gov/zones/forecast/GAZ364"
                ],
                "references": [],
                "sent": "2022-02-13T14:07:00-05:00",
                "effective": "2022-02-13T14:07:00-05:00",
                "onset": "2022-02-14T03:00:00-05:00",
                "expires": "2022-02-14T09:00:00-05:00",
                "ends": "2022-02-14T09:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Freeze Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Jacksonville FL",
                "headline": "Freeze Warning issued February 13 at 2:07PM EST until February 14 at 9:00AM EST by NWS Jacksonville FL",
                "description": "* WHAT...Sub-freezing temperatures as low as 31 expected.\n\n* WHERE...Portions of southeast Georgia and northeast and\nnorthern Florida.\n\n* WHEN...From 3 AM to 9 AM EST Monday.\n\n* IMPACTS...Frost and freeze conditions could kill crops and\nother sensitive vegetation.",
                "instruction": "Appropriate action should be taken to ensure tender vegetation\nand outdoor pets have adequate protection from the cold\ntemperatures. Young children, the elderly and the homeless are\nespecially vulnerable to the cold. Take measures to protect them.",
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWJAX"
                    ],
                    "WMOidentifier": [
                        "WWUS72 KJAX 131907"
                    ],
                    "NWSheadline": [
                        "FREEZE WARNING IN EFFECT FROM 3 AM TO 9 AM EST MONDAY"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KJAX.FZ.W.0008.220214T0800Z-220214T1400Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T14:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.0271d8cb9e2d8f5dd9b31370da1763c43f5dca9c.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.0271d8cb9e2d8f5dd9b31370da1763c43f5dca9c.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.0271d8cb9e2d8f5dd9b31370da1763c43f5dca9c.001.1",
                "areaDesc": "Pinellas; Coastal Manatee; Coastal Sarasota; Coastal Charlotte; Coastal Lee",
                "geocode": {
                    "SAME": [
                        "012103",
                        "012081",
                        "012115",
                        "012015",
                        "012071"
                    ],
                    "UGC": [
                        "FLZ050",
                        "FLZ155",
                        "FLZ160",
                        "FLZ162",
                        "FLZ165"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ050",
                    "https://api.weather.gov/zones/forecast/FLZ155",
                    "https://api.weather.gov/zones/forecast/FLZ160",
                    "https://api.weather.gov/zones/forecast/FLZ162",
                    "https://api.weather.gov/zones/forecast/FLZ165"
                ],
                "references": [],
                "sent": "2022-02-13T13:46:00-05:00",
                "effective": "2022-02-13T13:46:00-05:00",
                "onset": "2022-02-13T16:00:00-05:00",
                "expires": "2022-02-13T22:00:00-05:00",
                "ends": "2022-02-14T15:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tampa Bay Ruskin FL",
                "headline": "Rip Current Statement issued February 13 at 1:46PM EST until February 14 at 3:00PM EST by NWS Tampa Bay Ruskin FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...Pinellas, Coastal Manatee, Coastal Sarasota, Coastal\nCharlotte and Coastal Lee Counties.\n\n* WHEN...Through Monday afternoon.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTBW"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTBW 131846"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT THROUGH MONDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KTBW.RP.S.0006.220213T2100Z-220214T2000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T20:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.8c06bbed75a2cf82dde03dc79ff56980892dd548.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.8c06bbed75a2cf82dde03dc79ff56980892dd548.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.8c06bbed75a2cf82dde03dc79ff56980892dd548.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.95e3a913ecc133ebbbbdbf517a54d08f65e9c49c.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.95e3a913ecc133ebbbbdbf517a54d08f65e9c49c.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-12T19:06:00-06:00"
                    }
                ],
                "sent": "2022-02-13T10:15:00-06:00",
                "effective": "2022-02-13T10:15:00-06:00",
                "onset": "2022-02-13T10:15:00-06:00",
                "expires": "2022-02-13T20:00:00-06:00",
                "ends": "2022-02-13T20:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 13 at 10:15AM CST until February 13 at 8:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until this evening.\n\n* IMPACTS...At 17 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 10:05 AM CST Sunday the stage was 17.1 feet.\n- Forecast...The river is expected to fall below flood stage\nthis afternoon and continue falling to 12.6 feet Friday\nmorning.\n- Flood stage is 17 feet.\n- www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 131615"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220214T0200Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T02:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c20fd90073fab7a2cab0aa098c77d8140331e0ef.001.1,2022-02-12T10:06:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1,2022-02-11T19:17:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1,2022-02-11T09:46:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1,2022-02-10T20:04:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1,2022-02-10T10:01:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1,2022-02-09T19:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1,2022-02-09T10:26:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.41227283bc7b27ea288a463e147ec0122c56b422.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.41227283bc7b27ea288a463e147ec0122c56b422.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.41227283bc7b27ea288a463e147ec0122c56b422.001.1",
                "areaDesc": "Coastal Gulf",
                "geocode": {
                    "SAME": [
                        "012045"
                    ],
                    "UGC": [
                        "FLZ114"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ114"
                ],
                "references": [],
                "sent": "2022-02-13T06:32:00-05:00",
                "effective": "2022-02-13T06:32:00-05:00",
                "onset": "2022-02-13T07:00:00-05:00",
                "expires": "2022-02-14T07:00:00-05:00",
                "ends": "2022-02-14T07:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tallahassee FL",
                "headline": "Rip Current Statement issued February 13 at 6:32AM EST until February 14 at 7:00AM EST by NWS Tallahassee FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...West-Facing Gulf County Beaches.\n\n* WHEN...Through Monday morning.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTAE"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTAE 131132"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH MONDAY MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KTAE.RP.S.0018.220213T1200Z-220214T1200Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T12:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.162e71115ba1b7e031e4bd0f51ca576350d909ea.001.1,2022-02-12T11:39:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2ce858943e1426c001b9af68d7d1fbe9f313ccfe.002.1,2021-03-02T13:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b0ddca9a23d8235db0ed97a4dc56c176c4a47a0b.001.1,2021-03-01T19:54:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.130da72a76fdf5f131009d7f98a5b97ce5da8327.001.1,2021-02-28T20:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b288df3a2d2d44d26c94fae9f955c481b785f388.001.1,2021-03-01T02:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.130da72a76fdf5f131009d7f98a5b97ce5da8327.002.1,2021-02-28T20:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a36990ac178318fbbb4020dce94a287876cd706c.001.1,2021-02-28T02:33:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.83df0c5dbd72b3e351d14d99f1f9b7a6eb976458.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.83df0c5dbd72b3e351d14d99f1f9b7a6eb976458.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.83df0c5dbd72b3e351d14d99f1f9b7a6eb976458.001.1",
                "areaDesc": "Choctaw; Washington; Clarke; Wilcox; Monroe; Conecuh; Butler; Crenshaw; Escambia; Covington; Mobile Inland; Baldwin Inland; Mobile Central; Baldwin Central; Mobile Coastal; Baldwin Coastal; Escambia Inland; Escambia Coastal; Santa Rosa Inland; Santa Rosa Coastal; Okaloosa Inland; Okaloosa Coastal; Wayne; Perry; Greene; Stone; George",
                "geocode": {
                    "SAME": [
                        "001023",
                        "001129",
                        "001025",
                        "001131",
                        "001099",
                        "001035",
                        "001013",
                        "001041",
                        "001053",
                        "001039",
                        "001097",
                        "001003",
                        "012033",
                        "012113",
                        "012091",
                        "028153",
                        "028111",
                        "028041",
                        "028131",
                        "028039"
                    ],
                    "UGC": [
                        "ALZ051",
                        "ALZ052",
                        "ALZ053",
                        "ALZ054",
                        "ALZ055",
                        "ALZ056",
                        "ALZ057",
                        "ALZ058",
                        "ALZ059",
                        "ALZ060",
                        "ALZ261",
                        "ALZ262",
                        "ALZ263",
                        "ALZ264",
                        "ALZ265",
                        "ALZ266",
                        "FLZ201",
                        "FLZ202",
                        "FLZ203",
                        "FLZ204",
                        "FLZ205",
                        "FLZ206",
                        "MSZ067",
                        "MSZ075",
                        "MSZ076",
                        "MSZ078",
                        "MSZ079"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ051",
                    "https://api.weather.gov/zones/forecast/ALZ052",
                    "https://api.weather.gov/zones/forecast/ALZ053",
                    "https://api.weather.gov/zones/forecast/ALZ054",
                    "https://api.weather.gov/zones/forecast/ALZ055",
                    "https://api.weather.gov/zones/forecast/ALZ056",
                    "https://api.weather.gov/zones/forecast/ALZ057",
                    "https://api.weather.gov/zones/forecast/ALZ058",
                    "https://api.weather.gov/zones/forecast/ALZ059",
                    "https://api.weather.gov/zones/forecast/ALZ060",
                    "https://api.weather.gov/zones/forecast/ALZ261",
                    "https://api.weather.gov/zones/forecast/ALZ262",
                    "https://api.weather.gov/zones/forecast/ALZ263",
                    "https://api.weather.gov/zones/forecast/ALZ264",
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ201",
                    "https://api.weather.gov/zones/forecast/FLZ202",
                    "https://api.weather.gov/zones/forecast/FLZ203",
                    "https://api.weather.gov/zones/forecast/FLZ204",
                    "https://api.weather.gov/zones/forecast/FLZ205",
                    "https://api.weather.gov/zones/forecast/FLZ206",
                    "https://api.weather.gov/zones/forecast/MSZ067",
                    "https://api.weather.gov/zones/forecast/MSZ075",
                    "https://api.weather.gov/zones/forecast/MSZ076",
                    "https://api.weather.gov/zones/forecast/MSZ078",
                    "https://api.weather.gov/zones/forecast/MSZ079"
                ],
                "references": [],
                "sent": "2022-02-13T05:09:00-06:00",
                "effective": "2022-02-13T05:09:00-06:00",
                "onset": "2022-02-13T05:09:00-06:00",
                "expires": "2022-02-13T15:00:00-06:00",
                "ends": null,
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Observed",
                "urgency": "Expected",
                "event": "Special Weather Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Special Weather Statement issued February 13 at 5:09AM CST by NWS Mobile AL",
                "description": "...MUCH DRIER AIR AND INCREASED NORTHERLY WINDS WILL BRING AN\nELEVATED FIRE DANGER TO THE AREA THIS AFTERNOON...\n\nA much colder airmass is moving into southeast Mississippi and\ninterior southwest Alabama early this morning, with temperatures\nfalling into the mid to upper 30s. An approaching upper level\nsystem will bring an area of light precipitation to much of\nsoutheast Mississippi and southwest Alabama through around 9 AM\nCST this morning. Temperatures may be cold enough for light rain\nto briefly mix with or change to light snow flurries across\nlocations generally along and northwest of the Interstate 65\ncorridor early this morning. The wintry mix potential will be very\nbrief and no accumulations or impacts are anticipated.\n\nA much drier airmass will spread across the region in the wake of\nthe passing system this morning. Very low afternoon relative\nhumidity values in combination with increased northerly winds\nwill lead to elevated fire danger across the area this afternoon.\nExercise extreme caution if using flammable materials outdoors\ntoday.",
                "instruction": null,
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "SPSMOB"
                    ],
                    "WMOidentifier": [
                        "WWUS84 KMOB 131109"
                    ],
                    "NWSheadline": [
                        "A BRIEF WINTRY MIX OF LIGHT RAIN AND LIGHT SNOW WILL BE POSSIBLE ACROSS SOUTHEAST MISSISSIPPI AND INTERIOR SOUTHWEST ALABAMA EARLY THIS MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.95e3a913ecc133ebbbbdbf517a54d08f65e9c49c.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.95e3a913ecc133ebbbbdbf517a54d08f65e9c49c.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.95e3a913ecc133ebbbbdbf517a54d08f65e9c49c.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-11T19:17:00-06:00"
                    },
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.c20fd90073fab7a2cab0aa098c77d8140331e0ef.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.c20fd90073fab7a2cab0aa098c77d8140331e0ef.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-12T10:06:00-06:00"
                    }
                ],
                "sent": "2022-02-12T19:06:00-06:00",
                "effective": "2022-02-12T19:06:00-06:00",
                "onset": "2022-02-12T19:06:00-06:00",
                "expires": "2022-02-13T15:00:00-06:00",
                "ends": "2022-02-13T15:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 12 at 7:06PM CST until February 13 at 3:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring and minor flooding is forecast.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until tomorrow afternoon.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 6:05 PM CST Saturday the stage was 17.8 feet.\n- Recent Activity...The maximum river stage in the 24 hours\nending at 6:05 PM CST Saturday was 18.1 feet.\n- Forecast...The river is expected to fall below flood stage\nlate tomorrow morning and continue falling to 12.5 feet\nThursday evening.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 130106"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL TOMORROW AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220213T2100Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-13T21:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1,2022-02-11T09:46:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1,2022-02-10T20:04:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1,2022-02-10T10:01:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1,2022-02-09T19:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1,2022-02-09T10:26:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.162e71115ba1b7e031e4bd0f51ca576350d909ea.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.162e71115ba1b7e031e4bd0f51ca576350d909ea.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.162e71115ba1b7e031e4bd0f51ca576350d909ea.001.1",
                "areaDesc": "Coastal Gulf",
                "geocode": {
                    "SAME": [
                        "012045"
                    ],
                    "UGC": [
                        "FLZ114"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ114"
                ],
                "references": [],
                "sent": "2022-02-12T11:39:00-05:00",
                "effective": "2022-02-12T11:39:00-05:00",
                "onset": "2022-02-13T07:00:00-05:00",
                "expires": "2022-02-13T04:00:00-05:00",
                "ends": "2022-02-14T07:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tallahassee FL",
                "headline": "Rip Current Statement issued February 12 at 11:39AM EST until February 14 at 7:00AM EST by NWS Tallahassee FL",
                "description": "* WHAT...Dangerous rip currents expected.\n\n* WHERE...West-Facing Gulf County Beaches.\n\n* WHEN...From Sunday morning through Monday morning.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWTAE"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KTAE 121639"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IN EFFECT FROM SUNDAY MORNING THROUGH MONDAY MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KTAE.RP.S.0018.220213T1200Z-220214T1200Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T12:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.c20fd90073fab7a2cab0aa098c77d8140331e0ef.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.c20fd90073fab7a2cab0aa098c77d8140331e0ef.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.c20fd90073fab7a2cab0aa098c77d8140331e0ef.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-11T19:17:00-06:00"
                    }
                ],
                "sent": "2022-02-12T10:06:00-06:00",
                "effective": "2022-02-12T10:06:00-06:00",
                "onset": "2022-02-12T10:06:00-06:00",
                "expires": "2022-02-13T10:15:00-06:00",
                "ends": "2022-02-13T16:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 12 at 10:06AM CST until February 13 at 4:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until tomorrow afternoon.\n\n* IMPACTS...At 17 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 9:05 AM CST Saturday the stage was 18 feet.\n- Forecast...The river is expected to crest at 18 feet this\nafternoon. It will then fall below flood stage late tomorrow\nmorning.\n- Flood stage is 17 feet.\n- www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 121606"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL TOMORROW AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220213T2200Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-13T22:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1,2022-02-11T09:46:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1,2022-02-10T20:04:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1,2022-02-10T10:01:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1,2022-02-09T19:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1,2022-02-09T10:26:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.b79cb375bf9e4f4e644ff9f8bd40477d38b424c1.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.b79cb375bf9e4f4e644ff9f8bd40477d38b424c1.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.b79cb375bf9e4f4e644ff9f8bd40477d38b424c1.001.1",
                "areaDesc": "Choctaw; Washington; Clarke; Wilcox; Monroe; Conecuh; Butler; Escambia; Mobile Inland; Baldwin Inland; Mobile Central; Baldwin Central; Mobile Coastal; Baldwin Coastal; Escambia Inland; Wayne; Perry; Greene; Stone; George",
                "geocode": {
                    "SAME": [
                        "001023",
                        "001129",
                        "001025",
                        "001131",
                        "001099",
                        "001035",
                        "001013",
                        "001053",
                        "001097",
                        "001003",
                        "012033",
                        "028153",
                        "028111",
                        "028041",
                        "028131",
                        "028039"
                    ],
                    "UGC": [
                        "ALZ051",
                        "ALZ052",
                        "ALZ053",
                        "ALZ054",
                        "ALZ055",
                        "ALZ056",
                        "ALZ057",
                        "ALZ059",
                        "ALZ261",
                        "ALZ262",
                        "ALZ263",
                        "ALZ264",
                        "ALZ265",
                        "ALZ266",
                        "FLZ201",
                        "MSZ067",
                        "MSZ075",
                        "MSZ076",
                        "MSZ078",
                        "MSZ079"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ051",
                    "https://api.weather.gov/zones/forecast/ALZ052",
                    "https://api.weather.gov/zones/forecast/ALZ053",
                    "https://api.weather.gov/zones/forecast/ALZ054",
                    "https://api.weather.gov/zones/forecast/ALZ055",
                    "https://api.weather.gov/zones/forecast/ALZ056",
                    "https://api.weather.gov/zones/forecast/ALZ057",
                    "https://api.weather.gov/zones/forecast/ALZ059",
                    "https://api.weather.gov/zones/forecast/ALZ261",
                    "https://api.weather.gov/zones/forecast/ALZ262",
                    "https://api.weather.gov/zones/forecast/ALZ263",
                    "https://api.weather.gov/zones/forecast/ALZ264",
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ201",
                    "https://api.weather.gov/zones/forecast/MSZ067",
                    "https://api.weather.gov/zones/forecast/MSZ075",
                    "https://api.weather.gov/zones/forecast/MSZ076",
                    "https://api.weather.gov/zones/forecast/MSZ078",
                    "https://api.weather.gov/zones/forecast/MSZ079"
                ],
                "references": [],
                "sent": "2022-02-12T10:02:00-06:00",
                "effective": "2022-02-12T10:02:00-06:00",
                "onset": "2022-02-12T10:02:00-06:00",
                "expires": "2022-02-12T11:15:00-06:00",
                "ends": "2022-02-12T10:00:00-06:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Dense Fog Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Dense Fog Advisory issued February 12 at 10:02AM CST until February 12 at 10:00AM CST by NWS Mobile AL",
                "description": "Surface observations across the entire area are reporting that\nvisibilities have improved above dense fog advisory criteria.\nVisible satellite imagery is showing some residual valley fog\nremaining, and should dissipate through noon. Therefore, the\nDense Fog Advisory has been allowed to expire.",
                "instruction": null,
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWMOB"
                    ],
                    "WMOidentifier": [
                        "WWUS74 KMOB 121602"
                    ],
                    "NWSheadline": [
                        "DENSE FOG ADVISORY HAS EXPIRED"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXP.KMOB.FG.Y.0002.000000T0000Z-220212T1600Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-12T16:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e1f16a6d51d55dd57c81ef2046734c2e6b7e6278.001.1,2022-02-12T06:34:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e1f16a6d51d55dd57c81ef2046734c2e6b7e6278.002.1,2022-02-12T06:34:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.001.1,2022-02-12T04:11:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.002.1,2022-02-12T04:11:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7d6f20b35760d1281e08132d65ed98b559436c98.001.1,2022-02-11T18:17:00-06:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.668204d952109e8736729836d62edc04f71bcc75.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.668204d952109e8736729836d62edc04f71bcc75.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.668204d952109e8736729836d62edc04f71bcc75.001.1",
                "areaDesc": "Glades; Hendry",
                "geocode": {
                    "SAME": [
                        "012043",
                        "012051"
                    ],
                    "UGC": [
                        "FLZ063",
                        "FLZ066"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ063",
                    "https://api.weather.gov/zones/forecast/FLZ066"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fd48c1e9a8c3ecc6555f656c9c94a8206f4b2796.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.fd48c1e9a8c3ecc6555f656c9c94a8206f4b2796.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-12T04:13:00-05:00"
                    }
                ],
                "sent": "2022-02-12T07:46:00-05:00",
                "effective": "2022-02-12T07:46:00-05:00",
                "onset": "2022-02-12T07:46:00-05:00",
                "expires": "2022-02-12T09:00:00-05:00",
                "ends": "2022-02-12T08:00:00-05:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Dense Fog Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Dense Fog Advisory issued February 12 at 7:46AM EST until February 12 at 8:00AM EST by NWS Miami FL",
                "description": "Fog has begun to dissipate and will continue to become less dense\nas the morning progresses and thus the advisory will be allowed to\nexpire. However, patchy fog and localized visibility restrictions\nwill remain possible for the next hour or so in the area.",
                "instruction": null,
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWMFL"
                    ],
                    "WMOidentifier": [
                        "WWUS72 KMFL 121246"
                    ],
                    "NWSheadline": [
                        "DENSE FOG ADVISORY WILL EXPIRE AT 8 AM EST THIS MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXP.KMFL.FG.Y.0006.000000T0000Z-220212T1300Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-12T13:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f20c116459d92c37b0586b55991d047bd57034.002.1,2021-02-09T08:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fd19b3700e596c6a3fc16c96e2d64894578420a6.001.1,2021-02-09T01:47:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f20c116459d92c37b0586b55991d047bd57034.001.1,2021-02-09T08:34:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e1f16a6d51d55dd57c81ef2046734c2e6b7e6278.002.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e1f16a6d51d55dd57c81ef2046734c2e6b7e6278.002.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.e1f16a6d51d55dd57c81ef2046734c2e6b7e6278.002.1",
                "areaDesc": "Choctaw; Washington; Clarke; Monroe; Conecuh; Escambia; Mobile Inland; Baldwin Inland; Mobile Central; Baldwin Central; Mobile Coastal; Baldwin Coastal; Escambia Inland; Wayne; Perry; Greene; Stone; George",
                "geocode": {
                    "SAME": [
                        "001023",
                        "001129",
                        "001025",
                        "001099",
                        "001035",
                        "001053",
                        "001097",
                        "001003",
                        "012033",
                        "028153",
                        "028111",
                        "028041",
                        "028131",
                        "028039"
                    ],
                    "UGC": [
                        "ALZ051",
                        "ALZ052",
                        "ALZ053",
                        "ALZ055",
                        "ALZ056",
                        "ALZ059",
                        "ALZ261",
                        "ALZ262",
                        "ALZ263",
                        "ALZ264",
                        "ALZ265",
                        "ALZ266",
                        "FLZ201",
                        "MSZ067",
                        "MSZ075",
                        "MSZ076",
                        "MSZ078",
                        "MSZ079"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ051",
                    "https://api.weather.gov/zones/forecast/ALZ052",
                    "https://api.weather.gov/zones/forecast/ALZ053",
                    "https://api.weather.gov/zones/forecast/ALZ055",
                    "https://api.weather.gov/zones/forecast/ALZ056",
                    "https://api.weather.gov/zones/forecast/ALZ059",
                    "https://api.weather.gov/zones/forecast/ALZ261",
                    "https://api.weather.gov/zones/forecast/ALZ262",
                    "https://api.weather.gov/zones/forecast/ALZ263",
                    "https://api.weather.gov/zones/forecast/ALZ264",
                    "https://api.weather.gov/zones/forecast/ALZ265",
                    "https://api.weather.gov/zones/forecast/ALZ266",
                    "https://api.weather.gov/zones/forecast/FLZ201",
                    "https://api.weather.gov/zones/forecast/MSZ067",
                    "https://api.weather.gov/zones/forecast/MSZ075",
                    "https://api.weather.gov/zones/forecast/MSZ076",
                    "https://api.weather.gov/zones/forecast/MSZ078",
                    "https://api.weather.gov/zones/forecast/MSZ079"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.002.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.002.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-12T04:11:00-06:00"
                    },
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-12T04:11:00-06:00"
                    }
                ],
                "sent": "2022-02-12T06:34:00-06:00",
                "effective": "2022-02-12T06:34:00-06:00",
                "onset": "2022-02-12T06:34:00-06:00",
                "expires": "2022-02-12T10:00:00-06:00",
                "ends": "2022-02-12T10:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Dense Fog Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Dense Fog Advisory issued February 12 at 6:34AM CST until February 12 at 10:00AM CST by NWS Mobile AL",
                "description": "* WHAT...Visibility reduced to one quarter mile or less in dense\nfog.\n\n* WHERE...Portions of southwest Alabama and southeast\nMississippi.\n\n* WHEN...Until 10 AM CST this morning.\n\n* IMPACTS...Hazardous driving conditions due to low visibility.",
                "instruction": "If driving, slow down, use your headlights, and leave plenty of\ndistance ahead of you.",
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWMOB"
                    ],
                    "WMOidentifier": [
                        "WWUS74 KMOB 121234"
                    ],
                    "NWSheadline": [
                        "DENSE FOG ADVISORY NOW IN EFFECT UNTIL 10 AM CST THIS MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FG.Y.0002.000000T0000Z-220212T1600Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-12T16:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7d6f20b35760d1281e08132d65ed98b559436c98.001.1,2022-02-11T18:17:00-06:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ffb517bdd4bdddd8ec182df175d5264a31b0139f.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ffb517bdd4bdddd8ec182df175d5264a31b0139f.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.ffb517bdd4bdddd8ec182df175d5264a31b0139f.001.1",
                "areaDesc": "Southern Brevard County; Osceola",
                "geocode": {
                    "SAME": [
                        "012009",
                        "012097"
                    ],
                    "UGC": [
                        "FLZ047",
                        "FLZ053"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ047",
                    "https://api.weather.gov/zones/forecast/FLZ053"
                ],
                "references": [],
                "sent": "2022-02-12T06:43:00-05:00",
                "effective": "2022-02-12T06:43:00-05:00",
                "onset": "2022-02-12T06:43:00-05:00",
                "expires": "2022-02-12T08:00:00-05:00",
                "ends": null,
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Observed",
                "urgency": "Expected",
                "event": "Special Weather Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Melbourne FL",
                "headline": "Special Weather Statement issued February 12 at 6:43AM EST by NWS Melbourne FL",
                "description": "Surface observations, traffic cameras and satellite imagery\nindicate that patchy dense fog, producing visibilities of a\nquarter of a mile or less, has spread northward across portions\nof Osceola and southern Brevard counties early this morning,\nespecially near to west of Interstate 95 and across Florida's\nTurnpike. This fog is expected to persist across this area\nthrough sunrise, then dissipate shortly after 8 AM.\n\nMotorists on area roadways in these counties should prepare to\nencounter reduced visibilities due to the fog. When driving in\nfog, reduce your speed, use only your low beam headlights, and\nleave extra distance between your vehicle and the one in front of\nyou.",
                "instruction": null,
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "SPSMLB"
                    ],
                    "WMOidentifier": [
                        "WWUS82 KMLB 121143"
                    ],
                    "NWSheadline": [
                        "Locally Dense Fog Reducing Visibilities Across Osceola and Southern Brevard Counties"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.3cc4f6cec993980c2e985663f94a74b016302f05.001.1",
                "areaDesc": "Monroe; Conecuh; Escambia; Escambia Inland",
                "geocode": {
                    "SAME": [
                        "001099",
                        "001035",
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALZ055",
                        "ALZ056",
                        "ALZ059",
                        "FLZ201"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/ALZ055",
                    "https://api.weather.gov/zones/forecast/ALZ056",
                    "https://api.weather.gov/zones/forecast/ALZ059",
                    "https://api.weather.gov/zones/forecast/FLZ201"
                ],
                "references": [],
                "sent": "2022-02-12T04:11:00-06:00",
                "effective": "2022-02-12T04:11:00-06:00",
                "onset": "2022-02-12T04:11:00-06:00",
                "expires": "2022-02-12T09:00:00-06:00",
                "ends": "2022-02-12T09:00:00-06:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Dense Fog Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Dense Fog Advisory issued February 12 at 4:11AM CST until February 12 at 9:00AM CST by NWS Mobile AL",
                "description": "* WHAT...Visibility reduced to one quarter mile or less in dense\nfog.\n\n* WHERE...In Alabama, Monroe, Conecuh and Escambia Counties. In\nFlorida, Escambia Inland County.\n\n* WHEN...Until 9 AM CST Saturday.\n\n* IMPACTS...Hazardous driving conditions due to low visibility.",
                "instruction": "If driving, slow down, use your headlights, and leave plenty of\ndistance ahead of you.",
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWMOB"
                    ],
                    "WMOidentifier": [
                        "WWUS74 KMOB 121011"
                    ],
                    "NWSheadline": [
                        "DENSE FOG ADVISORY IN EFFECT UNTIL 9 AM CST THIS MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.EXA.KMOB.FG.Y.0002.000000T0000Z-220212T1500Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-12T15:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.c6a70438a786e9100f738e2c79ea8760e82a2ea1.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.c6a70438a786e9100f738e2c79ea8760e82a2ea1.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.c6a70438a786e9100f738e2c79ea8760e82a2ea1.001.1",
                "areaDesc": "Indian River; Okeechobee; St. Lucie; Martin",
                "geocode": {
                    "SAME": [
                        "012061",
                        "012093",
                        "012111",
                        "012085"
                    ],
                    "UGC": [
                        "FLZ054",
                        "FLZ058",
                        "FLZ059",
                        "FLZ064"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ054",
                    "https://api.weather.gov/zones/forecast/FLZ058",
                    "https://api.weather.gov/zones/forecast/FLZ059",
                    "https://api.weather.gov/zones/forecast/FLZ064"
                ],
                "references": [],
                "sent": "2022-02-12T05:10:00-05:00",
                "effective": "2022-02-12T05:10:00-05:00",
                "onset": "2022-02-12T05:10:00-05:00",
                "expires": "2022-02-12T08:00:00-05:00",
                "ends": null,
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Observed",
                "urgency": "Expected",
                "event": "Special Weather Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Melbourne FL",
                "headline": "Special Weather Statement issued February 12 at 5:10AM EST by NWS Melbourne FL",
                "description": "Surface observations and satellite imagery indicate that patchy\ndense fog, producing visibilities of a quarter of a mile or less,\nhas begun to form across portions of Okeechobee, Indian River,\nSaint Lucie and Martin counties early this morning. This fog is\nexpected to persist across this area through sunrise, then\ndissipate shortly after 8 AM.\n\nMotorists on area roadways in these counties should prepare to\nencounter reduced visibilities due to the fog. When driving in\nfog, reduce your speed, use only your low beam headlights, and\nleave extra distance between your vehicle and the one in front of\nyou.",
                "instruction": null,
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "SPSMLB"
                    ],
                    "WMOidentifier": [
                        "WWUS82 KMLB 121010"
                    ],
                    "NWSheadline": [
                        "Locally Dense Fog Reducing Visibilities across Okeechobee County and the Treasure Coast"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fd48c1e9a8c3ecc6555f656c9c94a8206f4b2796.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.fd48c1e9a8c3ecc6555f656c9c94a8206f4b2796.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.fd48c1e9a8c3ecc6555f656c9c94a8206f4b2796.001.1",
                "areaDesc": "Glades; Hendry",
                "geocode": {
                    "SAME": [
                        "012043",
                        "012051"
                    ],
                    "UGC": [
                        "FLZ063",
                        "FLZ066"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ063",
                    "https://api.weather.gov/zones/forecast/FLZ066"
                ],
                "references": [],
                "sent": "2022-02-12T04:13:00-05:00",
                "effective": "2022-02-12T04:13:00-05:00",
                "onset": "2022-02-12T04:13:00-05:00",
                "expires": "2022-02-12T08:00:00-05:00",
                "ends": "2022-02-12T08:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Dense Fog Advisory",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Dense Fog Advisory issued February 12 at 4:13AM EST until February 12 at 8:00AM EST by NWS Miami FL",
                "description": "* WHAT...Visibility a quarter mile or less.\n\n* WHERE...Glades and Hendry Counties.\n\n* WHEN...Until 8 AM EST this morning.\n\n* IMPACTS...Hazardous driving conditions due to low visibility.",
                "instruction": "If driving, slow down, use your headlights, and leave plenty of\ndistance ahead of you.",
                "response": "Execute",
                "parameters": {
                    "AWIPSidentifier": [
                        "NPWMFL"
                    ],
                    "WMOidentifier": [
                        "WWUS72 KMFL 120913"
                    ],
                    "NWSheadline": [
                        "DENSE FOG ADVISORY IN EFFECT UNTIL 8 AM EST THIS MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.NEW.KMFL.FG.Y.0006.220212T0913Z-220212T1300Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-12T13:00:00+00:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.e696d62d35a64d6b0959e32e5f5ca201827bf4fa.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-10T20:04:00-06:00"
                    },
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-11T09:46:00-06:00"
                    }
                ],
                "sent": "2022-02-11T19:17:00-06:00",
                "effective": "2022-02-11T19:17:00-06:00",
                "onset": "2022-02-11T19:17:00-06:00",
                "expires": "2022-02-12T19:30:00-06:00",
                "ends": "2022-02-13T09:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 11 at 7:17PM CST until February 13 at 9:00AM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring and minor flooding is forecast.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until late Sunday morning.\n\n* IMPACTS...At 19.0 feet, Low lying pastures will flood. Cattle\nshould be moved to higher ground.\n\n* ADDITIONAL DETAILS...\n- At 7:05 PM CST Friday the stage was 18.1 feet.\n- Recent Activity...The maximum river stage in the 24 hours\nending at 7:05 PM CST Friday was 18.1 feet.\n- Forecast...The river is expected to fall below flood stage\nearly Sunday morning and continue falling to 13.7 feet\nWednesday evening.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 120117"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL LATE SUNDAY MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220213T1500Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-13T15:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1,2022-02-10T10:01:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1,2022-02-09T19:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1,2022-02-09T10:26:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.d2d9a245f8e6db902d0cc55ee982d3e3f339fc02.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-10T20:04:00-06:00"
                    },
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-10T10:01:00-06:00"
                    }
                ],
                "sent": "2022-02-11T09:46:00-06:00",
                "effective": "2022-02-11T09:46:00-06:00",
                "onset": "2022-02-11T09:46:00-06:00",
                "expires": "2022-02-12T10:00:00-06:00",
                "ends": "2022-02-13T12:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 11 at 9:46AM CST until February 13 at 12:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until early Sunday afternoon.\n\n* IMPACTS...At 19 feet, Low lying pastures will flood. Cattle should\nbe moved to higher ground.\n\n* ADDITIONAL DETAILS...\n- At 9:05 AM CST Friday the stage was 18 feet.\n- Forecast...The river is expected to fall below flood stage\nSunday morning and continue falling to 14.2 feet Wednesday\nmorning.\n- Flood stage is 17 feet.\n- www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 111546"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL EARLY SUNDAY AFTERNOON"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220213T1800Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-13T18:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1,2022-02-09T19:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1,2022-02-09T10:26:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.ba2203329b77a6f1f062e86a0c1df2e8a75242fd.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-10T10:01:00-06:00"
                    }
                ],
                "sent": "2022-02-10T20:04:00-06:00",
                "effective": "2022-02-10T20:04:00-06:00",
                "onset": "2022-02-10T20:04:00-06:00",
                "expires": "2022-02-11T20:15:00-06:00",
                "ends": "2022-02-13T09:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 10 at 8:04PM CST until February 13 at 9:00AM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until late Sunday morning.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands.\n\n* ADDITIONAL DETAILS...\n- At 7:05 PM CST Thursday the stage was 17.9 feet.\n- Forecast...The river is expected to rise to a crest of 18.1\nfeet early Friday afternoon. It will then begin to fall,\nfalling below flood stage early Sunday morning.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 110204"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL LATE SUNDAY MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220213T1500Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-13T15:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1,2022-02-09T19:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1,2022-02-09T10:26:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.cf62d95748924e57e8f2a408ad8927244b260579.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-09T19:33:00-06:00"
                    },
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-09T10:26:00-06:00"
                    }
                ],
                "sent": "2022-02-10T10:01:00-06:00",
                "effective": "2022-02-10T10:01:00-06:00",
                "onset": "2022-02-10T10:01:00-06:00",
                "expires": "2022-02-11T10:15:00-06:00",
                "ends": "2022-02-13T18:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 10 at 10:01AM CST until February 13 at 6:00PM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until Sunday afternoon.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands. At 19.0\nfeet, Low lying pastures will flood. Cattle should be moved to\nhigher ground.\n\n* ADDITIONAL DETAILS...\n- At 9:05 AM CST Thursday the stage was 17.7 feet.\n- Forecast...The river is expected to continue to rise,\ncresting at a stage of 18.3 feet early Friday afternoon. It\nwill then begin to fall, falling below flood stage Sunday\nafternoon.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 101601"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL SUNDAY EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220214T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-14T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1,2022-02-08T20:08:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.7838c949cd670005d04190706364369ebb904e24.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.7838c949cd670005d04190706364369ebb904e24.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.7838c949cd670005d04190706364369ebb904e24.001.1",
                "areaDesc": "Coastal Palm Beach County",
                "geocode": {
                    "SAME": [
                        "012099"
                    ],
                    "UGC": [
                        "FLZ168"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e8798139701c7edcda83375d9349fc1b385513ce.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.e8798139701c7edcda83375d9349fc1b385513ce.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-10T02:26:00-05:00"
                    }
                ],
                "sent": "2022-02-10T09:51:00-05:00",
                "effective": "2022-02-10T09:51:00-05:00",
                "onset": "2022-02-10T09:51:00-05:00",
                "expires": "2022-02-10T10:06:24-05:00",
                "ends": "2022-02-10T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Cancel",
                "category": "Met",
                "severity": "Minor",
                "certainty": "Observed",
                "urgency": "Past",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "The Rip Current Statement has been cancelled.",
                "description": "The Rip Current Statement has been cancelled and is no longer in effect.",
                "instruction": null,
                "response": "AllClear",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 101451"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK IS CANCELLED"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CAN.KMFL.RP.S.0006.000000T0000Z-220211T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-11T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.db880bcef832bdc12f9a4328d993a13e779c1f45.001.1,2022-02-09T06:37:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7755500c2e6df90ac8bf4a76220fe3dc1cccd1cf.001.1,2022-02-08T09:22:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3030b380ce2d454165be6a4623f024f999eeb6c3.001.1,2022-02-08T03:40:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e22bfae95d33daa32d773b7be0b21e35b7b7639a.001.1,2022-02-07T23:00:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ad36d3153543e17386bd305f09acd8042bcaa891.001.1,2022-02-07T15:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1077f7c8ca3a666358cde22920714bb0a0e13d97.001.1,2022-02-07T02:11:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4309adcdcb8cf4f9070e5ab1bfa3ec85c2ab4431.001.1,2022-02-06T15:23:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.380bf028a388bedbd6b999e65681f38a2274b950.001.1,2022-02-06T03:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c1534722e0d5db97b24177db0d690b2428c62910.001.1,2022-02-05T15:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.87e2effa22fe967b7e7b58cb8c8b97f96d09386a.001.1,2022-02-05T09:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.809b0a09b57b58d769adfe77d2812e4863c12a38.001.1,2022-02-05T02:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.dcbf5ba79e490267f2d47097801060562ffe91d3.001.1,2022-02-04T15:18:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2aa0c80d93b2e6cde98d0c1bbb8ab6f871b7d580.002.1,2022-02-04T02:25:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3493420d5d04acfcd35af2211f016362614aecd2.002.1,2022-02-03T16:07:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.96eae0e133d1fc424ee3c4945879bef2668d10f6.002.1,2022-02-03T08:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2b715dde44e4548126d5cd7ab487d7646c8c9154.001.1,2022-02-03T03:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b65f6986b12f81eb6382d6a4ae2d3d38676fdd2.002.1,2022-02-02T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.feee54ffe4386b18c45017ae6e7a1d52ce4efe8b.002.1,2022-02-02T02:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.8695dbf1c9167732af0f1ef7a18a111f7a568a3d.002.1,2022-02-01T01:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.dbe284fa89a64539c70be0fdf1e97bf6be6192d6.002.1,2022-01-31T15:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f02042cf8a7e86ebe3a762b7a0fdfbfe51730a2.002.1,2022-01-30T23:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5cfd079b7261a92081403ecf0b389902e758e10b.002.1,2022-01-30T15:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.29d6f19f032198a9b594a119dba6ce8151af9171.001.1,2022-01-30T02:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.cdb60cfab10bde768e834d6f41305ad90e358c8d.002.1,2022-01-29T19:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.29c1d017f8c4810be053bf1746ffe3a4278784e7.002.1,2022-01-29T14:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.af89508c26357f99333b1929e494f46b5cfebc27.002.1,2022-01-29T02:47:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c4f96b1ce85c9d7a3b66e9664251bc84e52995b0.001.1,2022-01-28T14:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4c5b0a5e33a12bc2bba1c0329ca9506b2093823d.001.1,2021-02-11T03:36:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e8798139701c7edcda83375d9349fc1b385513ce.001.1",
            "type": "Feature",
            "geometry": null,
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.e8798139701c7edcda83375d9349fc1b385513ce.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.e8798139701c7edcda83375d9349fc1b385513ce.001.1",
                "areaDesc": "Coastal Palm Beach County",
                "geocode": {
                    "SAME": [
                        "012099"
                    ],
                    "UGC": [
                        "FLZ168"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/forecast/FLZ168"
                ],
                "references": [],
                "sent": "2022-02-10T02:26:00-05:00",
                "effective": "2022-02-10T02:26:00-05:00",
                "onset": "2022-02-10T02:26:00-05:00",
                "expires": "2022-02-10T19:00:00-05:00",
                "ends": "2022-02-10T19:00:00-05:00",
                "status": "Actual",
                "messageType": "Alert",
                "category": "Met",
                "severity": "Moderate",
                "certainty": "Likely",
                "urgency": "Expected",
                "event": "Rip Current Statement",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Miami FL",
                "headline": "Rip Current Statement issued February 10 at 2:26AM EST until February 10 at 7:00PM EST by NWS Miami FL",
                "description": "* WHAT...Dangerous rip currents.\n\n* WHERE...Coastal Palm Beach County.\n\n* WHEN...Through this evening.\n\n* IMPACTS...Rip currents can sweep even the best swimmers away\nfrom shore into deeper water.",
                "instruction": "Swim near a lifeguard. If caught in a rip current, relax and\nfloat. Don't swim against the current. If able, swim in a\ndirection following the shoreline. If unable to escape, face the\nshore and call or wave for help.",
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "CFWMFL"
                    ],
                    "WMOidentifier": [
                        "WHUS42 KMFL 100726"
                    ],
                    "NWSheadline": [
                        "HIGH RIP CURRENT RISK REMAINS IN EFFECT THROUGH THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "VTEC": [
                        "/O.CON.KMFL.RP.S.0006.000000T0000Z-220211T0000Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-11T00:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.db880bcef832bdc12f9a4328d993a13e779c1f45.001.1,2022-02-09T06:37:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7755500c2e6df90ac8bf4a76220fe3dc1cccd1cf.001.1,2022-02-08T09:22:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3030b380ce2d454165be6a4623f024f999eeb6c3.001.1,2022-02-08T03:40:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e22bfae95d33daa32d773b7be0b21e35b7b7639a.001.1,2022-02-07T23:00:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ad36d3153543e17386bd305f09acd8042bcaa891.001.1,2022-02-07T15:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1077f7c8ca3a666358cde22920714bb0a0e13d97.001.1,2022-02-07T02:11:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4309adcdcb8cf4f9070e5ab1bfa3ec85c2ab4431.001.1,2022-02-06T15:23:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.380bf028a388bedbd6b999e65681f38a2274b950.001.1,2022-02-06T03:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c1534722e0d5db97b24177db0d690b2428c62910.001.1,2022-02-05T15:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.87e2effa22fe967b7e7b58cb8c8b97f96d09386a.001.1,2022-02-05T09:19:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.809b0a09b57b58d769adfe77d2812e4863c12a38.001.1,2022-02-05T02:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.dcbf5ba79e490267f2d47097801060562ffe91d3.001.1,2022-02-04T15:18:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2aa0c80d93b2e6cde98d0c1bbb8ab6f871b7d580.002.1,2022-02-04T02:25:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3493420d5d04acfcd35af2211f016362614aecd2.002.1,2022-02-03T16:07:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.96eae0e133d1fc424ee3c4945879bef2668d10f6.002.1,2022-02-03T08:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2b715dde44e4548126d5cd7ab487d7646c8c9154.001.1,2022-02-03T03:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b65f6986b12f81eb6382d6a4ae2d3d38676fdd2.002.1,2022-02-02T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.feee54ffe4386b18c45017ae6e7a1d52ce4efe8b.002.1,2022-02-02T02:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.8695dbf1c9167732af0f1ef7a18a111f7a568a3d.002.1,2022-02-01T01:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.dbe284fa89a64539c70be0fdf1e97bf6be6192d6.002.1,2022-01-31T15:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f02042cf8a7e86ebe3a762b7a0fdfbfe51730a2.002.1,2022-01-30T23:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5cfd079b7261a92081403ecf0b389902e758e10b.002.1,2022-01-30T15:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.29d6f19f032198a9b594a119dba6ce8151af9171.001.1,2022-01-30T02:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.cdb60cfab10bde768e834d6f41305ad90e358c8d.002.1,2022-01-29T19:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.29c1d017f8c4810be053bf1746ffe3a4278784e7.002.1,2022-01-29T14:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.af89508c26357f99333b1929e494f46b5cfebc27.002.1,2022-01-29T02:47:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c4f96b1ce85c9d7a3b66e9664251bc84e52995b0.001.1,2022-01-28T14:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4c5b0a5e33a12bc2bba1c0329ca9506b2093823d.001.1,2021-02-11T03:36:00-05:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.a71381aa1668df6d9bf17c05a1d9f7d8163e182e.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -85.019999999999996,
                            30.539999999999999
                        ],
                        [
                            -84.929999999999993,
                            30.529999999999998
                        ],
                        [
                            -85.079999999999998,
                            30.089999999999996
                        ],
                        [
                            -84.909999999999997,
                            29.799999999999997
                        ],
                        [
                            -85.049999999999997,
                            29.719999999999999
                        ],
                        [
                            -85.200000000000003,
                            30.07
                        ],
                        [
                            -85.019999999999996,
                            30.539999999999999
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.a71381aa1668df6d9bf17c05a1d9f7d8163e182e.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.a71381aa1668df6d9bf17c05a1d9f7d8163e182e.001.1",
                "areaDesc": "Calhoun, FL; Franklin, FL; Gulf, FL; Liberty, FL",
                "geocode": {
                    "SAME": [
                        "012013",
                        "012037",
                        "012045",
                        "012077"
                    ],
                    "UGC": [
                        "FLC013",
                        "FLC037",
                        "FLC045",
                        "FLC077"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/FLC013",
                    "https://api.weather.gov/zones/county/FLC037",
                    "https://api.weather.gov/zones/county/FLC045",
                    "https://api.weather.gov/zones/county/FLC077"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.1567829aec85990debff8ca23cafcfcd7e1898b4.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.1567829aec85990debff8ca23cafcfcd7e1898b4.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-09T08:39:00-06:00"
                    }
                ],
                "sent": "2022-02-09T22:10:00-06:00",
                "effective": "2022-02-09T22:10:00-06:00",
                "onset": "2022-02-09T22:10:00-06:00",
                "expires": "2022-02-09T22:26:05-06:00",
                "ends": "2022-02-09T22:10:00-06:00",
                "status": "Actual",
                "messageType": "Cancel",
                "category": "Met",
                "severity": "Minor",
                "certainty": "Observed",
                "urgency": "Past",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Tallahassee FL",
                "headline": "The Flood Warning has been cancelled.",
                "description": "The Flood Warning has been cancelled and is no longer in effect.",
                "instruction": null,
                "response": "AllClear",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSTAE"
                    ],
                    "WMOidentifier": [
                        "WGUS82 KTAE 100410"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING IS CANCELLED THIS EVENING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.CAN.KTAE.FL.W.0001.000000T0000Z-220210T0410Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-10T04:10:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7cf03952499d1f077084dec919360d7e0dd0d590.001.1,2022-02-08T19:07:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.049cdf99d3f9e14f92fd5cf18cdc96f767a1bd6c.001.1,2022-02-08T07:30:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7e610a147fe163d9945c5704bce10ec5fdf72773.001.1,2022-02-07T19:52:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ea5995f4bbf75bde4c33629a737fd0ee4a3d099c.001.1,2022-02-07T07:38:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ec5e39e80f66b912e560176d2a53d907dbab42ab.001.1,2022-02-06T19:49:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e47552762cf73e06fe12ec33b0f233e1dac3023b.001.1,2022-02-06T08:21:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d0bd163d4d0e670f2ba2f9f4840e0336a1b5b03.001.1,2022-02-05T17:36:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.93a3b21f7c3cc92434596ed8aa973cbbb78ea244.001.1,2022-02-05T08:37:00-06:00"
                    ]
                }
            }
        },
        {
            "id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1",
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -87.180000000000007,
                            31
                        ],
                        [
                            -87.150000000000006,
                            31
                        ],
                        [
                            -87.280000000000001,
                            30.899999999999999
                        ],
                        [
                            -87.290000000000006,
                            30.77
                        ],
                        [
                            -87.330000000000013,
                            30.77
                        ],
                        [
                            -87.310000000000016,
                            30.93
                        ],
                        [
                            -87.180000000000007,
                            31
                        ]
                    ]
                ]
            },
            "properties": {
                "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1",
                "@type": "wx:Alert",
                "id": "urn:oid:2.49.0.1.840.0.ce3408ce49b81492b6b75347f5f2ef61cd85f914.001.1",
                "areaDesc": "Escambia, AL; Escambia, FL",
                "geocode": {
                    "SAME": [
                        "001053",
                        "012033"
                    ],
                    "UGC": [
                        "ALC053",
                        "FLC033"
                    ]
                },
                "affectedZones": [
                    "https://api.weather.gov/zones/county/ALC053",
                    "https://api.weather.gov/zones/county/FLC033"
                ],
                "references": [
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.2f74788d5dfec8907963091df7291ffc3aac0355.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-08T20:08:00-06:00"
                    },
                    {
                        "@id": "https://api.weather.gov/alerts/urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1",
                        "identifier": "urn:oid:2.49.0.1.840.0.93d414684144e9de8959a923084c84b630f85b3e.001.1",
                        "sender": "w-nws.webmaster@noaa.gov",
                        "sent": "2022-02-09T10:26:00-06:00"
                    }
                ],
                "sent": "2022-02-09T19:33:00-06:00",
                "effective": "2022-02-09T19:33:00-06:00",
                "onset": "2022-02-09T19:33:00-06:00",
                "expires": "2022-02-10T19:45:00-06:00",
                "ends": "2022-02-13T09:00:00-06:00",
                "status": "Actual",
                "messageType": "Update",
                "category": "Met",
                "severity": "Severe",
                "certainty": "Observed",
                "urgency": "Immediate",
                "event": "Flood Warning",
                "sender": "w-nws.webmaster@noaa.gov",
                "senderName": "NWS Mobile AL",
                "headline": "Flood Warning issued February 9 at 7:33PM CST until February 13 at 9:00AM CST by NWS Mobile AL",
                "description": "* WHAT...Minor flooding is occurring.\n\n* WHERE...Escambia River Near Century.\n\n* WHEN...Until late Sunday morning.\n\n* IMPACTS...At 17.0 feet, Considerable flooding of lowlands. At 19.0\nfeet, Low lying pastures will flood. Cattle should be moved to\nhigher ground.\n\n* ADDITIONAL DETAILS...\n- At 7:05 PM CST Wednesday the stage was 17.5 feet.\n- Forecast...The river is expected to continue to rise,\ncresting at a stage of 18.2 feet early Friday morning. It\nwill then begin to fall, falling below flood stage early\nSunday morning.\n- Flood stage is 17.0 feet.\n- http://www.weather.gov/safety/flood",
                "instruction": null,
                "response": "Avoid",
                "parameters": {
                    "AWIPSidentifier": [
                        "FLSMOB"
                    ],
                    "WMOidentifier": [
                        "WGUS84 KMOB 100133"
                    ],
                    "NWSheadline": [
                        "FLOOD WARNING NOW IN EFFECT UNTIL LATE SUNDAY MORNING"
                    ],
                    "BLOCKCHANNEL": [
                        "EAS",
                        "NWEM",
                        "CMAS"
                    ],
                    "EAS-ORG": [
                        "WXR"
                    ],
                    "VTEC": [
                        "/O.EXT.KMOB.FL.W.0012.000000T0000Z-220213T1500Z/"
                    ],
                    "eventEndingTime": [
                        "2022-02-13T15:00:00+00:00"
                    ],
                    "expiredReferences": [
                        "w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f385ce1f88d275c393a65fbb908d0be74934538a.001.1,2022-02-08T13:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.f302ba47a84b36fc2313b2a1378ebd3c312fea40.001.1,2022-02-08T10:12:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.27942697b1bbaf5b904fd1a4ffef0bec0d077f50.001.1,2022-02-07T21:39:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.144821152c9b01330e04cc26242cb4e3ea98e04b.001.1,2022-02-07T10:02:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.18f767976e81f7b85c05d1fe76c32fca1665585f.001.1,2022-02-06T23:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.5f6b1a1f5084bf43b170386ca09379d40f53e220.001.1,2022-02-06T18:33:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6b9565491df86427ac0f3f99739d730c18de9650.001.1,2022-02-06T10:56:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4653ffde125eabd6e579c0778c1eac7da2657524.001.1,2022-02-06T01:00:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.26bd683a42f66a0fcc5d40e6c10ae1fa224774c1.001.1,2022-02-05T17:35:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.9f298f0302ed3cf9428a5da9219844b99a448fa2.001.1,2022-02-05T09:41:00-06:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bea0c7f325ae3e22a87dd2187d964895ede9979d.001.1,2021-04-04T11:35:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b1516f98009ef5ca6c8bd355f1fb59427ecfef48.001.1,2021-04-04T20:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.84e5a2a8a502ee628a51c99e345f8a5e116a6dc4.001.1,2021-04-03T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4d76445c1bc4b0fd512b6a6671a8df0f7aa977af.001.1,2021-04-03T08:58:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e620ed823b0c0483a9206c395b799a6495024dc8.001.1,2021-04-02T20:05:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3d60abaf4e5844dc7502d879cecb532d931f4d52.001.1,2021-04-02T10:24:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e3c0fdc97b0b64e478b9156e6654a9783c2c34f4.001.1,2021-04-01T19:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.acc68781973925209cc4357693596f85591d7f46.001.1,2021-04-01T09:52:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.7af944090fc4c198247ca7bc5ad92eaac381be54.001.1,2021-04-01T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.432289ae2b673f7affa87fcc9af1727dac09e823.001.1,2021-03-31T09:59:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3fdc40a9c3660afcbcd744a91aa495a1b05972b0.001.1,2021-03-31T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.b514d4401ed4687748aa765bc0f00c793cae66fb.001.1,2021-03-30T09:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.880122dfb71e90f18c4f12e2d249b4d240423626.001.1,2021-03-30T20:01:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2e8b28cd9ecd6c452c8c5f2c18d0829cf615c8e4.001.1,2021-03-29T20:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4078eaca3993ce8b674f6a32ba13985055b08850.001.1,2021-03-29T10:26:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b6f2479bed082a072971fb1073c12bdac7d1bc5.001.1,2021-03-28T19:45:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c49cfb58a72af3d775becb09f4b076b3179c57b8.001.1,2021-03-28T09:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4bbf5adf67a37f7c342d3043eafcf837b5347029.001.1,2021-03-27T20:14:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.a1098ec7630d2d2ff8829759f81a591a77efee30.001.1,2021-03-27T09:41:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.bcbda4aeba1a0b9a587f55f6ba80e98bc812ee36.001.1,2021-03-26T21:33:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.3b9e88fe9c9b8f06acdbc5944b0924f38e7ef8d7.001.1,2021-03-26T11:08:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.4b103947895c66d9bf9e62f114e024c1c1123c10.001.1,2021-03-25T21:16:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.71cd1a1fa53999466761c1cb957d368d0179a0bb.001.1,2021-03-25T09:42:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6d92f0628b637cb9f13ba0c5a975ad4a5449b46b.001.1,2021-03-24T21:03:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ce7765ecacc3db755c6f763773fab832d941177d.001.1,2021-03-24T09:56:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.651b2dca1a3d893ea054e949742a5d7ee9c3285b.001.1,2021-03-24T08:04:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.fe664e6d028caf388140f7053bbd630f968a36be.001.1,2021-03-23T20:09:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.010ddfbe174b51416d36ad0df4bc846f3a1cfc55.001.1,2021-03-23T09:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.6aae669651fff3832e6a27c6cfd58cc7b8d6f7c5.001.1,2021-03-22T19:46:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.2d67bdd0d192262da3821a1157d7f3b0a9a50e8b.001.1,2021-03-22T10:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.1ec65b301cd4d2eb908e626f84c89179f5315917.001.1,2021-03-21T19:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.e9a85b70ac7c0afcb51a86b7398675fa99b4ba38.001.1,2021-03-21T10:49:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.91186827115ce0e4dcbd2049ca319f54f0628c00.001.1,2021-03-20T20:36:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.76cf118a1af16f3357c3a2f7d6c2b53c5617532b.001.1,2021-03-20T10:31:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.88b0cb0d6c2500ba0288ac5936a33be02b11fce0.001.1,2021-03-19T19:48:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.ab2e01e515a3914e1c27a724aea4bd08ddfeb5f9.001.1,2021-03-19T10:34:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c8185edf305bfbeb33f941369d6e1aa5200813b3.001.1,2021-03-19T07:53:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.aa340ecbf22ffab0a164736df9b71bc3c1f19a46.001.1,2021-03-18T19:55:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.35f64098249a375adf92999cf7b912d81b452dda.001.1,2021-03-18T10:43:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.c9db886e1cda70f767aac459389f24b8aa74fbb3.001.1,2021-03-17T20:29:00-05:00 w-nws.webmaster@noaa.gov,urn:oid:2.49.0.1.840.0.78253c9b43850338fea025bf0aa16760ddad6c8a.001.1,2021-03-17T10:32:00-05:00"
                    ]
                }
            }
        }
    ],
    "title": "watches, warnings, and advisories for Florida",
    "updated": "2022-02-16T19:24:58+00:00",
    "pagination": {
        "next": "https://api.weather.gov/alerts?area%5B0%5D=FL&cursor=eyJ0IjoxNjQ0NDU2NzgwLCJpIjoidXJuOm9pZDoyLjQ5LjAuMS44NDAuMC5jZTM0MDhjZTQ5YjgxNDkyYjZiNzUzNDdmNWYyZWY2MWNkODVmOTE0LjAwMS4xIn0%3D"
    }
}