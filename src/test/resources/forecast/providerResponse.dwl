{
    "@context": [
        "https://geojson.org/geojson-ld/geojson-context.jsonld",
        {
            "@version": "1.1",
            "wx": "https://api.weather.gov/ontology#",
            "geo": "http://www.opengis.net/ont/geosparql#",
            "unit": "http://codes.wmo.int/common/unit/",
            "@vocab": "https://api.weather.gov/ontology#"
        }
    ],
    "type": "Feature",
    "geometry": {
        "type": "Polygon",
        "coordinates": [
            [
                [
                    -82.806618999999998,
                    27.977357699999999
                ],
                [
                    -82.808937299999997,
                    27.9546502
                ],
                [
                    -82.783222100000003,
                    27.952599899999999
                ],
                [
                    -82.780898899999997,
                    27.975307099999998
                ],
                [
                    -82.806618999999998,
                    27.977357699999999
                ]
            ]
        ]
    },
    "properties": {
        "updated": "2022-02-16T19:22:45+00:00",
        "units": "us",
        "forecastGenerator": "BaselineForecastGenerator",
        "generatedAt": "2022-02-16T20:53:45+00:00",
        "updateTime": "2022-02-16T19:22:45+00:00",
        "validTimes": "2022-02-16T13:00:00+00:00/P7DT12H",
        "elevation": {
            "unitCode": "wmoUnit:m",
            "value": 7.0103999999999997
        },
        "periods": [
            {
                "number": 1,
                "name": "This Afternoon",
                "startTime": "2022-02-16T15:00:00-05:00",
                "endTime": "2022-02-16T18:00:00-05:00",
                "isDaytime": true,
                "temperature": 78,
                "temperatureUnit": "F",
                "temperatureTrend": "falling",
                "windSpeed": "7 to 10 mph",
                "windDirection": "ESE",
                "icon": "https://api.weather.gov/icons/land/day/few?size=medium",
                "shortForecast": "Sunny",
                "detailedForecast": "Sunny. High near 78, with temperatures falling to around 74 in the afternoon. East southeast wind 7 to 10 mph."
            },
            {
                "number": 2,
                "name": "Tonight",
                "startTime": "2022-02-16T18:00:00-05:00",
                "endTime": "2022-02-17T06:00:00-05:00",
                "isDaytime": false,
                "temperature": 63,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "6 to 9 mph",
                "windDirection": "E",
                "icon": "https://api.weather.gov/icons/land/night/few?size=medium",
                "shortForecast": "Mostly Clear",
                "detailedForecast": "Mostly clear, with a low around 63. East wind 6 to 9 mph."
            },
            {
                "number": 3,
                "name": "Thursday",
                "startTime": "2022-02-17T06:00:00-05:00",
                "endTime": "2022-02-17T18:00:00-05:00",
                "isDaytime": true,
                "temperature": 81,
                "temperatureUnit": "F",
                "temperatureTrend": "falling",
                "windSpeed": "7 to 10 mph",
                "windDirection": "SSE",
                "icon": "https://api.weather.gov/icons/land/day/few?size=medium",
                "shortForecast": "Sunny",
                "detailedForecast": "Sunny. High near 81, with temperatures falling to around 76 in the afternoon. South southeast wind 7 to 10 mph."
            },
            {
                "number": 4,
                "name": "Thursday Night",
                "startTime": "2022-02-17T18:00:00-05:00",
                "endTime": "2022-02-18T06:00:00-05:00",
                "isDaytime": false,
                "temperature": 63,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "2 to 8 mph",
                "windDirection": "S",
                "icon": "https://api.weather.gov/icons/land/night/sct?size=medium",
                "shortForecast": "Partly Cloudy",
                "detailedForecast": "Partly cloudy, with a low around 63. South wind 2 to 8 mph."
            },
            {
                "number": 5,
                "name": "Friday",
                "startTime": "2022-02-18T06:00:00-05:00",
                "endTime": "2022-02-18T18:00:00-05:00",
                "isDaytime": true,
                "temperature": 73,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "3 to 8 mph",
                "windDirection": "SSW",
                "icon": "https://api.weather.gov/icons/land/day/rain_showers,20?size=medium",
                "shortForecast": "Slight Chance Rain Showers",
                "detailedForecast": "A slight chance of rain showers after 7am. Partly sunny, with a high near 73. South southwest wind 3 to 8 mph. Chance of precipitation is 20%."
            },
            {
                "number": 6,
                "name": "Friday Night",
                "startTime": "2022-02-18T18:00:00-05:00",
                "endTime": "2022-02-19T06:00:00-05:00",
                "isDaytime": false,
                "temperature": 58,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "2 to 7 mph",
                "windDirection": "WNW",
                "icon": "https://api.weather.gov/icons/land/night/rain_showers,20?size=medium",
                "shortForecast": "Slight Chance Rain Showers",
                "detailedForecast": "A slight chance of rain showers. Mostly cloudy, with a low around 58. West northwest wind 2 to 7 mph. Chance of precipitation is 20%."
            },
            {
                "number": 7,
                "name": "Saturday",
                "startTime": "2022-02-19T06:00:00-05:00",
                "endTime": "2022-02-19T18:00:00-05:00",
                "isDaytime": true,
                "temperature": 66,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "7 to 12 mph",
                "windDirection": "N",
                "icon": "https://api.weather.gov/icons/land/day/rain_showers,20/sct?size=medium",
                "shortForecast": "Slight Chance Rain Showers then Mostly Sunny",
                "detailedForecast": "A slight chance of rain showers before 7am. Mostly sunny, with a high near 66. North wind 7 to 12 mph. Chance of precipitation is 20%."
            },
            {
                "number": 8,
                "name": "Saturday Night",
                "startTime": "2022-02-19T18:00:00-05:00",
                "endTime": "2022-02-20T06:00:00-05:00",
                "isDaytime": false,
                "temperature": 53,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "8 to 12 mph",
                "windDirection": "NNE",
                "icon": "https://api.weather.gov/icons/land/night/few?size=medium",
                "shortForecast": "Mostly Clear",
                "detailedForecast": "Mostly clear, with a low around 53. North northeast wind 8 to 12 mph."
            },
            {
                "number": 9,
                "name": "Sunday",
                "startTime": "2022-02-20T06:00:00-05:00",
                "endTime": "2022-02-20T18:00:00-05:00",
                "isDaytime": true,
                "temperature": 72,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "6 to 10 mph",
                "windDirection": "NE",
                "icon": "https://api.weather.gov/icons/land/day/few?size=medium",
                "shortForecast": "Sunny",
                "detailedForecast": "Sunny, with a high near 72. Northeast wind 6 to 10 mph."
            },
            {
                "number": 10,
                "name": "Sunday Night",
                "startTime": "2022-02-20T18:00:00-05:00",
                "endTime": "2022-02-21T06:00:00-05:00",
                "isDaytime": false,
                "temperature": 60,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "8 mph",
                "windDirection": "ENE",
                "icon": "https://api.weather.gov/icons/land/night/sct?size=medium",
                "shortForecast": "Partly Cloudy",
                "detailedForecast": "Partly cloudy, with a low around 60."
            },
            {
                "number": 11,
                "name": "Washington's Birthday",
                "startTime": "2022-02-21T06:00:00-05:00",
                "endTime": "2022-02-21T18:00:00-05:00",
                "isDaytime": true,
                "temperature": 76,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "5 to 9 mph",
                "windDirection": "SSE",
                "icon": "https://api.weather.gov/icons/land/day/few?size=medium",
                "shortForecast": "Sunny",
                "detailedForecast": "Sunny, with a high near 76."
            },
            {
                "number": 12,
                "name": "Monday Night",
                "startTime": "2022-02-21T18:00:00-05:00",
                "endTime": "2022-02-22T06:00:00-05:00",
                "isDaytime": false,
                "temperature": 62,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "5 to 9 mph",
                "windDirection": "NE",
                "icon": "https://api.weather.gov/icons/land/night/few?size=medium",
                "shortForecast": "Mostly Clear",
                "detailedForecast": "Mostly clear, with a low around 62."
            },
            {
                "number": 13,
                "name": "Tuesday",
                "startTime": "2022-02-22T06:00:00-05:00",
                "endTime": "2022-02-22T18:00:00-05:00",
                "isDaytime": true,
                "temperature": 77,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "9 mph",
                "windDirection": "SSE",
                "icon": "https://api.weather.gov/icons/land/day/few?size=medium",
                "shortForecast": "Sunny",
                "detailedForecast": "Sunny, with a high near 77."
            },
            {
                "number": 14,
                "name": "Tuesday Night",
                "startTime": "2022-02-22T18:00:00-05:00",
                "endTime": "2022-02-23T06:00:00-05:00",
                "isDaytime": false,
                "temperature": 62,
                "temperatureUnit": "F",
                "temperatureTrend": null,
                "windSpeed": "7 mph",
                "windDirection": "SSW",
                "icon": "https://api.weather.gov/icons/land/night/few?size=medium",
                "shortForecast": "Mostly Clear",
                "detailedForecast": "Mostly clear, with a low around 62."
            }
        ]
    }
}