%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "name": "This Afternoon",
    "date": "2022-02-16",
    "description": "Sunny. High near 78, with temperatures falling to around 74 in the afternoon. East southeast wind 7 to 10 mph.",
    "temperature": 78
  },
  {
    "name": "Tonight",
    "date": "2022-02-16",
    "description": "Mostly clear, with a low around 63. East wind 6 to 9 mph.",
    "temperature": 63
  },
  {
    "name": "Thursday",
    "date": "2022-02-17",
    "description": "Sunny. High near 81, with temperatures falling to around 76 in the afternoon. South southeast wind 7 to 10 mph.",
    "temperature": 81
  },
  {
    "name": "Thursday Night",
    "date": "2022-02-17",
    "description": "Partly cloudy, with a low around 63. South wind 2 to 8 mph.",
    "temperature": 63
  },
  {
    "name": "Friday",
    "date": "2022-02-18",
    "description": "A slight chance of rain showers after 7am. Partly sunny, with a high near 73. South southwest wind 3 to 8 mph. Chance of precipitation is 20%.",
    "temperature": 73
  },
  {
    "name": "Friday Night",
    "date": "2022-02-18",
    "description": "A slight chance of rain showers. Mostly cloudy, with a low around 58. West northwest wind 2 to 7 mph. Chance of precipitation is 20%.",
    "temperature": 58
  },
  {
    "name": "Saturday",
    "date": "2022-02-19",
    "description": "A slight chance of rain showers before 7am. Mostly sunny, with a high near 66. North wind 7 to 12 mph. Chance of precipitation is 20%.",
    "temperature": 66
  },
  {
    "name": "Saturday Night",
    "date": "2022-02-19",
    "description": "Mostly clear, with a low around 53. North northeast wind 8 to 12 mph.",
    "temperature": 53
  },
  {
    "name": "Sunday",
    "date": "2022-02-20",
    "description": "Sunny, with a high near 72. Northeast wind 6 to 10 mph.",
    "temperature": 72
  },
  {
    "name": "Sunday Night",
    "date": "2022-02-20",
    "description": "Partly cloudy, with a low around 60.",
    "temperature": 60
  },
  {
    "name": "Washington's Birthday",
    "date": "2022-02-21",
    "description": "Sunny, with a high near 76.",
    "temperature": 76
  },
  {
    "name": "Monday Night",
    "date": "2022-02-21",
    "description": "Mostly clear, with a low around 62.",
    "temperature": 62
  },
  {
    "name": "Tuesday",
    "date": "2022-02-22",
    "description": "Sunny, with a high near 77.",
    "temperature": 77
  },
  {
    "name": "Tuesday Night",
    "date": "2022-02-22",
    "description": "Mostly clear, with a low around 62.",
    "temperature": 62
  }
])
