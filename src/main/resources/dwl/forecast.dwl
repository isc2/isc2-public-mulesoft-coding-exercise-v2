%dw 2.0
output application/json
---
payload.properties.periods map (value, index) -> {
  name: value.name,
  date: value.startTime as Date,
  description: value.detailedForecast
}
