%dw 2.0
output application/json
---
{
  wfo: payload.properties.cwa,
  x: payload.properties.gridX,
  y: payload.properties.gridY,
  forecastLink: payload.properties.forecast
}
