# (ISC)² Interview Coding Exercise

This repository contains a coding exercise for use in the hiring process for Mulesoft developers.

# Instructions for Candidates

* To get started, fork the repository, working off of the ```Unsolved``` branch.
* Make changes to the code to fulfill the ```Requirements``` section of this README.
* The Munit test suite should pass if your solution fulfills certain requirements.  Note that there are other requirements that are not covered by the unit tests.
* This exercise is open-book and you may use any online resources.
* When completed, push the solution to a Git repository of your own (Bitbucket, Github, GitLab, etc.) and provide the URL to your interviewer.

# Requirements

In this coding exercise you will modify one REST resource and create one REST resource according to the requirements.  These tasks should take less than two hours to complete.

For more information about the data requirements, examples are provided within the source in the form of unit tests, RAML, and Postman collections.

The code will make outbound calls to the public NWS Weather REST API.

Documentation: https://www.weather.gov/documentation/services-web-api

## 1. Update the Forecast resource to include temperature in the response body

In `forecast.xml` there is a flow that retrieves the weather forecast and transforms the response.  Update the response transform to include the temperature in a new temperature field.

## 2. Implement the Alerts resource

In `alerts.xml` there is a flow with processors stubbed in.  Change the HTTP request connector to invoke the Weather API using parameters from the initial Mule request.  Change the response transform to return the title, description, and instructions of each weather alert as shown in the Postman collection and unit tests.  Update the RAML accordingly.

1. Mule receives a request at `GET:/alerts/{statecode}`
2. Mule passes the `statecode` URI parameter to NWS in the `area` query parameter.  Example: https://api.weather.gov/alerts/active?area={statecode}
3. For each alert, Mule maps `title` to `headline`, `description` to `description`, and `instructions` to `instruction`

The unit test will look for an HTTP Request processor with a `doc:name` of Request.  Do not change the `doc:name`.
